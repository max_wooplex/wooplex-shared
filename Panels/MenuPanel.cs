using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;

public class MenuPanel : WooPanel
{
	private void OnOpen()
	{
		GameManager.Instance.EndGame();
	}

	public void OnStartGameClick()
	{
		PanelManager.Open<GamePanel>();
	}
}
