﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

using DG.Tweening;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextPopUp : MonoBehaviour
{
	private TextMeshProUGUI _textField;

	[SerializeField] private Settings _settings;

	public void ShowPopup(string text)
	{
		this._textField.text = text;

		Vector3 endValue = this.transform.localPosition;
		endValue.y += this._settings.UpDistance;

		this.GetComponent<RectTransform>().DOLocalMove(endValue, this._settings.Duration);

		this._textField.DOFade(
			0.0f,
			this._settings.Duration).OnComplete(() => 
			{
				Destroy(this.gameObject);
			}
		);
		this._textField.transform.DOPunchScale(Vector3.one * this._settings.PunchScale, this._settings.Duration);
	}

	public void ShowPopup(string text, Color color)
	{
		this._textField.color = color;

		this.ShowPopup(text);
	}

	private void Awake()
	{
		this._textField = this.GetComponent<TextMeshProUGUI>();
	}

#if UNITY_EDITOR
	//protected override void OnDrawGizmos()
	//{
	//}
#endif

	[System.Serializable]
	public class Settings
	{
		public float UpDistance = 0.5f;
		public float PunchScale = 0.5f;
		public float Duration = 1.0f;
	}
}

#if UNITY_EDITOR
	[CustomEditor(typeof(TextPopUp))]
	[CanEditMultipleObjects]
	public class TextPopUpEditor : Editor
	{
#pragma warning disable 0219, 414
		private TextPopUp _sTextPopUp;
#pragma warning restore 0219, 414

		private void OnEnable()
		{
			this._sTextPopUp = this.target as TextPopUp;
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
		}
	}
#endif