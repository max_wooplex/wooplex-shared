using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;

using TMPro;

public class ScorePanel : WooPanel
{
	public RectTransform Content;

	public TextMeshProUGUI ScoreText;

	public TextPopUp ScorePopupTextPrefab;
	public TextPopUp PerfectPopupTextPrefab;

	protected override void OnDisable()
	{
		base.OnDisable();

		if (ScoreManager.Instance != null)
		{
			ScoreManager.Instance.OnScoreChange.RemoveListener(OnScoreChange);
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		
		ScoreManager.Instance.OnScoreChange.AddListener(OnScoreChange);
	}

	void OnScoreChange()
	{
		//this.ScoreText.text = string.Format("{0:0.00}", (float)ScoreManager.Instance.Score / 100.0f) + "$";
		this.ScoreText.text = ScoreManager.Instance.Score.ToString();
	}

	private TextPopUp InstantiatePopUp(Vector3 worldPosition)
	{
		TextPopUp scorePopupText = GameObject.Instantiate(ScorePopupTextPrefab, this.Content);
		scorePopupText.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(worldPosition);

		return scorePopupText;
	}

	public void ShowPerfectPopupAtWorldPosition(Vector3 worldPosition, Color color)
	{
		this.InstantiatePopUp(worldPosition).ShowPopup("Perfect!", color);
	}

	public void ShowPerfectPopupAtWorldPosition(Vector3 worldPosition)
	{
		this.InstantiatePopUp(worldPosition).ShowPopup("Perfect!");
	}

	public void ShowScorePopupAtWorldPosition(Vector3 worldPosition, int scoreAmount, Color color)
	{
		this.InstantiatePopUp(worldPosition).ShowPopup("+" + scoreAmount, color);
	}

	public void ShowScorePopupAtWorldPosition(Vector3 worldPosition, int scoreAmount)
	{
		this.InstantiatePopUp(worldPosition).ShowPopup("+" + scoreAmount);
	}
}
