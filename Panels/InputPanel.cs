using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor.Events;
#endif

[System.Serializable]
public class InputEvent : UnityEvent<Vector2> { }

public class InputPanel : WooPanel
{
	public InputEvent OnTap;

	public InputEvent OnInputMovePosition;

	public InputEvent OnInputMove;
	public InputEvent OnInputStart;
	public InputEvent OnInputEnd;

	[SerializeField] private SwipeEvents _swipeEvents;
	public SwipeEvents SwipeEvents { get { return this._swipeEvents; } }

	private Vector2 _previousInputPosition;
	private Vector2 startingInputPosition;

	private bool _isInputActive = false;
	private int _previousFingerId = -1;

	private bool _isSwipeDetected = false;

	private const float SWIPE_DISTANCE = 0.1f;

	public static float S_InputValue { get; private set; }

#if UNITY_EDITOR
	[SerializeField] private bool _debugIUE = true;
#endif

	private void CheckForSwipes()
	{
		if (this._isSwipeDetected)
			return;

		if (this._previousInputPosition.x - this.startingInputPosition.x > SWIPE_DISTANCE)
		{
			this._isSwipeDetected = true;
			this._swipeEvents.OnSwipeRight.Invoke();
		}
		if (this._previousInputPosition.x - this.startingInputPosition.x < -SWIPE_DISTANCE)
		{
			this._isSwipeDetected = true;
			this._swipeEvents.OnSwipeLeft.Invoke();
		}
		if (this._previousInputPosition.y - this.startingInputPosition.y > SWIPE_DISTANCE)
		{
			this._isSwipeDetected = true;
			this._swipeEvents.OnSwipeUp.Invoke();
		}
		if (this._previousInputPosition.y - this.startingInputPosition.y < -SWIPE_DISTANCE)
		{
			this._isSwipeDetected = true;
			this._swipeEvents.OnSwipeDown.Invoke();
		}
	}

	public void PanelUpdate()
	{
		if (this._isInputActive)
		{
			Vector2 currentInputPosition = this.GetCurrentInputWorldPosition();

			Vector2 delta = currentInputPosition - this._previousInputPosition;

			this._previousInputPosition = currentInputPosition;

			this.OnInputMove.Invoke(delta);
			this.OnInputMovePosition.Invoke(currentInputPosition);

			this.CheckForSwipes();
		}
	}

	private Vector2 GetCurrentInputScreenPosition()
	{
		Vector3 inputPosition = Input.mousePosition;

		if (Input.touchCount > 0)
		{
			bool isTouchFound = Input.touches.Any(el => el.fingerId == this._previousFingerId);

			Touch touch = Input.touches[0];

			if (isTouchFound)
			{
				touch = Input.touches.First(el => el.fingerId == this._previousFingerId);
			}

			inputPosition = touch.position;
		}

		return inputPosition;
	}

	private Vector2 GetCurrentInputWorldPosition()
	{
		//return mainCamera.ScreenToWorldPoint(GetCurrentInputScreenPosition());
		return this.GetCurrentInputScreenPosition() / Screen.dpi;
	}

	public override void OnPointerDown(PointerEventData eventData)
	{
#if UNITY_EDITOR
		if (this._debugIUE)
			Debug.Log("On Pointer Down");
#endif
		base.OnPointerDown(eventData);

		S_InputValue = 1f;

		Vector2 currentInputPosition = GetCurrentInputWorldPosition();

		this._isInputActive = true;
		this._isSwipeDetected = false;

		this.OnInputStart.Invoke(currentInputPosition);

		this._previousInputPosition = GetCurrentInputWorldPosition();
		this.startingInputPosition = this._previousInputPosition;
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
#if UNITY_EDITOR
		if (this._debugIUE)
			Debug.Log("On Pointer Up");
#endif
		base.OnPointerUp(eventData);

		S_InputValue = 0f;

		Vector2 currentInputPosition = GetCurrentInputWorldPosition();

		this._isInputActive = false;
		if (!this._isSwipeDetected)
		{
			this.OnTap.Invoke(currentInputPosition);
		}

		this.OnInputEnd.Invoke(currentInputPosition);
	}
}

[System.Serializable]
public struct SwipeEvents
{
	public UnityEvent OnSwipeLeft;
	public UnityEvent OnSwipeRight;
	public UnityEvent OnSwipeUp;
	public UnityEvent OnSwipeDown;
}