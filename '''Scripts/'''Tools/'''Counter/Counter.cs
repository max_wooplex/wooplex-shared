﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif



using TMPro;

public abstract class Counter<T> : Singleton<T>
	where T : Counter<T>
{
	/// <summary>
	/// Secondary value. Resets every time associated event triggers.
	/// </summary>
	private int _internalValue;

	/// <summary>
	/// All counts that are never reset.
	/// </summary>
	public int GlobalValue { get; private set; }

	/// <summary>
	/// Main value. Resets only by request.
	/// </summary>
	public int Value { get; private set; }

	public void ResetValue()
	{
		this._internalValue = 0;
		this.Value = 0;
	}

	[SerializeField] private CounterEvent _counterEvent;

	public void AddValue(int quantity)
	{
		this.GlobalValue += quantity;
		this.Value += quantity;

		this._internalValue += quantity;
		if (this._internalValue >= this._counterEvent.TriggerValue)
		{
			this._counterEvent.Event.Invoke();

			this._internalValue = 0;
		}
	}

	public void AddValue()
	{
		this.AddValue(1);
	}

#if UNITY_EDITOR
#endif
}

[System.Serializable]
public class CounterEvent
{
	[SerializeField] private UnityEvent _event;
	public UnityEvent Event { get { return this._event; } }

	[SerializeField] private int _triggerValue = int.MaxValue;
	public int TriggerValue { get { return this._triggerValue; } }
}