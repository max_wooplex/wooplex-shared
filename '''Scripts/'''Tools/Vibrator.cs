﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vibrator
{
#if UNITY_ANDROID && !UNITY_EDITOR
    public static AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    public static AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
    public static AndroidJavaObject vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif

    public static void Vibrate()
    {
#if !UNITY_EDITOR
	#if UNITY_ANDROID
		vibrator.Call("vibrate");
	#elif UNITY_IOS
		iOSHapticFeedback.Instance.Trigger(iOSHapticFeedback.iOSFeedbackType.ImpactMedium);
	#endif
#else
	Handheld.Vibrate();
#endif
	}

    public static void Vibrate(long milliseconds, iOSHapticFeedback.iOSFeedbackType iOSFeedbackType = iOSHapticFeedback.iOSFeedbackType.None)
    {
#if !UNITY_EDITOR
	#if UNITY_ANDROID
		vibrator.Call("vibrate", milliseconds);
	#elif UNITY_IOS
		if (iOSFeedbackType != iOSHapticFeedback.iOSFeedbackType.None)
			iOSHapticFeedback.Instance.Trigger(iOSFeedbackType);
		else if (milliseconds <= 250)
			iOSHapticFeedback.Instance.Trigger(iOSHapticFeedback.iOSFeedbackType.ImpactLight);
		else if (milliseconds <= 1000)
			iOSHapticFeedback.Instance.Trigger(iOSHapticFeedback.iOSFeedbackType.ImpactMedium);
		else
			iOSHapticFeedback.Instance.Trigger(iOSHapticFeedback.iOSFeedbackType.ImpactHeavy);
	#endif
#else
	Handheld.Vibrate();
#endif
    }

    public static void Vibrate(long[] pattern, int repeat)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        vibrator.Call("vibrate", pattern, repeat);
#else
        Handheld.Vibrate();

		Debug.LogWarning("Vibration patterns aren't supported on iOS. Handheld vibration is being used instead.");
#endif
    }

    public static void Cancel()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        vibrator.Call("cancel");
#else
		Debug.LogWarning("Cancel isn't supported on iOS");
#endif
	}
}
