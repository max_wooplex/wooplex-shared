﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProperties
{
    public float Distance { get; private set; }
	public float SnakeSpeed { get; private set; }

	public int GoalBlockCount { get; private set; }

    public LevelProperties(int level, LevelControllerSettings levelControllerSettings)
    {
        this.Distance = Mathf.Lerp(levelControllerSettings.DistanceRange.x, levelControllerSettings.DistanceRange.y, level / 20f) + level;
		this.SnakeSpeed = Mathf.Lerp(levelControllerSettings.SnakeSpeedRange.x, levelControllerSettings.SnakeSpeedRange.y, level / 50f);

		this.GoalBlockCount = levelControllerSettings.InitialGoalBlockCount + level;
    }
}
