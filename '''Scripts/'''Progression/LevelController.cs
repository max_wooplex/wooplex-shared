﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wooplex.Panels;

public class LevelController : Singleton<LevelController>
{
	/// <summary>
	/// Active level.
	/// </summary>
	/// <value>Level identifier.</value>
	public int Level { get; private set; }

	/// <summary>
	/// Increments the level by one and saves progress.
	/// </summary>
	public void IncrementLevel()
	{
		this.Level++;

		PlayerPrefs.SetInt("MaxLevel", this.Level);

		SkinsManager.Instance.ChangeSkin(SkinsManager.SkinChangeProcessType.Incremental);
	}

	/// <summary>
	/// Active level properties.
	/// </summary>
	/// <value>The level properties.</value>
	public LevelProperties LevelProperties { get; private set; }

	[SerializeField] private LevelControllerSettings _levelControllerSettings;

	/// <summary>
	/// - Clears active level.
	/// - Generates the level.
	/// </summary>
	/// <param name="level">Level.</param>
	public void GenerateLevel(int level)
	{
		this.Level = level;
		this.LevelProperties = new LevelProperties(this.Level, this._levelControllerSettings);

		//ScoreManager.Instance.ResetScore();

		Generator.Instance.Generate();

		//this.levelProgressPanel.ActiveLevelTextField.text = this.Level.ToString();
		//this.levelProgressPanel.NextLevelTextField.text = (this.Level + 1).ToString();
	}

	///// <summary>
	///// Generates the next level.
	///// </summary>
	//public void GenerateNextLevel()
	//{
	//    this.GenerateLevel(++this.Level);
	//}

	/// <summary>
	/// Generates/Re-generates active level.
	/// </summary>
	public void GenerateLevel()
	{
		this.GenerateLevel(this.Level);
	}

	private void OnGameStart()
	{
		this.GenerateLevel();
	}

	private void OnGameEnd()
	{
	}

	private void OnEnable()
	{
		GameManager.Instance.OnGameStart.AddListener(this.OnGameStart);
		GameManager.Instance.OnGameEnd.AddListener(this.OnGameEnd);
	}

	private void OnDisable()
	{
		if (GameManager.Instance != null)
		{
			GameManager.Instance.OnGameStart.RemoveListener(this.OnGameStart);
			GameManager.Instance.OnGameEnd.RemoveListener(this.OnGameEnd);
		}
	}

	private void Awake()
	{
		//PlayerPrefs.SetInt("MaxLevel", 1);
		this.Level = PlayerPrefs.GetInt("MaxLevel", 1);
	}
}
