﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

[CreateAssetMenu(fileName = "Level Controller Settings", menuName = "Level Controller/Settings", order = 81)]
public class LevelControllerSettings : ScriptableObject
{
	[SerializeField] private Vector2 _distanceRange = new Vector2(15f, 60f);
	public Vector2 DistanceRange { get { return this._distanceRange; } }

	[SerializeField] private Vector2 _snakeSpeedRange = new Vector2(3f, 6f);
	public Vector2 SnakeSpeedRange { get { return this._snakeSpeedRange; } }

	[SerializeField] private int _initialGoalBlockcount = 9;
	public int InitialGoalBlockCount { get { return this._initialGoalBlockcount; } }

#if UNITY_EDITOR
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(LevelControllerSettings))]
[CanEditMultipleObjects]
public class LevelControllerSettingsEditor : Editor
{
#pragma warning disable 0219, 414
	private LevelControllerSettings _sLevelControllerSettings;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sLevelControllerSettings = this.target as LevelControllerSettings;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif