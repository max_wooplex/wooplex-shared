﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu]
public class Skin : ScriptableObject
{
	[SerializeField] private List<SkinColor> _colors;
	public List<SkinColor> Colors { get { return this._colors; } private set { this._colors = value; } }

	[SerializeField] private List<SkinGradient> _gradients;
	public List<SkinGradient> Gradients { get { return this._gradients; } private set { this._gradients = value; } }

	[SerializeField] private List<SkinSprite> _sprites;
	public List<SkinSprite> Sprites { get { return this._sprites; } private set { this._sprites = value; } }

	public void Copy(Skin skin)
	{
		this.Colors = skin.Colors.Where(el => true).ToList();
		this.Gradients = skin.Gradients.Where(el => true).ToList();
		this.Sprites = skin.Sprites.Where(el => true).ToList();
	}

	public Color GetColor(SkinValueId skinValueId)
	{
		return this._colors.Find(el => el.SkinColorType == skinValueId).Color;
	}

	public List<Color> GetColors(SkinValueId skinValueId)
	{
		return this._colors.FindAll(el => el.SkinColorType == skinValueId).ConvertAll(e => e.Color);
	}

	public Gradient GetGradient(SkinValueId skinValueId)
	{
		return this._gradients.Find(el => el.SkinColorType == skinValueId).Gradient;
	}

	public List<Gradient> GetGradients(SkinValueId skinValueId)
	{
		return this._gradients.FindAll(el => el.SkinColorType == skinValueId).ConvertAll(e => e.Gradient);
	}

	public Sprite GetSprite(SkinValueId skinValueId)
	{
		return this._sprites.Find(el => el.SkinColorType == skinValueId).Sprite;
	}

	public List<Sprite> GetSprites(SkinValueId skinValueId)
	{
		return this._sprites.FindAll(el => el.SkinColorType == skinValueId).ConvertAll(e => e.Sprite);
	}
}