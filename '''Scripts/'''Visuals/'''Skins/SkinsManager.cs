﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct SkinColor
{
	public SkinValueId SkinColorType;
	public Color Color;

	public SkinColor(SkinValueId skinColorType, Color color)
	{
		this.SkinColorType = skinColorType;
		this.Color = color;
	}
}

[System.Serializable]
public struct SkinGradient
{
	public SkinValueId SkinColorType;
	public Gradient Gradient;
}

[System.Serializable]
public struct SkinSprite
{
	public SkinValueId SkinColorType;
	public Sprite Sprite;
}

public enum SkinValueId
{
	Main,
	Sunset,
	Moonlight,
	Oxygen,
	Darkness,
	Cosmos,
	Gravity
}

public class SkinEvent : UnityEvent<Skin> { }

public class SkinsManager : Singleton<SkinsManager>
{
	public Skin ActiveSkin { get; private set; }

	private int selectedSkinIndex;

	[SerializeField] private Skin _skinTemplate;
	public Skin SkinTemplate { get { return this._skinTemplate; } }

	[SerializeField] private List<Skin> skins;
	public List<Skin> Skins { get { return this.skins; } set { this.skins = value; this.ChangeSkin(0); } }

	public SkinEvent OnSkinChange = new SkinEvent();

	private void Awake()
	{
		this.ChangeSkin(0);

		//this.ChangeSkin((LevelController.Instance.Level - 1) % this.skins.Count);
	}

	public void Register<TTarget>(SkinSetter<TTarget> skinSetter)
		where TTarget : Component
	{
		skinSetter.ChangeSkin(this.ActiveSkin);
	}

	public void ChangeSkin(Skin skin)
	{
		this.ActiveSkin = skin;

		this.OnSkinChange.Invoke(this.ActiveSkin);
	}

	public void ChangeSkin(int index)
	{
		if (this.skins.Count > 0)
		{
			this.selectedSkinIndex = index;

			this.ChangeSkin(this.skins[this.selectedSkinIndex]);
		}
#if UNITY_EDITOR || DEBUG || TEST
		else
			throw new System.IndexOutOfRangeException();
#endif
	}

	public enum SkinChangeProcessType { Incremental, Random }

	public void ChangeSkin(SkinChangeProcessType skinChangeProcessType)
	{
		switch (skinChangeProcessType)
		{
			case SkinChangeProcessType.Incremental:

				this.ChangeSkin(((++this.selectedSkinIndex) % this.skins.Count + this.skins.Count) % this.skins.Count); //TODO: change to Loop()/LoopIndex() when extensions are ready.

				break;
			case SkinChangeProcessType.Random:

				this.ChangeSkin(Random.Range(0, this.skins.Count));

				break;
		}
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(SkinsManager))]
[CanEditMultipleObjects]
public class SkinsManagerEditor : Editor
{
	private const string SKINS_FOLDER_LOCATION = "Skins";

#pragma warning disable 0219, 414
	private SkinsManager _sSkinsManager;
#pragma warning restore 0219, 414

	private string _pathToResourcesSkins;

	private void OnEnable()
	{
		this._sSkinsManager = this.target as SkinsManager;

		this._pathToResourcesSkins = Path.Combine("Assets", Path.Combine("Resources", SKINS_FOLDER_LOCATION));
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if (GUILayout.Button(new GUIContent("Load Skins From Folder")))
		{
			List<Skin> loadedSkins = Resources.LoadAll<Skin>(SKINS_FOLDER_LOCATION).ToList();

			if (loadedSkins.Count > 0)
			{
				if (EditorUtility.DisplayDialog("Choose Loading Method", "Override - replaces all currently set skins in inspector with loaded ones.\n\nMerge - merges currently set skins in inspector with loaded ones.", "Override", "Merge"))
				{
					this._sSkinsManager.Skins = loadedSkins;
				}
				else
				{
					this._sSkinsManager.Skins = this._sSkinsManager.Skins.Concat(loadedSkins).ToList();
				}
			}
			else
			{
				Debug.LogError("Couldn't load any skins. The folder is empty or skins objects are corrupted.");
			}
		}

		if (GUILayout.Button(new GUIContent("Create Skin In Resources/" + SKINS_FOLDER_LOCATION)))
		{
			Directory.CreateDirectory(this._pathToResourcesSkins);

			Skin skin = ScriptableObjectUtility.CreateAsset<Skin>(this._pathToResourcesSkins);

			if (this._sSkinsManager.SkinTemplate != null)
				skin.Copy(this._sSkinsManager.SkinTemplate);

			//Selection.activeTransform = this._sSkinsManager.transform;
		}
	}
}
#endif