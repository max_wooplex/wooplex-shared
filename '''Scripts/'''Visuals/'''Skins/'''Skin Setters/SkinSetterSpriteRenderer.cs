﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class SkinSetterSpriteRenderer : SkinSetter<SpriteRenderer>
{
	public enum TargetFieldType { Color, Sprite, Both }

	[SerializeField] private TargetFieldType _targetField;
	public TargetFieldType TargetField { get { return this._targetField; } }
	
	public override void ChangeSkin(Skin skin)
	{
		switch (this._targetField)
		{
			case TargetFieldType.Color:

				this.target.color = skin.GetColor(this.skinColorType);

				break;
			case TargetFieldType.Sprite:

				this.target.sprite = skin.GetSprite(this.skinColorType);

				break;
			case TargetFieldType.Both:

				this.target.color = skin.GetColor(this.skinColorType);
				this.target.sprite = skin.GetSprite(this.skinColorType);

				break;
		}
	}
}
