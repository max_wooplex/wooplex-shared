﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SkinSetter<TTarget> : MonoBehaviour
	where TTarget : Component
{
	[SerializeField] protected SkinValueId skinColorType; 
	public SkinValueId SkinColorType { get { return this.skinColorType; } }

	protected TTarget target;

	public abstract void ChangeSkin(Skin skin);

    protected virtual void Awake()
    {
		this.target = this.GetComponent<TTarget>();
    }

    protected virtual void Start()
    {
        SkinsManager.Instance.Register(this);
    }

	[SerializeField] private bool _unSubscribeOnDisable = true;

    protected virtual void OnEnable()
    {
		if (this._unSubscribeOnDisable)
        	SkinsManager.Instance.OnSkinChange.AddListener(this.ChangeSkin);
		else if (this.target == null)
			SkinsManager.Instance.OnSkinChange.AddListener(this.ChangeSkin);
    }

    protected virtual void OnDisable()
    {
        if (this._unSubscribeOnDisable && SkinsManager.Instance != null)
        {
            SkinsManager.Instance.OnSkinChange.RemoveListener(this.ChangeSkin);
        }
    }
}