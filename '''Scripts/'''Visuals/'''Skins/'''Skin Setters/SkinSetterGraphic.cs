﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinSetterGraphic : SkinSetter<Graphic>
{
	public override void ChangeSkin(Skin skin)
	{
		this.target.color = skin.GetColor(this.skinColorType);
	}
}
