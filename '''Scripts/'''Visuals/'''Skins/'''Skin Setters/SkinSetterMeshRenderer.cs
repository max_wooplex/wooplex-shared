﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class SkinSetterMeshRenderer : SkinSetter<MeshRenderer>
{
	public override void ChangeSkin(Skin skin)
	{
		this.target.material.color = skin.GetColor(this.skinColorType);
	}
}
