﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSetterCamera : SkinSetter<Camera>
{
	public override void ChangeSkin(Skin skin)
	{
		this.target.backgroundColor = skin.GetColor(this.skinColorType);
	}	
}
