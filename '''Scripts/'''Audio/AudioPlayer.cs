﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public class AudioPlayer : Singleton<AudioPlayer>
{
	[SerializeField] private AudioSource _musicAudioSource;
	[SerializeField] private AudioSource _sfxAudioSource;

	[SerializeField] private AudioPack _musicPack;

	public enum AudioType { Music, SFX }

	public void PlayOneShot(AudioClip audioClip, AudioType audioType)
	{
		switch (audioType)
		{
			case AudioType.Music:

				this._musicAudioSource.PlayOneShot(audioClip);

				break;
			case AudioType.SFX:

				this._sfxAudioSource.PlayOneShot(audioClip);

				break;
		}
	}

    public void PlayOneShot(AudioClip audioClip, AudioType audioType, float volumeScale)
    {
        switch (audioType)
        {
            case AudioType.Music:

                this._musicAudioSource.PlayOneShot(audioClip, volumeScale);

                break;
            case AudioType.SFX:

                this._sfxAudioSource.PlayOneShot(audioClip, volumeScale);

                break;
        }
    }

    private IEnumerator PlayAudioPackProcess(AudioPack audioPack, AudioType audioType)
	{
		while (true)
		{
			this.PlayOneShot(audioPack.Next(), audioType);

			yield return new WaitForSecondsRealtime(audioPack.SelectedAudioClip.length + 1f);
		}
	}

	public void PlayAudioPack(AudioPack audioPack, AudioType audioType)
	{
		this.StartCoroutine(this.PlayAudioPackProcess(audioPack, audioType));
	}

	private void Awake()
	{
		this.PlayAudioPack(this._musicPack, AudioType.Music);
	}

#if UNITY_EDITOR
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(AudioPlayer))]
[CanEditMultipleObjects]
public class AudioPlayerEditor : Editor
{
#pragma warning disable 0219, 414
	private AudioPlayer _sAudioPlayer;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sAudioPlayer = this.target as AudioPlayer;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif