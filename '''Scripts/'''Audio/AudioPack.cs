﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

[CreateAssetMenu(fileName = "name Audio Pack", menuName = "Audio/Audio Pack", order = 81)]
public class AudioPack : ScriptableObject
{
	[SerializeField] private AudioClip[] _audioClips;

	private int _selectedAudioClipIndex = -1;

	public AudioClip SelectedAudioClip { get { return this._audioClips[this._selectedAudioClipIndex]; } }

	public AudioClip Previous()
	{
		this._selectedAudioClipIndex = ((--this._selectedAudioClipIndex % this._audioClips.Length) + this._audioClips.Length) % this._audioClips.Length;

		return this._audioClips[this._selectedAudioClipIndex];
	}

	public AudioClip Next()
	{
		this._selectedAudioClipIndex = ((++this._selectedAudioClipIndex % this._audioClips.Length) + this._audioClips.Length) % this._audioClips.Length;

		return this._audioClips[this._selectedAudioClipIndex];
	}

	public AudioClip GetRandom()
	{
		return this._audioClips[Random.Range(0, this._audioClips.Length)];
	}

	private void OnEnable()
	{
		this._selectedAudioClipIndex = -1;
	}

#if UNITY_EDITOR
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(AudioPack))]
[CanEditMultipleObjects]
public class AudioPackEditor : Editor
{
#pragma warning disable 0219, 414
	private AudioPack _sAudioPack;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sAudioPack = this.target as AudioPack;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif