﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public class CameraController : Singleton<CameraController>
{
	[SerializeField] private Transform _target;
	public Transform Target { get { return this._target; } }

#if UNITY_EDITOR
	//protected override void OnDrawGizmos()
	//{
	//}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(CameraController))]
[CanEditMultipleObjects]
public class CameraControllerEditor : Editor
{
#pragma warning disable 0219, 414
	private CameraController _sCameraController;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sCameraController = this.target as CameraController;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif