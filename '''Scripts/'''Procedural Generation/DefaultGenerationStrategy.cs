﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public class DefaultGenerationStrategy : GenerationStrategy
{
	private void Generate(AssetBehaviour innerPart, GenerationStrategySettings generationStrategySettings, ref float requiredSpaceForNextGeneration, ref float verticalInnerPartGenerationEdgePosition, ref float distanceBetweenAnchors)
	{
		requiredSpaceForNextGeneration -= innerPart.CalculatedSize;

#if UNITY_EDITOR
		if (requiredSpaceForNextGeneration < innerPart.CalculatedSize)
			Debug.LogWarning("There is a high chance that bridge part will overflow the generated space.");
#endif
		Vector2 yPositionMinMax = new Vector2(
			requiredSpaceForNextGeneration - distanceBetweenAnchors / 2f,
			verticalInnerPartGenerationEdgePosition
		);

		innerPart.transform.localPosition = new Vector3(
			innerPart.transform.localPosition.x,
			Random.Range(
				yPositionMinMax.x,
				yPositionMinMax.y
			),
			innerPart.transform.localPosition.z
		);

		verticalInnerPartGenerationEdgePosition = innerPart.transform.localPosition.y - innerPart.CalculatedSize / 2f;
	}

	protected override void GenerationProcess(GenerationStrategySettings generationStrategySettings)
	{
		// Get random distance between anchors.
		float distanceBetweenAnchors = Random.Range(generationStrategySettings.DistanceBetweenAnchorsMinMax.x, generationStrategySettings.DistanceBetweenAnchorsMinMax.y);

		GameObject defaultItem = new GameObject("Default Item");

		defaultItem.transform.position = Generator.Instance.GenerationEdgePosition;
		defaultItem.transform.SetParent(this.generatedObjectsContainer, true);

		this.generatedObjects.Add(defaultItem);

		// Create inner parts. Calculate required space.
		List<AssetBehaviour> innerParts = new List<AssetBehaviour>(generationStrategySettings.InnerPartPrefabs.Length);

		float requiredSpaceForNextGeneration = 0;

		for (int i = 0; i < generationStrategySettings.InnerPartPrefabs.Length; i++)
		{
			AssetBehaviour innerPart = GameObject.Instantiate(generationStrategySettings.InnerPartPrefabs[i], defaultItem.transform, false);
			innerParts.Add(innerPart);

			requiredSpaceForNextGeneration += innerPart.CalculatedSize;
		}

		// Place inner parts. Create connections.
		new Random().Shuffle(innerParts);

		float verticalInnerPartGenerationEdgePosition = distanceBetweenAnchors / 2f;

		for (int i = 0; i < innerParts.Count; i++)
		{
			this.Generate(innerParts[i], generationStrategySettings, ref requiredSpaceForNextGeneration, ref verticalInnerPartGenerationEdgePosition, ref distanceBetweenAnchors);
		}
	}

#if UNITY_EDITOR
	//protected override void OnDrawGizmos()
	//{
	//}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(DefaultGenerationStrategy))]
[CanEditMultipleObjects]
public class DefaultGenerationStrategyEditor : Editor
{
#pragma warning disable 0219, 414
	private DefaultGenerationStrategy _sDefaultGenerationStrategy;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sDefaultGenerationStrategy = this.target as DefaultGenerationStrategy;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif