﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif



using TMPro;

[CreateAssetMenu(fileName = "name Generation Strategy Settings", menuName = "Procedural Generation/Generation Strategy Settings", order = 81)]
public class GenerationStrategySettings : ScriptableObject
{
	[Header("General Settings")]

	[Range(1, 100)]
	[SerializeField] private int _level;
	public int _Level { get { return this._level; } }

	[Range(0f, 1f)]
	[SerializeField] private float _probability = 1f;
	public float Probability { get { return this._probability; } }

	[SerializeField] private Vector2 _marginMinMax = new Vector2(1f, 2f);
	public Vector2 MarginMinMax { get { return this._marginMinMax; } }

	[SerializeField] private Vector2 _displacement = new Vector2(0f, 2f);
	public Vector2 Displacement { get { return this._displacement; } }

	[SerializeField] private Vector3 _offset = new Vector3(0f, 0f, 0f);
	public Vector3 Offset { get { return this._offset; } }

	[SerializeField] private Vector2 _distanceBetweenAnchorsMinMax = new Vector2(3f, 12f);
	public Vector2 DistanceBetweenAnchorsMinMax { get { return this._distanceBetweenAnchorsMinMax; } }

	[SerializeField] private AssetBehaviour[] _innerPartPrefabs;
	public AssetBehaviour[] InnerPartPrefabs { get { return this._innerPartPrefabs; } }

#if UNITY_EDITOR
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(GenerationStrategySettings))]
[CanEditMultipleObjects]
public class GenerationStrategySettingsEditor : Editor
{
#pragma warning disable 0219, 414
	private GenerationStrategySettings _sGenerationStrategySettings;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sGenerationStrategySettings = this.target as GenerationStrategySettings;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif