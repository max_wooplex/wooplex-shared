﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public abstract class MainGenerationStrategy : GenerationStrategy
{
	[SerializeField] private GenerationStrategySettings _levelFinishGenerationStrategySettings;
	public GenerationStrategySettings LevelFinishGenerationStrategySettings { get { return this._levelFinishGenerationStrategySettings; } }

#if UNITY_EDITOR
#endif
}

#if UNITY_EDITOR
	[CustomEditor(typeof(MainGenerationStrategy))]
	[CanEditMultipleObjects]
	public class MainGenerationStrategyEditor : Editor
	{
#pragma warning disable 0219, 414
		private MainGenerationStrategy _sMainGenerationStrategy;
#pragma warning restore 0219, 414

		private void OnEnable()
		{
			this._sMainGenerationStrategy = this.target as MainGenerationStrategy;
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
		}
	}
#endif