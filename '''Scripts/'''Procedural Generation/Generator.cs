﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public class Generator : Singleton<Generator>
{
	[SerializeField] private Camera _mainCamera;
	public Camera MainCamera { get { return this._mainCamera; } }

	[Header("Settings")]

	[SerializeField] private Settings _generationSettings;
	public Settings GenerationSettings { get { return this._generationSettings; } }

	[Header("Generation Strategies")]

	[SerializeField] private GenerationStrategy[] _generationStrategies;
	public GenerationStrategy[] GenerationStrategies { get { return this._generationStrategies; } }

	[SerializeField] private GenerationStrategy[] _independentGenerationStrategies;
	public GenerationStrategy[] IndependentGenerationStrategies { get { return this._independentGenerationStrategies; } }

	[SerializeField] private MainGenerationStrategy _mainGenerationStrategy;
	public MainGenerationStrategy MainGenerationStrategy { get { return this._mainGenerationStrategy; } }

	// Generation edge because it's not last position the object has been generated on. The magrin and every other settings are included as well.
	public Vector3 GenerationEdgePosition { get; set; }

	public System.Func<bool> ValidateGeneration;

	//public bool FinishGenerationApproved { get { return LevelController.Instance.LevelProperties.Distance - this.GenerationEdgePosition.x < this._generationSettings.LevelFinishGenerationDistance; } }
	//public bool FinishGenerationApproved { get { return LevelPerfectsCounter.Instance.Value >= LevelController.Instance.LevelProperties.GoalBlockCount; } }
	public bool FinishGenerationApproved { get; set; }

	private bool ReachedExtremumFinish()
	{
		// if (LevelController.Instance.LevelProperties.Distance - this._mainCamera.transform.position.x <= -3f)
		// {
		// 	GameManager.Instance.RestartGame();

		// 	return true;
		// }

		if (this.GenerationEdgePosition.x - this._mainCamera.transform.position.x <= -3f)
		{
			GameManager.Instance.RestartGame();

			return true;
		}

		return false;
	}

	private bool ApproveGeneration()
	{
		//TODO: Uncomment in case you need distanced levels again.
		//if (this.FinishGenerationApproved)
		//{
		//	this.Generate(this._levelFinishGenerationStrategySettings);

		//	this._validateGeneration = new System.Func<bool>(this.ReachedExtremumFinish);

		//	return false;
		//}

		if (this.FinishGenerationApproved)
		{
			this._mainGenerationStrategy.Generate(this._mainGenerationStrategy.LevelFinishGenerationStrategySettings);

			this.ValidateGeneration = new System.Func<bool>(this.ReachedExtremumFinish);

			return false;
		}

		return true;
	}

	public void GenerateNext(bool applyMargin = true, bool applyDisplacement = true)
	{
		this._generationStrategies[Random.Range(0, this._generationStrategies.Length)].GenerateNext(applyMargin, applyDisplacement);

		for (int i = 0; i < this._independentGenerationStrategies.Length; i++)
		{
			this._independentGenerationStrategies[i].GenerateNext(applyDisplacement:true);
		}
	}

	public void TryToGenerateNext()
	{
		if (this.ValidateGeneration.Invoke() && this.GenerationEdgePosition.x - this._mainCamera.transform.position.x < this._generationSettings.GenerationDistance)
		{
			this.GenerateNext();
		}
	}

	public void Generate()
	{
		this._mainGenerationStrategy.OnGenerateStart();
		for (int i = 0; i < this._generationStrategies.Length; i++)
		{
			this._generationStrategies[i].OnGenerateStart();
		}
		for (int i = 0; i < this._independentGenerationStrategies.Length; i++)
		{
			this._independentGenerationStrategies[i].OnGenerateStart();
		}

		this.GenerationEdgePosition = this._generationSettings.StartingPoint;

		this.FinishGenerationApproved = false;
		this.ValidateGeneration = new System.Func<bool>(this.ApproveGeneration);


		this._mainGenerationStrategy.OnGenerate();
		for (int i = 0; i < this._generationStrategies.Length; i++)
		{
			this._generationStrategies[i].OnGenerate();
		}
		for (int i = 0; i < this._independentGenerationStrategies.Length; i++)
		{
			this._independentGenerationStrategies[i].OnGenerate();
		}

		// Generate initial bridges.

		if (this._generationSettings.InitialGenerationQuantity > 0)
		{
			// Generate first.
			this.GenerateNext(false, false);

			for (int i = 1; i < this._generationSettings.InitialGenerationQuantity; i++)
			{
				this.GenerateNext();
			}
		}

		this._mainGenerationStrategy.OnGenerateEnd();
		for (int i = 0; i < this._generationStrategies.Length; i++)
		{
			this._generationStrategies[i].OnGenerateEnd();
		}
		for (int i = 0; i < this._independentGenerationStrategies.Length; i++)
		{
			this._independentGenerationStrategies[i].OnGenerateEnd();
		}
	}

	//private void OnGameStart()
	//{
	//}

	//private void OnGameEnd()
	//{
	//}

	//private void OnEnable()
	//{
	//	GameManager.Instance.OnGameStart.AddListener(this.OnGameStart);
	//	GameManager.Instance.OnGameEnd.AddListener(this.OnGameEnd);
	//}

	//private void OnDisable()
	//{
	//	if (GameManager.Instance != null)
	//	{
	//		GameManager.Instance.OnGameStart.RemoveListener(this.OnGameStart);
	//		GameManager.Instance.OnGameEnd.RemoveListener(this.OnGameEnd);
	//	}
	//}

#if UNITY_EDITOR
	//protected override void OnDrawGizmos()
	//{
	//}
#endif

	[System.Serializable]
	public class Settings
	{
		[Header("Values")]

		[SerializeField] private Vector3 _startingPoint;
		public Vector3 StartingPoint { get { return this._startingPoint; } }

		[SerializeField] private float _generationDistance = 8f;
		public float GenerationDistance { get { return this._generationDistance; } }

		[SerializeField] private float _levelFinishGenerationDistance = 5f;
		public float LevelFinishGenerationDistance { get { return this._levelFinishGenerationDistance; } }

		[SerializeField] private int _initialGenerationQuantity;
		public int InitialGenerationQuantity { get { return this._initialGenerationQuantity; } }
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(Generator))]
[CanEditMultipleObjects]
public class GeneratorEditor : Editor
{
#pragma warning disable 0219, 414
	private Generator _sGenerator;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sGenerator = this.target as Generator;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif