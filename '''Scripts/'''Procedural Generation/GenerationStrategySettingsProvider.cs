﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

[System.Serializable]
public class GenerationStrategySettingsProvider
{
	public List<GenerationStrategySettings> GenerationStrategiesSettings { get; private set; }

	public void SelectGenerationStrategiesSettings(GenerationStrategySettings[] generationStrategiesSettings, int levelPoints, float maxLevelPointsPerGenerationStrategyPercentage = 0.2f)
	{
		this.GenerationStrategiesSettings.Clear();

		maxLevelPointsPerGenerationStrategyPercentage = Mathf.Clamp(maxLevelPointsPerGenerationStrategyPercentage, 0f, 1f);

		int maxLevelPointsPerGenerationStrategy = Mathf.CeilToInt(levelPoints * maxLevelPointsPerGenerationStrategyPercentage);

		int currentLevelPoints = 0;

		new Random().Shuffle(generationStrategiesSettings);
		for (int i = 0; i < generationStrategiesSettings.Length && currentLevelPoints < levelPoints; i++)
		{
			if (generationStrategiesSettings[i]._Level <= maxLevelPointsPerGenerationStrategy 
				&& currentLevelPoints + generationStrategiesSettings[i]._Level <= levelPoints)
			{
				this.GenerationStrategiesSettings.Add(generationStrategiesSettings[i]);
			}
		}

		this.GenerationStrategiesSettings.Sort((el, other) => { return el.Probability.CompareTo(other.Probability); });
	}

	public GenerationStrategySettings ProvideSettings()
	{
		float rndValue = Random.value;

		List<GenerationStrategySettings> selection = new List<GenerationStrategySettings>();

		for (int a = 0; a < this.GenerationStrategiesSettings.Count; a++)
		{
			if (rndValue < this.GenerationStrategiesSettings[a].Probability)
			{
				while (a < this.GenerationStrategiesSettings.Count
					&& rndValue < this.GenerationStrategiesSettings[a].Probability)
				{
					selection.Add(this.GenerationStrategiesSettings[a]);

					a++;
				};

				return selection[Random.Range(0, selection.Count)];
			}
		}

		return null;
	}

	public GenerationStrategySettingsProvider()
	{
		this.GenerationStrategiesSettings = new List<GenerationStrategySettings>(16);
	}

#if UNITY_EDITOR
#endif
}