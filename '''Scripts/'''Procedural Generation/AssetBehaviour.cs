﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif



using TMPro;

public class AssetBehaviour : MonoBehaviour
{
	[SerializeField] protected Vector2 sizeMinMax = new Vector2(4f, 6f);
	public Vector2 SizeMinMax { get { return this.sizeMinMax; } }

	protected float? calculatedSize;
	public float CalculatedSize
	{
		get
		{
			if (!this.calculatedSize.HasValue)
			{
				this.calculatedSize = Random.Range(this.sizeMinMax.x, this.sizeMinMax.y);
			}

			return this.calculatedSize.Value;
		}

		set
		{
			this.calculatedSize = value;
		}
	}

#if UNITY_EDITOR
	protected virtual void OnDrawGizmosSelected()
	{
		Vector3 bias = new Vector3(0.3f, 0f);

		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(this.transform.position + bias, new Vector3(Mathf.Epsilon, this.sizeMinMax.x));

		bias.x += 0.1f;

		Gizmos.color = Color.cyan;
		Gizmos.DrawWireCube(this.transform.position + bias, new Vector3(Mathf.Epsilon, this.sizeMinMax.y));
	}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(AssetBehaviour))]
[CanEditMultipleObjects]
public class AssetBehaviourEditor : Editor
{
#pragma warning disable 0219, 414
	private AssetBehaviour _sAssetBehaviour;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sAssetBehaviour = this.target as AssetBehaviour;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif