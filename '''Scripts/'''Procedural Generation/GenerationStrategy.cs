﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public abstract class GenerationStrategy : MonoBehaviour
{
	[SerializeField] protected GenerationStrategySettings[] generationStrategiesSettings;

	protected Transform generatedObjectsContainer;
	protected List<GameObject> generatedObjects = new List<GameObject>(8);

	protected GenerationStrategySettingsProvider generationStrategySettingsProvider = new GenerationStrategySettingsProvider();

	protected abstract void GenerationProcess(GenerationStrategySettings generationStrategySettings);

	public virtual void Generate(GenerationStrategySettings generationStrategySettings, bool applyMargin = true, bool applyDisplacement = true)
	{
		// Apply horizontal margin.
		Generator.Instance.GenerationEdgePosition = new Vector3(
			Generator.Instance.GenerationEdgePosition.x + (applyMargin ? Random.Range(generationStrategySettings.MarginMinMax.x, generationStrategySettings.MarginMinMax.y) : 0f),
			Generator.Instance.GenerationSettings.StartingPoint.y + (applyDisplacement ? Random.Range(generationStrategySettings.Displacement.x, generationStrategySettings.Displacement.y) : 0f),
			Generator.Instance.GenerationEdgePosition.z
		);

		this.GenerationProcess(generationStrategySettings);

		// Apply horizontal margin.
		Generator.Instance.GenerationEdgePosition += new Vector3(
			(applyMargin ? Random.Range(generationStrategySettings.MarginMinMax.x, generationStrategySettings.MarginMinMax.y) : 0f),
			0f,
			0f
		);
	}

	public virtual void GenerateNext(bool applyMargin = true, bool applyDisplacement = true)
	{
		this.Generate(this.generationStrategySettingsProvider.ProvideSettings(), applyMargin, applyDisplacement);
	}

	protected virtual void ClearGenerated()
	{
		for (int i = 0; i < this.generatedObjects.Count; i++)
		{
			GameObject.Destroy(this.generatedObjects[i].gameObject);
		}

		this.generatedObjects.Clear();
	}

	protected virtual void PrepareForGeneration()
	{
		this.generationStrategySettingsProvider.SelectGenerationStrategiesSettings(this.generationStrategiesSettings, LevelController.Instance.Level * 5);
	}

	protected virtual void RemoveOffScreenObjects()
	{
#if UNITY_EDITOR
		if (this.generatedObjects.Count < 1)
		{
			Debug.LogWarning("Generated objects list is empty. In case you forgot: you have to add generated objects to list if you want objects off screen to be destroyed. Use this.generatedObjects .");
		}
#endif

		if (this.generatedObjects.Count > 0 &&
			this.generatedObjects[0].transform.position.x < Generator.Instance.MainCamera.transform.position.x - Generator.Instance.GenerationSettings.GenerationDistance)
		{
			GameObject.Destroy(this.generatedObjects[0].gameObject);
			this.generatedObjects.RemoveAt(0);
		}
	}

	public virtual void OnGenerateStart()
	{
		this.ClearGenerated();
		this.PrepareForGeneration();
	}

	public virtual void OnGenerate() { }
	public virtual void OnGenerateEnd() { }

	protected virtual void Awake()
	{
		this.generatedObjectsContainer = new GameObject(this.GetType() + " Generation Container").transform;
	}

	protected virtual void Update()
	{
		this.RemoveOffScreenObjects();
	}

#if UNITY_EDITOR
	//protected override void OnDrawGizmos()
	//{
	//}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(GenerationStrategy))]
[CanEditMultipleObjects]
public class GenerationStrategyEditor : Editor
{
#pragma warning disable 0219, 414
	private GenerationStrategy _sGenerationStrategy;
#pragma warning restore 0219, 414

	private void OnEnable()
	{
		this._sGenerationStrategy = this.target as GenerationStrategy;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
#endif