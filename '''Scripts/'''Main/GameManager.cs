﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum GameState
{
	NotPlaying,
	Playing,
	Paused,
}

public class GameManager : Singleton<GameManager>
{
	public GameState State;

	public UnityEvent OnGameStartBegin = new UnityEvent();
	public UnityEvent OnGameStart = new UnityEvent();
	public UnityEvent OnGamePause = new UnityEvent();
	public UnityEvent OnGameResume = new UnityEvent();
	public UnityEvent OnGameEnd = new UnityEvent();

	[SerializeField] private float _restartDelaySeconds = 1f;

	public void StartGame()
	{
		if (State == GameState.NotPlaying || State == GameState.Paused)
		{
			State = GameState.Playing;

			this.OnGameStartBegin.Invoke();
			this.OnGameStart.Invoke();
		}
	}

	public void PauseGame()
	{
		if (State == GameState.Playing)
		{
			State = GameState.Paused;
			OnGamePause.Invoke();
		}
	}

	public void ResumeGame()
	{
		if (State == GameState.Paused)
		{
			State = GameState.Playing;
			OnGameResume.Invoke();
		}
	}

	public void EndGame()
	{
		if (State == GameState.Playing || State == GameState.Paused)
		{
			State = GameState.NotPlaying;
			OnGameEnd.Invoke();
		}
	}

	private IEnumerator RestartGameProcess()
	{
		if (this._restartDelaySeconds > 0)
		{
			yield return new WaitForSeconds(this._restartDelaySeconds);
		}

		this.EndGame();
		this.StartGame();
	}

	public void RestartGame()
	{
		this.StartCoroutine(this.RestartGameProcess());
	}

	private void Awake()
	{
		Application.targetFrameRate = 60;
		QualitySettings.vSyncCount = 0;
	}

	private void Start()
	{
		this.StartGame();
	}
}
