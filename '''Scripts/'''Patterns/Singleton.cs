﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T: MonoBehaviour
{
    private static T instance;

    private static bool isApplicationQuitting = false;

    public static T Instance
    {
        get
        {
            if (isApplicationQuitting)
            {
                return null;
            }
            if (instance == null)
            {
                var objectsOnScene = GameObject.FindObjectsOfType<T>();

                if (objectsOnScene.Length == 0)
                {
                    var instanceGameObject = new GameObject();
                    instanceGameObject.name = AddSpacesToSentence(typeof(T).ToString(), true);
                    instance = instanceGameObject.AddComponent<T>();
                }
                else if (objectsOnScene.Length == 1)
                {
                    instance = objectsOnScene[0];
                }
                else
                {
                    instance = objectsOnScene[0];
                    for (int i = objectsOnScene.Length - 1; i > 0; i++)
                    {
                        GameObject.Destroy(objectsOnScene[i].gameObject);
                    }
                }
            }

            return instance;
        }
    }

    public void OnDestroy()
    {
        isApplicationQuitting = true;
    }

    private static string AddSpacesToSentence(string text, bool preserveAcronyms)
    {
        if (text == "")
            return "";
        System.Text.StringBuilder newText = new System.Text.StringBuilder(text.Length * 2);
        newText.Append(text[0]);
        for (int i = 1; i < text.Length; i++)
        {
            if (char.IsUpper(text[i]))
                if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                    (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                        i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                    newText.Append(' ');
            newText.Append(text[i]);
        }
        return newText.ToString();
    }
}
