﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class ComponentExtensions
{
    public static T GetComponentSafe<T>(this Component component)
        where T : Component
    {
        T resultComponent;
        return component.GetComponent<T>(out resultComponent) ? resultComponent : component.gameObject.AddComponent<T>();
    }

	public static bool GetComponent<T>(this Component component, out T resultComponent)
        where T : Component
    {
        resultComponent = component.GetComponent<T>();

        return resultComponent != null;
    }

    public static bool HasComponent<T>(this Component component)
        where T : Component
    {
        return component.GetComponent<T>() != null;
    }

	public static bool HasComponentInChildren<T>(this Component component)
		where T : Component
	{
		return component.GetComponentInChildren<T>() != null;
	}

	public static bool EnsureComponent<T>(this Component component)
        where T : Component
    {
        if (component.HasComponent<T>())
            return true;
        else
        {
            component.gameObject.AddComponent<T>();
            return false;
        }
    }

    public static void ActivateGameObject(this Component component)
    {
        component.gameObject.SetActive(true);
    }

    public static void ActivateGameObjects(this Component[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            components[i].ActivateGameObject();
        }
    }

    public static void ActivateGameObjectsOfType<T>(this Component[] components)
        where T : Component
    {
        for (int i = 0; i < components.Length; i++)
        {
            Component component = components[i].GetComponent<T>();
            if (component != null)
                component.ActivateGameObject();
        }
    }

    public static void DeactivateGameObject(this Component component)
    {
        component.gameObject.SetActive(false);
    }

    public static void DeactivateGameObjects(this Component[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            components[i].DeactivateGameObject();
        }
    }

    public static void DeactivateGameObjectsOfType<T>(this Component[] components)
        where T : Component
    {
        for (int i = 0; i < components.Length; i++)
        {
            Component component = components[i].GetComponent<T>();
            if (component != null)
                component.DeactivateGameObject();
        }
    }
}