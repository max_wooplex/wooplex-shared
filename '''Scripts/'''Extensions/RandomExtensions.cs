﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

public static class RandomExtensions
{
	public static void Shuffle<T>(this Random random, T[] array)
	{
		int n = array.Length;
		while (n > 1)
		{
			int k = Random.Range(0, n--);

			T temp = array[n];
			array[n] = array[k];
			array[k] = temp;
		}
	}

	public static void Shuffle<T>(this Random random, List<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			int k = Random.Range(0, n--);

			T temp = list[n];
			list[n] = list[k];
			list[k] = temp;
		}
	}

#if UNITY_EDITOR
#endif
}