﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class TransformExtensions
{
	public static void ResetPosition(this Transform transform)
	{
		transform.position = Vector3.zero;
	}

	public static void ResetLocalPosition(this Transform transform)
	{
		transform.localPosition = Vector3.zero;
	}

	public static void ResetRotation(this Transform transform)
	{
		transform.rotation = Quaternion.identity;
	}

	public static void ResetLocalRotation(this Transform transform)
	{
		transform.localRotation = Quaternion.identity;
	}

	public static void ResetLocalScale(this Transform transform)
	{
		transform.localScale = Vector3.one;
	}

	public static void ActivateChildren(this Transform transform)
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(true);
		}
	}

	public static void ActivateChildrenOfType<T>(this Transform transform)
		where T : Component
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			T component;
			if (transform.GetChild(i).GetComponent(out component))
				component.gameObject.SetActive(true);
		}
	}

	public static void DeactivateChildren(this Transform transform)
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	public static void DeactivateChildrenOfType<T>(this Transform transform)
		where T : Component
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			T component;
			if (transform.GetChild(i).GetComponent(out component))
				component.gameObject.SetActive(false);
		}
	}

	public static void ActivateChildrenRecursively(this Transform transform)
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);

			child.ActivateChildrenRecursively();
			child.gameObject.SetActive(true);
		}
	}

	public static void ActivateChildrenRecursivelyOfType<T>(this Transform transform)
		where T : Component
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);

			if (child.HasComponent<T>())
			{
				child.ActivateChildrenRecursivelyOfType<T>();
				child.gameObject.SetActive(true);
			}
		}
	}

	public static void DeactivateChildrenRecursively(this Transform transform)
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);

			child.DeactivateChildrenRecursively();
			child.gameObject.SetActive(false);
		}
	}

	public static void DeactivateChildrenRecursivelyOfType<T>(this Transform transform)
		where T : Component
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);

			if (child.HasComponent<T>())
			{
				child.DeactivateChildrenRecursivelyOfType<T>();
				child.gameObject.SetActive(false);
			}
		}
	}

	public static void DestroyChildren(this Transform transform)
	{
		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			GameObject.Destroy(transform.GetChild(i).gameObject);
		}
	}

	public static void DestroyChildrenOfType<T>(this Transform transform)
		where T : Component
	{
		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			T component;
			if (transform.GetChild(i).GetComponent(out component))
				GameObject.Destroy(component.gameObject);
		}
	}

	public static void DestroyChildrenWithTag(this Transform transform, string tag)
	{
		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			GameObject childGameObject = transform.GetChild(i).gameObject;
			if (childGameObject.CompareTag(tag))
				GameObject.Destroy(childGameObject);
		}
	}

	public static void DestroyChildrenImmediate(this Transform transform)
	{
		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
		}
	}

	public static void DestroyChildrenImmediateOfType<T>(this Transform transform)
		where T : Component
	{
		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			T component;
			if (transform.GetChild(i).GetComponent(out component))
				GameObject.DestroyImmediate(component.gameObject);
		}
	}

	public static void DestroyChildrenImmediateWithTag(this Transform transform, string tag)
	{
		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			GameObject childGameObject = transform.GetChild(i).gameObject;
			if (childGameObject.CompareTag(tag))
				GameObject.DestroyImmediate(childGameObject);
		}
	}

	//! Kryvo !!! Kostyl !!!
	public static IEnumerator DestroyChildrenFromEditModeProcess(this Transform transform)
	{
		List<GameObject> children = new List<GameObject>(transform.childCount);

		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			children.Add(transform.GetChild(i).gameObject);
		}

		yield return new WaitForEndOfFrame();

		for (int i = 0; i < children.Count; i++)
		{
			GameObject.DestroyImmediate(children[i]);
		}
	}

	public static IEnumerator DestroyChildrenFromEditModeOfTypeProcess<T>(this Transform transform)
		where T : Component
	{
		List<GameObject> children = new List<GameObject>(transform.childCount);

		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			T component;
			if (transform.GetChild(i).GetComponent(out component))
				children.Add(component.gameObject);
		}

		yield return new WaitForEndOfFrame();

		for (int i = 0; i < children.Count; i++)
		{
			GameObject.DestroyImmediate(children[i]);
		}
	}

	public static IEnumerator DestroyChildrenFromEditModeWithTagProcess(this Transform transform, string tag)
	{
		List<GameObject> children = new List<GameObject>(transform.childCount);

		for (int i = transform.childCount - 1; i >= 0; --i)
		{
			GameObject childGameObject = transform.GetChild(i).gameObject;
			if (childGameObject.CompareTag(tag))
				children.Add(childGameObject);
		}

		yield return new WaitForEndOfFrame();

		for (int i = 0; i < children.Count; i++)
		{
			GameObject.DestroyImmediate(children[i]);
		}
	}
}