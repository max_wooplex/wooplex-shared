﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumable : MonoBehaviour
{
    [SerializeField] private LayerMask _collisionProcessingLayerMask;

	[SerializeField] private AudioPack _collisionSFX;

	public abstract void Consume(IConsumer consumer);

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        if ((this._collisionProcessingLayerMask & (1 << collider2D.gameObject.layer)) > 0)
        {
            this.Consume(collider2D.GetComponent<IConsumer>());

			AudioPlayer.Instance.PlayOneShot(this._collisionSFX.GetRandom(), AudioPlayer.AudioType.SFX);

			Destroy(this.gameObject);
        }
    }
}
