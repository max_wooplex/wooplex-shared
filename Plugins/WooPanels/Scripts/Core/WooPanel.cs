﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;


#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

using System.Collections;
using UnityEngine.EventSystems;

namespace Wooplex.Panels
{
    public enum PanelType
    {
        Dependent,
        Independent
    }

    public enum PanelState
    {
        NotDefined,
        Opened,
        Closed,
        IsOpening,
        IsClosing,
        IsWaitingToOpen,
        IsWaitingToClose,
        IsBeingDragged
    }

    [System.Serializable]
    public class PanelProperties
    {
        [Header("When To Open")]
        [Tooltip("Opens on awake")]
        public bool OnAwake = false;
        [Tooltip("Opens whenever a parent panel in the hierarchy is opened")]
        public bool WithParent = false;
        [Tooltip("Opens whenever a child panel in the hierarchy is opened")]
        public bool WithChild = true;

        [Header("Animation")]
        //        [Tooltip ("If warmed up first animations won't be played")]
        //        public bool SkipFirstAnimation = false;

        [Range(0.01f, 5.0f)]
        [Tooltip("Animation speed multiplier")]
        public float OpeningSpeed = 1.0f;
        [Range(0.01f, 5.0f)]
        [Tooltip("Animation speed multiplier")]
        public float ClosingSpeed = 1.0f;

        [Header("Before Opening Wait For")]
        [Range(0.0f, 1.0f)]
        [Tooltip("Waits for all of the panels on the same level of hierarchy to close. Doesn't wait for Not Dependent Panels.")]
        public float SiblingsToClose = 0.0f;
        [Tooltip("Waits for the parent panel to open")]
        [Range(0.0f, 1.0f)]
        public float ParentToOpen = 0.0f;

        [Header("Before Closing Wait For")]
        [Range(0.0f, 1.0f)]
        [Tooltip("Waits for all of the children panels in hierarchy to close.")]
        public float ChildrenToClose = 0.0f;

        [Header("Extra")]
        [Tooltip("Sets as last sibling if always on top")]
        public bool AlwaysOnTop = false;

        [Tooltip("Whether the gameobject would remain active when closed. When 'false' the performance is better, because gameobjects are not rendered and updated.")]
        public bool ActiveWhenClosed = false;

        [Tooltip("When checked panels will ignore Unity’s Time.timeScale")]
        public bool IgnoreTimeScale = false;
    }

    [System.Serializable]
    public class Movement
    {
        public bool IsDraggable
        {
            get
            {
                return DraggableContent != null;
            }
        }

        public bool IsDragAllowed = true;

        public WooDraggableContent DraggableContent;

        [HideInInspector]
        [Tooltip("Starting Position")]
        public GameObject DragFrom;
        [HideInInspector]
        [Tooltip("Destination Position")]
        public GameObject DragTo;

        [HideInInspector]
        [Tooltip("Starting Anchor")]
        public GameObject DragFromAnchor;
        [HideInInspector]
        [Tooltip("Destination Anchor")]
        public GameObject DragToAnchor;
    }

    [RequireComponent(typeof(CanvasGroup), typeof(Animator))]
    public class WooPanel : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IEventBlocker
    {
        public string Title;

        public PanelType PanelType = PanelType.Dependent;

        public PanelProperties PanelProperties = new PanelProperties();
        public Movement Movement = new Movement();

        public UnityEvent OnPanelOpen = new UnityEvent();
        public UnityEvent OnPanelClose = new UnityEvent();
        public UnityEvent OnPanelOpenEnd = new UnityEvent();
        public UnityEvent OnPanelCloseEnd = new UnityEvent();

        private const string IS_OPENED = "IsOpened";
        private const string ON_OPEN = "OnOpen";
        private const string ON_CLOSE = "OnClose";
        private const string ON_OPEN_END = "OnOpenEnd";
        private const string ON_CLOSE_END = "OnCloseEnd";
        private const string ON_INIT = "OnInit";
        private const string PANEL_LAYER = "Panel";
        private const string IS_DEFINED = "IsDefined";
        private const string ON_PANEL_DESTROY = "OnPanelDestroy";

        private const string PANEL_UPDATE = "PanelUpdate";
        private const string DEFAULT_ANIMATOR_NAME = "WooPanels/Animators/Default Panel Controller";
        private const string LAYER_NAME = "Panel";
        private const string PANEL_OPENED_STATE = "PanelOpened";
        private const string PANEL_CLOSED_STATE = "PanelClosed";

        private Animator animator;
        private RectTransform rectTransform;

        internal PanelState PanelState = PanelState.NotDefined;

        private Coroutine waitingForOthersToCloseBeforeClosingCoroutine;
        private Coroutine closingCoroutine;
        private Coroutine waitingForOthersToCloseBeforeOpeningCoroutine;
        private Coroutine openingCoroutine;

        private bool InitCalled = false;

        public Vector3 StartDragPosition = Vector3.zero;
        public Vector3 EndDragPosition = new Vector3(0.0f, 300.0f, 0.0f);

        public WooPanel PreviousDependentPanel;
        [HideInInspector]
        public Canvas Canvas;

        public PanelState GetPanelState()
        {
            return PanelState;
        }

        public Vector3 WorldSpaceStartPosition
        {
            get
            {
                var result = Movement.DraggableContent.transform.position;

                if (Movement.DragFrom != null && Movement.DragFromAnchor != null)
                {
                    var distance = Movement.DragFromAnchor.transform.position - Movement.DraggableContent.transform.position;
                    return Movement.DragFrom.transform.position - distance;
                }
                return result;
            }
            set
            {
                Movement.DragFrom.transform.position = value;
            }
        }

        public Vector3 WorldSpaceEndPosition
        {
            get
            {
                var result = Movement.DraggableContent.transform.position;

                if (Movement.DragTo != null && Movement.DragToAnchor != null)
                {
                    var distance = Movement.DragToAnchor.transform.position - Movement.DraggableContent.transform.position;

                    return Movement.DragTo.transform.position - distance;
                }

                return result;
            }
            set
            {
                Movement.DragTo.transform.position = value;
            }
        }

        public WooPanel GetParentPanel()
        {
            return WooPanelsEngine.Instance.panelNodes[this].Parent != null ? WooPanelsEngine.Instance.panelNodes[this].Parent.Panel : null;
        }

        public void Init()
        {
            if (!InitCalled)
            {
                var curTransform = this.transform;

                List<GameObject> gameObjectsToTurnOff = new List<GameObject>();

                while (curTransform != null)
                {
                    if (!curTransform.gameObject.activeSelf)
                    {
                        gameObjectsToTurnOff.Add(curTransform.gameObject);
                    }
                    curTransform.gameObject.SetActive(true);
                    curTransform = curTransform.parent;
                }

                SafeSendMessage(ON_INIT, SendMessageOptions.DontRequireReceiver);

                //for (int i = 0; i < gameObjectsToTurnOff.Count; i++)
                //{
                //    gameObjectsToTurnOff[i].SetActive(false);
                //}

                InitCalled = true;
            }
        }

        public void SetCanvas()
        {
            Canvas = GetBelongingCanvas(this.gameObject);
        }

        protected override void OnEnable()
        {
            if (!Application.isPlaying)
            {
                RegisterIfNecessary();
            }
        }

        protected override void Awake()
        {
            RegisterIfNecessary();
            if (Movement.IsDraggable)
            {
                if (this.gameObject.GetComponent<WooGestureCapturer>() == null)
                {
                    this.gameObject.AddComponent<WooGestureCapturer>().CaptureType = GestureType.Any;
                }
            }
        }

		protected override void OnTransformParentChanged()
		{
            base.OnTransformParentChanged();

            if (WooPanelsEngine.Instance.panelNodes.ContainsKey(this))
            {
                WooPanelsEngine.Instance.RemovePanelFromEngine(this);
                RegisterIfNecessary();
            }
		}

		protected override void OnDestroy()
        {
            WooPanelsEngine.Instance.RemovePanelFromEngine(this);
        }

        private Canvas GetBelongingCanvas(GameObject go)
        {
            if (go.GetComponent<Canvas>() != null)
            {
                return go.GetComponent<Canvas>();
            }
            else
            {
                if (go.transform.parent != null)
                {
                    return GetBelongingCanvas(go.transform.parent.gameObject);
                }
            }

            return null;
        }

        private bool RegisterIfNecessary()
        {
            if (WooPanelsEngine.Instance != null)
            {
                return WooPanelsEngine.Instance.RegisterPanel(this);
            }

            return false;
        }

        public Animator GetAnimator()
        {
            if (animator == null)
            {
                animator = GetComponent<Animator>();
            }

            return animator;
        }

        public RectTransform GetRectTransform()
        {
            if (rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }

            return rectTransform;
        }

        public void Toggle()
        {
            if (!IsOpenedOrOpening())
            {
                Open();
            }
            else if (!IsClosedOrClosing())
            {
                Close();
            }
        }

        public void ToggleImmediately()
        {
            if (!IsOpenedOrOpening())
            {
                OpenImmediately();
            }
            else if (!IsClosedOrClosing())
            {
                CloseImmediately();
            }
        }

        public void Open()
        {
            PanelManager.Open(this);

            if (!Application.isPlaying && !WooPanelsEngine.CanBeAnimated)
            {
                WooPanelsEngine.Instance.FinishOpeningAndClosing();
            }
        }

        public void Close()
        {
            PanelManager.Close(this);

            PanelManager.StopDragging(this);

            if (!Application.isPlaying && !WooPanelsEngine.CanBeAnimated)
            {
                WooPanelsEngine.Instance.FinishOpeningAndClosing();
            }
        }

        public void CloseImmediately()
        {
            PanelManager.CloseImmediately(this);

            PanelManager.StopDragging(this);

        }

        public void OpenImmediately()
        {
            PanelManager.OpenImmediately(this);
        }

#if UNITY_EDITOR
        private AnimatorState GetState(AnimatorControllerLayer layer, string stateName)
        {
            var states = layer.stateMachine.states;

            for (int i = 0; i < states.Length; i++)
            {
                if (states[i].state.name == stateName)
                {
                    return states[i].state;
                }
            }

            return null;
        }
        private AnimatorControllerLayer GetLayer(Animator animator)
        {
            var layers = (animator.runtimeAnimatorController as AnimatorController).layers;

            for (int i = 0; i < layers.Length; i++)
            {
                if (layers[i].name == LAYER_NAME)
                {
                    return layers[i];
                }
            }

            return null;
        }

#endif
        public bool IsMirrored()
        {
#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                return GetState(GetLayer(GetAnimator()), PANEL_CLOSED_STATE).speed == -1.0f;
            }

#endif
            return GetAnimationLength(this.animator, "PanelClosed") == -1.0f;
        }

        private void SafeSendMessage(string message, SendMessageOptions options)
        {
            if (!Application.isPlaying)
            {
                return;
            }
            try
            {
                SendMessage(message, SendMessageOptions.DontRequireReceiver);
            }
            catch
            {

            }
        }

        internal void NotifyClosingBegin()
        {
            SafeSendMessage(ON_CLOSE, SendMessageOptions.DontRequireReceiver);
            if (OnPanelClose != null)
            {
                OnPanelClose.Invoke();
            }
#if UNITY_EDITOR
            EditorApplication.RepaintHierarchyWindow();
#endif
        }

        internal void NotifyClosingEnd()
        {
            SafeSendMessage(ON_CLOSE_END, SendMessageOptions.DontRequireReceiver);
            if (OnPanelCloseEnd != null)
            {
                OnPanelCloseEnd.Invoke();
            }
#if UNITY_EDITOR
            EditorApplication.RepaintHierarchyWindow();
#endif
        }


        internal void NotifyOpeningBegin()
        {
            SafeSendMessage(ON_OPEN, SendMessageOptions.DontRequireReceiver);
            if (OnPanelOpen != null)
            {
                OnPanelOpen.Invoke();
            }
#if UNITY_EDITOR
            EditorApplication.RepaintHierarchyWindow();
#endif
        }

        internal void NotifyOpeningEnd()
        {
            SafeSendMessage(ON_OPEN_END, SendMessageOptions.DontRequireReceiver);

            if (OnPanelOpenEnd != null)
            {
                OnPanelOpenEnd.Invoke();
            }
#if UNITY_EDITOR
            EditorApplication.RepaintHierarchyWindow();
#endif
        }

        public bool IsOpened()
        {
            return PanelState == PanelState.Opened;
        }

        public bool IsClosed()
        {
            return PanelState == PanelState.Closed;
        }

        public bool IsClosing()
        {
            return PanelState == PanelState.IsClosing;
        }

        public bool IsOpening()
        {
            return PanelState == PanelState.IsOpening;
        }

        internal bool IsNotDefined()
        {
            return PanelState == PanelState.NotDefined;
        }

        public bool IsWaitingToOpen()
        {
            return PanelState == PanelState.IsWaitingToOpen;
        }

        public bool IsWaitingToClose()
        {
            return PanelState == PanelState.IsWaitingToClose;
        }

        public bool IsBeingDragged()
        {
            return WooPanelsEngine.Instance.panelNodes[this].IsDraggedDirectly && WooPanelsEngine.Instance.panelNodes[this].IsBeingDragged;
            return PanelState == PanelState.IsBeingDragged;
        }

        internal bool IsClosedOrNotDefined()
        {
            return IsClosed() || IsNotDefined();
        }

        internal bool IsClosedOrClosing()
        {
            return IsClosed() || IsClosing();
        }

        internal bool IsOpenedOrOpening()
        {
            return IsOpened() || IsOpening();
        }

        private Vector2 previousPosition;
        private int pointerId = -1;
        private Vector2 previousPositionForAvgSpeed;
        private float previousTimeForAvgSpeed;
        private float avgSpeedTime = 0.15f;
        private float speedTresholdForOpeningClosing = 0.35f;

        void BaseUpdate()
        {
            if (IsOpenedOrOpening() || IsClosing())
            {
                SendMessage(PANEL_UPDATE, SendMessageOptions.DontRequireReceiver);
            }

            if (Movement.IsDraggable && !eventsBlocked && Movement.IsDragAllowed)
            {
                ProcessDrag();
            }
        }

        void ProcessDrag()
        {
            if (IsBeingDragged())
            {
                if (Input.touchCount == 0 && !Input.GetMouseButton(0))
                {
                    OnPointerUp(null);
                    return;
                }

                var newMousePosition = pointerId < 0 ? (Vector2)Input.mousePosition : Input.GetTouch(pointerId).position;
                var simulatedTimeToAdd = CalculateSimulatedTimeBasedOnDrag(previousPosition, newMousePosition);
                RecordPreviousPositions(newMousePosition);

                PanelManager.DragPanel(this, simulatedTimeToAdd);
            }
        }

        private void RecordPreviousPositions(Vector2 newPos)
        {
            previousPosition = newPos;

            if (previousTimeForAvgSpeed + avgSpeedTime <= Time.realtimeSinceStartup)
            {
                previousPositionForAvgSpeed = previousPosition;
                previousTimeForAvgSpeed = Time.realtimeSinceStartup;
            }
        }
        private float CalculateSimulatedTimeBasedOnDrag(Vector2 previousPos, Vector2 newPos)
        {
            var WorldSpaceStartPos = WorldSpaceStartPosition;
            var WorldSpaceEndPos = WorldSpaceEndPosition;
            var projectedDelta = Vector3.Project(newPos - previousPos, WorldSpaceEndPos - WorldSpaceStartPos);
            var angle = Vector3.Angle(projectedDelta, WorldSpaceEndPos - WorldSpaceStartPos);
            var simulatedTimeToAdd = projectedDelta.magnitude / (WorldSpaceEndPos - WorldSpaceStartPos).magnitude;
            simulatedTimeToAdd = angle > 1.0f ? -simulatedTimeToAdd : simulatedTimeToAdd;

            return simulatedTimeToAdd;
        }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            if (!Movement.IsDraggable || !Movement.IsDragAllowed)
            {
                return;
            }

            pointerId = eventData.pointerId;
            previousPosition = pointerId < 0 ? (Vector2)Input.mousePosition : Input.GetTouch(pointerId).position;
            previousPositionForAvgSpeed = previousPosition;
            previousTimeForAvgSpeed = Time.realtimeSinceStartup;

            PanelManager.StartDragging(this);
        }

        private void CloseOrOpenDependingOnAvarageSpeed()
        {
            var pos = pointerId < 0 ? (Vector2)Input.mousePosition : Input.GetTouch(pointerId).position;
            var simulatedTimeToAdd = CalculateSimulatedTimeBasedOnDrag(previousPositionForAvgSpeed, pos);
            var dt = Time.realtimeSinceStartup - previousTimeForAvgSpeed;
            var speed = simulatedTimeToAdd / (float)dt;
            WooPanelsEngine.Instance.panelNodes[this].Speed = speed;

            if (speed > speedTresholdForOpeningClosing)
            {
                Open();
            }
            else if (speed < -speedTresholdForOpeningClosing)
            {
                Close();
            }
            else
            {
                CloseOrOpenBasedOnSimulatedTime();
            }
        }

        private void CloseOrOpenBasedOnSimulatedTime()
        {
            if (WooPanelsEngine.Instance.panelNodes[this].SimulatedTime < 0.5f)
            {
                Close();
            }
            else
            {
                Open();
            }
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (!Movement.IsDraggable)
            {
                return;
            }

            if (IsBeingDragged())
            {
                StopDragging();
            }
        }

        private void StopDragging(bool closeBasedOnSpeed = true)
        {
            pointerId = -1;
            PanelManager.StopDragging(this);

            if (closeBasedOnSpeed)
            {
                CloseOrOpenDependingOnAvarageSpeed();
            }
            else
            {
                CloseOrOpenBasedOnSimulatedTime();
            }
        }

        private float GetAnimationLength(Animator anim, string track)
        {
            float length = -1.0f;
            if (anim != null && anim.runtimeAnimatorController != null)
            {
                for (int i = 0; i < anim.runtimeAnimatorController.animationClips.Length; i++)
                {
                    var animationClip = anim.runtimeAnimatorController.animationClips[i];
                    if (animationClip.name == track)
                    {
                        length = animationClip.length;
                    }
                }
            }

            return length;
        }

        public float GetNormalizedAnimationTime()
        {
            var time = GetCurrentAnimatorStateInfo().normalizedTime;
            time = Mathf.Min(time, 1.0f);
            if (IsMirrored() && IsClosedOrClosing())
            {
                time = 1.0f - time;
            }

            return time;
        }

        public AnimatorStateInfo GetCurrentAnimatorStateInfo()
        {
            AnimatorStateInfo result = new AnimatorStateInfo(); ;
            if (Application.isPlaying)
            {
                result = GetAnimator().GetCurrentAnimatorStateInfo(GetLayerIndex());
            }

            return result;
        }


        public int GetLayerIndex()
        {
            GetAnimator().logWarnings = false;
            int index = GetAnimator().GetLayerIndex(PANEL_LAYER);
            GetAnimator().logWarnings = true;
            return index;
        }

        private bool eventsBlocked = false;

        public void BlockEvents(WooGestureCapturer gestureCapturer)
        {
            if (gestureCapturer.gameObject != this.gameObject)
            {
                if (IsBeingDragged())
                {
                    eventsBlocked = true;
                    StopDragging(false);
                }

            }
        }

        public void UnblockEvents(WooGestureCapturer gestureCapturer)
        {
            if (gestureCapturer.gameObject != this.gameObject)
            {
                eventsBlocked = false;
            }
        }
    }
}
