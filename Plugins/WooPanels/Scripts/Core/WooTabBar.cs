using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;

public class WooTabBar : WooPanel
{
	// At the beginning even if the panel is closed 
	void OnInit ()
	{
		
	}

	// Before animation starts
	void OnOpen ()
	{
		
	}

	// After animation finishes
	void OnOpenEnd ()
	{
		
	}

	// Before animation starts
	void OnClose ()
	{
		
	}

	// After animation finishes
	void OnCloseEnd ()
	{
		
	}

	// Every frame when the panel is opened or being opened
	void PanelUpdate ()
	{
		
	}
}
