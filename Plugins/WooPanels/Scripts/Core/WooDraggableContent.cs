﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wooplex.Panels;

public enum WooSnapPosition
{
    Left,
    Right,
    Top,
    Bottom,
    Middle
}

public class WooDraggableContent : MonoBehaviour 
{
    [HideInInspector]
    public WooPanel Panel;

    [Range (0.0f, 1.0f)]
    public float Inertia = 1.0f;

    [System.NonSerialized]
    public float MaxVelocity = 3.0f;
    [System.NonSerialized]
    public float SlowingRadius = 0.4f;
    [System.NonSerialized]
    public float BreakingForce = 4.0f;
}
