﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Wooplex.Panels;

[RequireComponent(typeof(WooEventsPropagator))]
public class WooTabButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IEventBlocker
{
    public WooPanel Panel;

    public bool SwapSprites = false;
    public Sprite OnSprite;
    public Sprite OffSprite;

    public bool SwapColors = false;
    public Color OnColor;
    public Color OffColor;
    public Color PressedColor;

    public bool SwapText = false;
    public string OnText = "Panel On";
    public string OffText = "Panel Off";

    public bool IsOn;

    public Image TargetGraphic;
    public Text TargetText;

    public bool SelfClickToClose = false;
    private bool isButtonEnabled = true;

    void Start()
    {
        if (Panel != null)
        {
            Panel.OnPanelOpen.AddListener(ChangeState);
            Panel.OnPanelClose.AddListener(ChangeState);

            if (TargetText.text != null)
            {
                TargetText.text = Panel.Title;
            }
        }
        else
        {
            //Debug.LogWarning("WooTabButton should have a Panel assigned. Please, check");
        }

        ChangeState();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (TargetGraphic != null)
        {
            TargetGraphic.color = PressedColor;
        }

        if (TargetText != null)
        {
            TargetText.color = PressedColor;
        }
    }

    private void ProcessState()
    {
        if (IsOn)
        {
            if (SwapSprites)
            {
                if (TargetGraphic != null)
                {
                    TargetGraphic.sprite = OnSprite;
                }
            }

            if (SwapColors)
            {
                if (TargetGraphic != null)
                {
                    TargetGraphic.color = OnColor;
                }

                if (TargetText != null)
                {
                    TargetText.color = OnColor;
                }
            }

            if (SwapText)
            {
                if (TargetText != null)
                {
                    TargetText.text = OnText;
                }
            }
        }
        else
        {
            if (SwapSprites)
            {
                if (TargetGraphic != null)
                {
                    TargetGraphic.sprite = OffSprite;
                }
            }

            if (SwapColors)
            {
                if (TargetGraphic != null)
                {
                    TargetGraphic.color = OffColor;
                }

                if (TargetText != null)
                {
                    TargetText.color = OffColor;
                }
            }

            if (SwapText)
            {
                if (TargetText != null)
                {
                    TargetText.text = OffText;
                }
            }
        }
    }

    public void ChangeState()
    {
        if (Panel != null)
        {
            IsOn = Panel.IsOpening() || Panel.IsWaitingToOpen() || Panel.IsOpened();
        }

        ProcessState();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (Panel != null && isButtonEnabled)
        {
            if (SelfClickToClose)
            {
                Panel.Toggle();
            }
            else
            {
                Panel.Open();
            }
        }

        ChangeState();
    }

    public void BlockEvents(WooGestureCapturer gestureCapturer)
    {
        isButtonEnabled = false;
    }

    public void UnblockEvents(WooGestureCapturer gestureCapturer)
    {
        isButtonEnabled = true;
    }
}