﻿//Version 1.1 (Build 3)
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;
using System;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wooplex.Panels
{
    public class WooPanelsEngine : WooPanelsEngineCore
    {
        private static WooPanelsEngine instance;
        public static bool CanBeAnimated = true;
        public static WooPanelsEngine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WooPanelsEngine();


                    instance.TryToRebuildIfNecessary();

                }

                return instance;
            }
        }
        private const float SMALL_NUMBER = 0.0001f;

        float prevScreenWidth = -1.0f;
        float prevScreenHeight = -1.0f;

        public void FinishOpeningAndClosingForPanel(WooPanel panel)
        {
            panel.gameObject.SetActive(true);

            foreach (var childPanel in panelNodes[panel].Children)
            {
                FinishOpeningAndClosingForPanel(childPanel.Panel);
            }

            if (panelNodes[panel].IsOpening || panelNodes[panel].IsOpen || panelNodes[panel].IsWaitingToOpen)
            {
                panelNodes[panel].PerformOpening();
            }
            else if (panelNodes[panel].IsClosing || panelNodes[panel].IsClosed || panelNodes[panel].IsWaitingToClose)
            {
                panelNodes[panel].PerformClosing(false);
            }
        }

        public void FinishOpeningAndClosing()
        {
            foreach (var rootNodes in allRootNodes)
            {
                foreach (var root in rootNodes.Value)
                {
                    FinishOpeningAndClosingForPanel(root.Panel);
                }
            }
        }

        public void PlayOpenAnimationImmdiately(WooPanel panel, bool notify = true)
        {
            panelNodes[panel].SetSimulatedTimeManually(1.0f);
        }

        public void PlayCloseAnimationImmdiately(WooPanel panel, bool notify = true)
        {
            panelNodes[panel].SetSimulatedTimeManually(0.0f);
        }

        private void CloseDependentPanels(WooPanel panel)
        {
            if (panel.PanelType == PanelType.Dependent)
            {
                foreach (var sibling in panelNodes[panel].GetSiblings())
                {
                    if (sibling.Panel != panel && sibling.Panel.PanelType == PanelType.Dependent && sibling.Panel.IsOpenedOrOpening())
                    {
                        panel.PreviousDependentPanel = sibling.Panel;
                    }
                }
                foreach (var sibling in panelNodes[panel].GetSiblings())
                {
                    if (sibling.Panel != panel && sibling.Panel.PanelType == PanelType.Dependent)
                    {
                        sibling.Panel.Close();
                    }
                }
            }
        }

        private void CloseDependentPanelsImmediately(WooPanel panel)
        {
            if (panel.PanelType == PanelType.Dependent)
            {
                foreach (var sibling in panelNodes[panel].GetSiblings())
                {
                    if (sibling.Panel != panel && sibling.Panel.PanelType == PanelType.Dependent && sibling.Panel.IsOpenedOrOpening())
                    {
                        panel.PreviousDependentPanel = sibling.Panel;
                    }
                }
                foreach (var sibling in panelNodes[panel].GetSiblings())
                {
                    if (sibling.Panel != panel && sibling.Panel.PanelType == PanelType.Dependent)
                    {
                        sibling.Panel.CloseImmediately();
                    }
                }
            }
        }

        private void OpenChildren(WooPanel panel, bool openDependentChildren = true)
        {
            foreach (var child in panelNodes[panel].Children)
            {
                if (child.Panel.PanelProperties.WithParent)
                {
                    if (!openDependentChildren && child.Panel.PanelType == PanelType.Dependent)
                    {
                        continue;
                    }

                    Open(child.Panel);
                }
            }
        }

        private void OpenChildrenImmediately(WooPanel panel, bool openDependentChildren = true)
        {
            foreach (var child in panelNodes[panel].Children)
            {
                if (child.Panel.PanelProperties.WithParent)
                {
                    if (!openDependentChildren && child.Panel.PanelType == PanelType.Dependent)
                    {
                        continue;
                    }

                    OpenImmediately(child.Panel);
                }
            }
        }

        public void Open(WooPanel panel, bool shouldOpenChildren = true)
        {
#if UNITY_EDITOR

#endif
            if (!(panel.IsOpenedOrOpening() || panel.IsWaitingToOpen()))
            {
                CloseDependentPanels(panel);

                if (panelNodes[panel].Parent != null && panelNodes[panel].Parent.Panel.PanelProperties.WithChild)
                {
                    Open(panelNodes[panel].Parent.Panel, shouldOpenChildren && panel.PanelType == PanelType.Independent);
                }

                panelNodes[panel].CheckIfCanBeOpenedOrShouldBeWaiting();

                OpenChildren(panel, shouldOpenChildren);
            }
        }

        public void Close(WooPanel panel, bool immediate = false, bool notify = true)
        {
            if (!(panel.IsClosedOrClosing()))
            {
                panelNodes[panel].CheckIfCanBeClosedOrShouldBeWaiting();

                foreach (var child in panelNodes[panel].Children)
                {
                    child.Panel.Close();
                }
            }
        }

        public void Toggle(WooPanel panel, bool immediate = false, bool notify = true)
        {
            if (panel.IsOpenedOrOpening())
            {
                Close(panel, immediate, notify);
            }
            else
            {
                Open(panel);
            }
        }
        public void RewindPanelAnimation(WooPanel panel)
        {
            panelNodes[panel].SimulatedTime = 1.0f - Mathf.Min(panelNodes[panel].SimulatedTime, 1.0f);
        }

        public void RewindToEndPanelAnimation(WooPanel panel)
        {
            panelNodes[panel].SimulatedTime = 1.0f;
        }

        public void OpenNotAnimated(WooPanel panel, bool notify = true, WooPanel target = null)
        {

        }

        public bool CanBeOpened(WooPanel panel)
        {
            return true;
        }


        public void OpenAnimated(WooPanel panel, bool notify = true, WooPanel target = null)
        {

        }


        public void CloseAll(bool notify = true)
        {
            foreach (var root in allRootNodes)
            {
                foreach (var child in root.Value)
                {
                    CloseImmediately(child.Panel);
                }
            }
        }

        public void OpenAllOnAwake(bool immediate = false, bool notify = true)
        {
            foreach (var panel in panelNodes)
            {
                if (panel.Value.Panel.PanelProperties.OnAwake)
                {
                    Open(panel.Value.Panel);
                }
            }

            if (!Application.isPlaying && !CanBeAnimated)
            {
                FinishOpeningAndClosing();
            }
        }

        public void OpenImmediately(WooPanel panel, bool shouldOpenChildren = true)
        {
            if (!(panel.IsOpenedOrOpening() || panel.IsWaitingToOpen()))
            {
                CloseDependentPanelsImmediately(panel);

                panelNodes[panel].CheckIfCanBeOpenedOrShouldBeWaiting();

                if (panelNodes[panel].Parent != null && panelNodes[panel].Parent.Panel.PanelProperties.WithChild)
                {
                    OpenImmediately(panelNodes[panel].Parent.Panel, shouldOpenChildren && panel.PanelType == PanelType.Independent);
                }

                OpenChildrenImmediately(panel, shouldOpenChildren);

            }

            WooPanelsEngine.Instance.FinishOpeningAndClosingForPanel(panel);
        }

        public void CloseImmediately(WooPanel panel, bool immediate = false, bool notify = true)
        {
            if (!(panel.IsClosedOrClosing()))
            {
                panelNodes[panel].CheckIfCanBeClosedOrShouldBeWaiting();

                foreach (var child in panelNodes[panel].Children)
                {
                    child.Panel.CloseImmediately();
                }
            }

            WooPanelsEngine.Instance.FinishOpeningAndClosingForPanel(panel);
        }

        public void ToggleImmediately(WooPanel panel, bool immediate = false, bool notify = true)
        {
            if (panel.IsOpenedOrOpening())
            {
                CloseImmediately(panel, immediate, notify);
            }
            else
            {
                OpenImmediately(panel);
            }
        }

        public void Animate()
        {
            bool changed = false;
            if (!Application.isPlaying)
            {
                changed = prevScreenHeight != Camera.main.pixelHeight || prevScreenWidth != Camera.main.pixelWidth;
                prevScreenHeight = Camera.main.pixelHeight;
                prevScreenWidth = Camera.main.pixelWidth;
            }


            for (int i = 0; i < panelNodes.Count; i++)
            {
                var node = panelNodes.ElementAt(i);

                if (node.Value.IsBeingDragged)
                {
                    continue;
                }

                float deltaTime = node.Value.Panel.PanelProperties.IgnoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;

                if (!Application.isPlaying)
                {
                    deltaTime = (float)(DateTime.Now.Subtract(prevTime).TotalMilliseconds / 1000.0f);
                }

                if (node.Value.IsWaitingToOpen)
                {
                    node.Value.CheckIfCanBeOpenedOrShouldBeWaiting();
                }
                if (node.Value.IsWaitingToClose)
                {
                    node.Value.CheckIfCanBeClosedOrShouldBeWaiting();
                }

                changed = (node.Value.IsOpen && Mathf.Abs(node.Value.SimulatedTime - 1.0f) > SMALL_NUMBER) ||
                    (node.Value.IsClosed && Mathf.Abs(node.Value.SimulatedTime) > SMALL_NUMBER);
                
                if (node.Value.IsOpening || node.Value.IsClosing || changed)
                {
                    if (node.Value.Panel.Movement.IsDraggable)
                    {
                        var target = node.Value.Panel.IsOpenedOrOpening() ? 1.0f : 0.0f;
                        var slowingRadius = node.Value.Panel.Movement.DraggableContent.SlowingRadius;
                        var maxVelocity = node.Value.Panel.Movement.DraggableContent.MaxVelocity;
                        var mass = (node.Value.Panel.Movement.DraggableContent.Inertia + 0.1f) * 10.0f;

                        var desiredVelocity = Mathf.Sign(target - node.Value.SimulatedTime);
                        var distance = Mathf.Abs(target - node.Value.SimulatedTime);

                        if (distance < slowingRadius)
                        {
                            desiredVelocity = desiredVelocity * maxVelocity * (distance / slowingRadius);
                        }
                        else
                        {
                            desiredVelocity = desiredVelocity * maxVelocity;
                        }

                        var steering = desiredVelocity - node.Value.Speed;

                        var acceleration = steering / mass;

                        var newSpeed = node.Value.Speed + acceleration;

                        node.Value.Speed = node.Value.Speed + acceleration;
                        node.Value.SimulatedTime += node.Value.Speed * deltaTime;
                    }
                    else
                    {
                        var mod = node.Value.IsClosing ? node.Value.Panel.PanelProperties.ClosingSpeed : 1.0f;
                        mod = node.Value.IsOpening ? node.Value.Panel.PanelProperties.OpeningSpeed : mod;
                        var activeAnimation = node.Value.GetActiveAnimation();
                        var sign = node.Value.IsOpening ? 1.0f : -1.0f;

                        if (activeAnimation != null)
                        {
                            node.Value.SimulatedTime += sign * deltaTime / (node.Value.GetActiveAnimation().length / mod);
                        }
                    }
                }

                if (node.Value.IsWaiting)
                {
                    node.Value.remainingWaitingTime -= deltaTime;
                    node.Value.remainingWaitingTime = Mathf.Max(node.Value.remainingWaitingTime, 0.0f);
                }

                node.Value.SamplePanelAnimator(changed);
            }
            prevTime = DateTime.Now;

            foreach (var node in panelNodes)
            {
                node.Value.IsTimeSet = false;
            }
        }

#if UNITY_EDITOR
        WooSceneViewWindow sceneViewWindow;
        bool showMenu = false;
        SceneView sceneView = null;

        private WooPanelsEngine()
        {
            EditorApplication.playModeStateChanged += OnPlaymodeChanged;
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;

            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                EditorApplication.hierarchyWindowChanged += Rebuild;
            }

            UnityEditor.SceneView.onSceneGUIDelegate += OnSceneGUIDelegate;
            EditorApplication.update += OnGameUpdate;


            sceneViewWindow = new WooPanelSceneViewWindow();
        }

        void OnGameUpdate()
        {
            if (!Application.isPlaying)
            {
                WooPanelsEngine.Instance.Animate();
            }
        }

        void OnSceneGUIDelegate(UnityEditor.SceneView sceneView)
        {
            var selectedGo = Selection.activeGameObject;
            if (selectedGo != null)
            {
                var target = this.GetSelectedPanel(selectedGo.transform);

                if (target)
                {
                    sceneViewWindow.Show(sceneView, selectedGo);
                }
            }
        }

        static void HierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            var panels = WooPanelsEngine.Instance.panelIds;
            if (panels == null)
            {
                return;
            }
            var concretePanel = panels.ContainsKey(instanceID) ? panels[instanceID] : null;

            if (concretePanel != null)
            {
                var prevColor2 = Handles.color;
                var rect = selectionRect;
                var buttonRect = new Rect(rect.position.x + rect.width - 15.0f, rect.position.y + 1.0f, 13.0f, 13.0f);

                var prevColor = GUI.color;
                var fadedPrevColor = prevColor;
                fadedPrevColor.a = 0.5f;

                GUI.color = concretePanel.IsOpened() ? Color.green : concretePanel.IsOpenedOrOpening() ? new Color(1.0f, 0.7f, 0.2f) : concretePanel.IsClosing() ? fadedPrevColor : prevColor;

                if (GUI.Button(buttonRect, concretePanel.IsOpened() ? "" : ""))
                {
                    //Selection.activeInstanceID = instanceID;
                    Instance.Toggle(concretePanel);
                    if (!Application.isPlaying && !CanBeAnimated)
                    {
                        Instance.FinishOpeningAndClosing();
                    }
                }

                GUI.color = prevColor;
            }
        }
        private void OnPlaymodeChanged(PlayModeStateChange stateChange)
        {
            if (!Application.isPlaying)
            {
                Instance.Rebuild();
                Instance.CloseAll(false);
                Instance.OpenAllOnAwake(true);
            }

            if (stateChange == PlayModeStateChange.ExitingPlayMode)
            {
                EditorApplication.hierarchyWindowChanged += Rebuild;
            }
        }
#endif
    }
}
