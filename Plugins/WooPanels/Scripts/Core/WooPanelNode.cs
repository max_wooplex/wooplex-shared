﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wooplex.Panels
{
    public class WooPanelNode
    {
        public WooPanel Panel;

        public bool IsWaiting
        {
            get
            {
                return IsWaitingToOpen || IsWaitingToClose;
            }
        }

        public bool IsWaitingToOpen
        {
            get
            {
                return Panel.PanelState == PanelState.IsWaitingToOpen;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.IsWaitingToOpen;
                }

            }
        }

        public bool IsWaitingToClose
        {
            get
            {
                return Panel.PanelState == PanelState.IsWaitingToClose;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.IsWaitingToClose;
                }
            }
        }

        public bool IsOpening
        {
            get
            {
                return Panel.PanelState == PanelState.IsOpening;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.IsOpening;

                    Panel.GetAnimator().enabled = true;    
                }
            }
        }

        public bool IsClosing
        {
            get
            {
                return Panel.PanelState == PanelState.IsClosing;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.IsClosing;

                    Panel.GetAnimator().enabled = true;
                }
            }
        }

        public bool IsOpen
        {
            get
            {
                return Panel.PanelState == PanelState.Opened;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.Opened;

                    Panel.GetAnimator().enabled = false;
                }

            }
        }

        public bool IsClosed
        {
            get
            {
                return Panel.PanelState == PanelState.Closed;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.Closed;

                    Panel.GetAnimator().enabled = false;
                }

            }
        }

        public bool NotDefined
        {
            get
            {
                return Panel.PanelState == PanelState.NotDefined;
            }
            set
            {
                if (value)
                {
                    Panel.PanelState = PanelState.NotDefined;
                }
            }
        }

        private bool isBeingDragged;

        public bool IsBeingDragged
        {
            get
            {
                return isBeingDragged;
            }
            set
            {
                isBeingDragged = value;
            }
        }


        public float SimulatedTime
        {
            get
            {
                return simulatedTime;
            }

            set
            {
                simulatedTime = value;

                CheckIfPanelIsNowOpened();
                CheckIfPanelIsNowClosed();
            }
        }

        public Vector3 StartDragPosition
        {
            get
            {
                return Panel.StartDragPosition;
            }

            set
            {
                Panel.StartDragPosition = value;
            }
        }

        public Vector3 EndDragPosition
        {
            get
            {
                return Panel.EndDragPosition;
            }

            set
            {
                EndDragPosition = value;
            }
        }

        public RectTransform Content
        {
            get
            {
                return Panel.Movement.DraggableContent != null ? Panel.Movement.DraggableContent.GetComponent<RectTransform>() : null;
            }
        }

        public float Speed;
        private float BreakingForce = 4.0f;

        public bool IsTimeSet = false;

        public bool Collapsed = false;
        public int HierarchyIndex = 0;
        public float remainingWaitingTime = 0.0f;
        public float waitingTime = 0.0f;
        public bool IsDraggedDirectly = false;
        public List<WooPanelNode> Children = new List<WooPanelNode>();
        public WooPanelNode Parent;
        public Transform ParentTransform;

        private float simulatedTime = 0.0f;
        private const float SMALL_NUMBER = 0.0001f;

        internal bool ForceMirrored = false;
        private bool IsAffectedByDrag = false;

        public void StartDragging(WooPanelNode fromPanelNode = null, bool enableChildren = true)
        {
            if (IsBeingDragged)
            {
                return;
            }

            IsBeingDragged = true;
            Speed = 0.0f;
            IsDraggedDirectly = fromPanelNode == null;

            if (Parent != null && Parent.Panel.PanelProperties.WithChild)
            { 
                // avoid infinite recursion
                if (Parent != fromPanelNode)
                {
                    Parent.StartDragging(this, false);
                }
            }

            if (enableChildren)
            {
                foreach (var child in Children)
                {
                    if (child != fromPanelNode)
                    {
                        child.StartDragging(this);
                    }
                }

            }

            if (this.Panel.PanelType == PanelType.Dependent)
            {
                foreach (var sibling in GetSiblings())
                {
                    if (sibling != this && sibling.Panel.PanelType == PanelType.Dependent && !sibling.Panel.IsClosedOrClosing())
                    {
                        sibling.StartDragging(this);
                    }
                }
            }
        }

        public void StopDragging(WooPanelNode fromPanelNode = null)
        {
            if (!IsBeingDragged)
            {
                return;
            }

            IsBeingDragged = false;
            IsAffectedByDrag = false;

            if (Parent != null && Parent.Panel.PanelProperties.WithChild)
            {
                // avoid infinite recursion
                if (Parent != fromPanelNode)
                {
                    Parent.StopDragging(this);
                }
            }
            foreach (var child in Children)
            {
                if (child != fromPanelNode)
                {
                    child.StopDragging(this);
                }
            }

            if (this.Panel.PanelType == PanelType.Dependent)
            {
                foreach (var sibling in GetSiblings())
                {
                    if (sibling != fromPanelNode && sibling != this && sibling.Panel.PanelType == PanelType.Dependent)
                    {
                        sibling.StopDragging(this);
                    }
                }
            }
        }

        public List<WooPanelNode> GetSiblings()
        {
            var parent = Parent;

            List<WooPanelNode> siblings;

            if (parent != null)
            {
                siblings = WooPanelsEngine.Instance.GetChildren(parent.Panel);
            }
            else
            {
                siblings = WooPanelsEngine.Instance.GetRootNodes(Panel);

            }

            return siblings;
        }

        private void SimulateParent(float deltaTime, bool normalizedTime = false)
        {
            if (Parent != null && Parent.Panel.PanelProperties.WithChild)
            {
                Parent.SetSimulatedTimeManuallyFromPanel(deltaTime, this, false, normalizedTime);
            }
        }

        private void SetPreviousPanelForSiblings()
        {
            foreach (var sibling in GetSiblings())
            {
                if (sibling.Panel != Panel && sibling.Panel.PanelType == PanelType.Dependent && sibling.Panel.IsOpenedOrOpening())
                {
                    Panel.PreviousDependentPanel = sibling.Panel;
                }
            }
        }

        private void SimulateAndSetPreviousPanelsForSiblingsIfDependent(float deltaTime, WooPanelNode fromPanelNode, bool openChildren, bool normalizedTime = false)
        {
            if (this.Panel.PanelType == PanelType.Dependent)
            {
                SetPreviousPanelForSiblings();

                SimulateDependentPanels((fromPanelNode == this || !openChildren) ? -deltaTime : deltaTime, normalizedTime);
            }
        }

        public void ProcessOpening(float deltaTime, WooPanelNode fromPanelNode, bool openChildren, bool normalizedTime)
        {
            CheckIfCanBeOpenedOrShouldBeWaiting();
            SimulateParent(deltaTime, normalizedTime);
            SimulateChildren(deltaTime, normalizedTime);
            SimulateAndSetPreviousPanelsForSiblingsIfDependent(deltaTime, fromPanelNode, openChildren, normalizedTime);

            if (IsOpening || IsClosed || Panel.IsBeingDragged())
            {
                var time = normalizedTime ? deltaTime : (deltaTime / GetActiveAnimationLengthWithModifier());
                SimulatedTime += time;
                IsAffectedByDrag = true;
            }
        }

        private void ProcessClosing(float deltaTime, WooPanelNode fromPanelNode, bool normalizedTime)
        {
            if (!Panel.IsClosedOrClosing())
            {
                CheckIfCanBeClosedOrShouldBeWaiting();
            }

            foreach (var child in Children)
            {
                child.SetSimulatedTimeManuallyFromPanel(deltaTime, this, true, normalizedTime);
            }

            if (this.Panel.PanelType == PanelType.Dependent)
            {
                SimulateDependentPanels(fromPanelNode == this ? -deltaTime : deltaTime, normalizedTime);
            }

            if (IsClosing || IsOpen || Panel.IsBeingDragged())
            {
                var time = normalizedTime ? deltaTime : (deltaTime / GetActiveAnimationLengthWithModifier());
                SimulatedTime += time;
                IsAffectedByDrag = true;
            }

            CheckIfPanelIsNowClosed();
        }

        private void SimulateDependentPanels(float deltaTime, bool normalizedTime)
        {
            if (Panel.PanelType == PanelType.Dependent)
            {
                foreach (var sibling in GetSiblings())
                {
                    if (sibling.Panel != Panel && sibling.Panel.PanelType == PanelType.Dependent && sibling.Panel.PanelState == PanelState.Closed)
                    {
                        sibling.IsTimeSet = true;
                    }
                    else if (sibling.Panel != Panel && sibling.Panel.PanelType == PanelType.Dependent)
                    {
                        sibling.SetSimulatedTimeManuallyFromPanel(deltaTime, this, true, normalizedTime);
                    }
                }
            }
        }

        public void CheckIfCanBeClosedOrShouldBeWaiting()
        {
            if (Panel.IsClosed())
            {
                return;
            }

            var readyToClose = true;

            foreach (var child in Children)
            {
                if (child.SimulatedTime > 1.0f - Panel.PanelProperties.ChildrenToClose)
                {
                    readyToClose = false;
                }
            }
            var prevPanelState = Panel.PanelState;

            if (readyToClose)
            {
                IsClosing = true;

                Panel.GetAnimator().enabled = true;
            }
            else
            {
                IsWaitingToClose = true;
            }

            if (Panel.IsClosing() && Panel.PanelState != prevPanelState)
            {
                Panel.NotifyClosingBegin();
            }
        }

        public void CheckIfCanBeOpenedOrShouldBeWaiting()
        {
            if (Panel.IsOpened())
            {
                return;
            }

            var maxSiblingsTime = GetMaxSimulatedTimeInDependentSiblings();
            var previousState = Panel.PanelState;

            if (Parent != null && Parent.SimulatedTime < Panel.PanelProperties.ParentToOpen)
            {
                IsWaitingToOpen = true;
            }
            else if (maxSiblingsTime > (1.0f - Panel.PanelProperties.SiblingsToClose) && maxSiblingsTime <= 1.0f)
            {
                IsWaitingToOpen = true;
            }
            else
            {
                IsOpening = true;
            }

            if (!Panel.gameObject.activeSelf)
            {
                Panel.gameObject.SetActive(true);
            }
             

            if (Panel.IsOpening() && previousState != Panel.PanelState)
            {
                WooPanelsEngine.Instance.ProcessAlwaysOnTop(Panel);
                Panel.NotifyOpeningBegin();
            }
        }

        private void SimulateChildren(float deltaTime, bool normalizedTime)
        {
            bool canSimulate = true;

            if (deltaTime > 0)
            {
                foreach (var child in Children)
                {
                    if (child.Panel.PanelType == PanelType.Dependent && (child.IsOpen || child.IsAffectedByDrag))
                    {
                        canSimulate = false;
                    }
                }
            }

            foreach (var child in Children)
            {
                if (child.Panel.PanelType == PanelType.Dependent && !(canSimulate || child.IsAffectedByDrag))
                {
                    continue;
                }

                if (child.Panel.PanelProperties.WithParent || child.IsAffectedByDrag)
                {
                    child.SetSimulatedTimeManuallyFromPanel(deltaTime, this, true, normalizedTime);
                }
            }
        }

        public float GetMaxSimulatedTimeInDependentSiblings()
        {
            var maxSimulatedTime = 0.0f;

            if (Panel.PanelType == PanelType.Dependent)
            {
                foreach (var sibling in GetSiblings())
                {
                    if (sibling.Panel != Panel && sibling.Panel.PanelType == PanelType.Dependent && sibling.SimulatedTime > maxSimulatedTime)
                    {
                        maxSimulatedTime = sibling.SimulatedTime;
                    }
                }
            }

            return maxSimulatedTime;
        }

        private void CheckIfPanelIsNowOpened()
        {
            if ((this.IsOpening || this.IsWaitingToOpen))
            {
                if (
                    (!Panel.Movement.IsDraggable && simulatedTime >= (1.0f - SMALL_NUMBER)) ||
                    (IsBeingDragged && !IsDraggedDirectly && simulatedTime >= (1.0f - SMALL_NUMBER)) ||
                    (Panel.Movement.IsDraggable && Mathf.Abs(simulatedTime - 1.0f) < SMALL_NUMBER && Speed < SMALL_NUMBER)
                   )
                {
                    this.IsOpen = true;
                    simulatedTime = 1.0f;
                    Panel.NotifyOpeningEnd();
                }
            }
        }

        public void PerformOpening()
        {
            simulatedTime = 1.0f;
            SamplePanelAnimator(true);
            this.IsOpen = true;
            Panel.NotifyOpeningEnd();
        }

        private void CheckIfPanelIsNowClosed()
        {
            if (this.IsClosing || this.IsWaitingToClose)
            {
                if (
                    (!Panel.Movement.IsDraggable && simulatedTime < SMALL_NUMBER) ||
                    (IsBeingDragged && !IsDraggedDirectly && simulatedTime < SMALL_NUMBER) ||
                    (Panel.Movement.IsDraggable && Mathf.Abs(simulatedTime) < SMALL_NUMBER && Speed < SMALL_NUMBER)
                   )
                {
                    PerformClosing();
                }
            }
        }

        public void PerformClosing(bool closeChildren = true)
        {
            if (closeChildren)
            {
                foreach (var child in Children)
                {
                    child.Panel.Close();
                }
            }

            if (!isBeingDragged || (isBeingDragged && !IsDraggedDirectly))
            {
                simulatedTime = 0.0f;
            }

            SamplePanelAnimator(true);
            this.IsClosed = true;

            Panel.GetAnimator().enabled = false;
            Panel.NotifyClosingEnd();
            Panel.gameObject.SetActive(Panel.PanelProperties.ActiveWhenClosed);
        }

        private float SimTimeToSecForCurrentAnimation(float simTime)
        {
            return GetActiveAnimationLengthWithModifier() * simTime;
        }

        private float CalculateBreakingForce()
        {
            float result = 1.0f;
            if (SimulatedTime > 1.0f)
            {
                result = ((SimulatedTime) * Panel.Movement.DraggableContent.BreakingForce);
            }
            else if (SimulatedTime < 0.0f)
            {
                result = ((-SimulatedTime + 1.0f) * Panel.Movement.DraggableContent.BreakingForce);
            }

            return result;
        }

        private float SimTimeToDistance(float simTime)
        {
            float result = 1.0f;

            var pathLength = (Panel.WorldSpaceEndPosition - Panel.WorldSpaceStartPosition).magnitude;
            result = simTime * pathLength;

            return result;
        }

        public void AddSimulatedTimeManually(float deltaTime)
        {
            // deltaTime = SimTimeToSecForCurrentAnimation(deltaTime);

            deltaTime /= CalculateBreakingForce();

            SetSimulatedTimeManuallyFromPanel(deltaTime, this, true, true);

            foreach (var node in WooPanelsEngine.Instance.panelNodes)
            {
                node.Value.IsTimeSet = false;
            }
        }


        public void SetSimulatedTimeManually(float time)
        {
            AddSimulatedTimeManually(time - simulatedTime);
        }

        public void SetSimulatedTimeManuallyFromPanel(float deltaTime, WooPanelNode fromPanelNode, bool openChildren, bool normalizedTime)
        {
            if (IsTimeSet || !IsBeingDragged)
            {
                return;
            }

            IsTimeSet = true;

            if (Mathf.Abs(deltaTime) < 0.001f)
            {
                return;
            }

            if (deltaTime > 0)
            {
                ProcessOpening(deltaTime, fromPanelNode, openChildren, normalizedTime);
            }
            else if (deltaTime < 0)
            {
                ProcessClosing(deltaTime, fromPanelNode, normalizedTime);
            }

            SamplePanelAnimator(true);
        }

        private AnimationClip closingAnimation;
        private bool alreadySearched = false;

        private AnimationClip SearchForClosingAnimation()
        {
            if (alreadySearched)
            {
                return closingAnimation;
            }

            if (Panel == null)
            {
                return null;
            }

            var animator = Panel.GetAnimator();
            if (animator == null)
            {
                return null;
            }

            var animatorController = animator.runtimeAnimatorController;

            if (animatorController == null)
            {
                return null;
            }

            if (closingAnimation == null)
            {
                closingAnimation = animatorController.animationClips.FirstOrDefault(clip => clip.name == "PanelClosed");
                alreadySearched = true;
            }

            return closingAnimation;
        }

        public AnimationClip GetClosingAnimation()
        {
            if (SearchForClosingAnimation() == null || ForceMirrored)
            {
                return GetOpeningAnimation();
            }

            return closingAnimation;
        }

        private AnimationClip openingAnimation;

        private string openingAnimationName = "PanelOpened";
        private string closingAnimationName = "PanelClosed";

        public AnimationClip GetOpeningAnimation()
        {
            var animator = Panel.GetAnimator();

            if (Panel.GetAnimator() == null)
            {
                return null;
            }

            if (openingAnimation != null)
            {
                return openingAnimation;
            }
            var animatorController = animator.runtimeAnimatorController;

            if (animatorController == null)
            {
                return null;
            }

            openingAnimation = animatorController.animationClips.FirstOrDefault(clip => clip.name == "PanelOpened");

            return openingAnimation;
        }

        public bool IsMirrored()
        {
            var animator = Panel.GetAnimator();
            if (animator == null)
            {
                return false;
            }

            var animatorController = animator.runtimeAnimatorController;

            if (animatorController == null)
            {
                return false;
            }

            if (ForceMirrored)
            {
                return true;
            }

            return SearchForClosingAnimation() == null;
        }

        public AnimationClip GetActiveAnimation()
        {
            return (IsOpening || IsOpen || IsWaitingToOpen) ? GetOpeningAnimation() : GetClosingAnimation();
        }

        private string GetActiveAnimationName()
        {
            return (IsOpening || IsOpen || IsWaitingToOpen) ? openingAnimationName : GetClosingAnimationName();
        }

        private string GetClosingAnimationName()
        {
            return SearchForClosingAnimation() == null ? openingAnimationName : closingAnimationName;
        }

        public float GetActiveAnimationLengthWithModifier()
        {
            var mod = IsClosing ? Panel.PanelProperties.ClosingSpeed : 1.0f;
            mod = IsOpening ? Panel.PanelProperties.OpeningSpeed : mod;

            return GetActiveAnimation().length / mod;
        }

        public Rect rect;

        public Color Color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        public Color TargetColor;

        public bool LerpColor()
        {
            Color = Color.Lerp(Color, TargetColor, 0.85f);
            if (DistanceBetweenColors(Color, TargetColor) > Mathf.Epsilon)
            {
                return true;
            }

            return false;
        }

        private float DistanceBetweenColors(Color a, Color b)
        {
            var vecA = new Vector4(a.r, a.g, a.b, a.a);
            var vecB = new Vector4(b.r, b.g, b.b, b.a);

            return (vecA - vecB).sqrMagnitude;
        }

        Vector3 LerpWithoutClamp(Vector3 A, Vector3 B, float t)
        {
            return A + (B - A) * t;
        }

        private float previousSampleTime = -9999.0f;
        public void SamplePanelAnimator(bool forceSampleDraggablePanel = false)
        {
            if (Panel == null || Panel.GetAnimator() == null)
            {
                return;
            }

            if (Mathf.Abs(previousSampleTime - SimulatedTime) < SMALL_NUMBER && !forceSampleDraggablePanel)
            {
                return;
            }

            previousSampleTime = SimulatedTime;
            AnimationClip animation = null;
            var animatorController = Panel.GetAnimator().runtimeAnimatorController;

            animation = GetActiveAnimation();
            var animationName = GetActiveAnimationName();

            var time = SimulatedTime;

            if (Content != null && Panel.Movement.IsDraggable && ((!IsOpen && !IsClosed) || forceSampleDraggablePanel))
            {
                Content.transform.position = (Vector2)LerpWithoutClamp(Panel.WorldSpaceStartPosition, Panel.WorldSpaceEndPosition, SimulatedTime);

#if UNITY_2018_1_OR_NEWER
                Content.ForceUpdateRectTransforms();
#endif
            }

            time = Mathf.Clamp(time, 0.0f, 1.0f);
            var dt = forceSampleDraggablePanel ? Time.fixedDeltaTime : Time.deltaTime;
            if (Panel.PanelProperties.IgnoreTimeScale)
            {
                dt = Panel.PanelProperties.IgnoreTimeScale ? Time.unscaledDeltaTime : dt;
            }
            time += (IsClosing || IsWaitingToClose || IsClosed) ? -dt : dt;

            if (!IsMirrored() && (IsClosing || IsWaitingToClose || IsClosed))
            {
                time = 1.0f - time;
            }

            if (animation != null)
            {
#if UNITY_EDITOR
                if (AnimationMode.InAnimationMode())
                {
                    //if (!Application.isPlaying)
                    //{
                    //    AnimationMode.SampleAnimationClip(Panel.gameObject, animation, animation.length * time);
                    //}
                }
                else
                {
                    if (!Application.isPlaying && Panel.GetLayerIndex() != -1)
                    {
                        Panel.GetAnimator().Play(animationName, Panel.GetLayerIndex(), time);
                        Panel.GetAnimator().Update(dt);
                        Panel.GetAnimator().StopPlayback();
                    }
                }
#endif

                if (Application.isPlaying && Panel.GetLayerIndex() != -1)
                {
                    Panel.GetAnimator().Play(animationName, Panel.GetLayerIndex(), time);
                    Panel.GetAnimator().Update(dt);
                    Panel.GetAnimator().StopPlayback();
                }
            }
        }
    }
}