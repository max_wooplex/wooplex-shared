﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wooplex.Panels;

public class WooNavigationBar : WooPanel 
{
    public Text PanelTitleText;
    public Text PreviousPanelTitleText;
    public bool ShowBackButton = true;

    public Button BackButton;

    private WooPanel Panel;
    // Use this for initialization

    void OnInit()
    {
        if (Panel == null)
        {
            Panel = this.GetParentPanel();
        }

        if (Panel != null)
        {
            Panel.OnPanelOpen.AddListener(OnParentPanelOpen);
            UpdateTexts();
        }
    }

    public void OnParentPanelOpen()
    {
        UpdateTexts();
    }

    private void UpdateTexts()
    {
        if (Panel != null)
        {
            PanelTitleText.text = Panel.Title;
        }
        else
        {
            PanelTitleText.text = "";
        }

        if (Panel.PreviousDependentPanel != null && ShowBackButton)
        {
            BackButton.gameObject.SetActive(true);
            PreviousPanelTitleText.text = Panel.PreviousDependentPanel.Title;
        }
        else
        {
            PreviousPanelTitleText.text = "";
            BackButton.gameObject.SetActive(false);
        }
    }

    public void OnBackClick()
    {
        if (Panel.PreviousDependentPanel != null)
        {
            Panel.PreviousDependentPanel.Open();
        }
    }
}
