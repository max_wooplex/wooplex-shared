using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(WooEventsPropagator))]
public class WooButton : Button, IEventBlocker
{
    private bool isButtonEnabled = true;

	public override void OnPointerUp(PointerEventData eventData)
	{
        if (isButtonEnabled)
        {
            base.OnPointerUp(eventData);
        }
	}

	public override void OnPointerClick(PointerEventData eventData)
    {
        if (isButtonEnabled)
        {
            base.OnPointerClick(eventData);
        }

	}
	public void BlockEvents(WooGestureCapturer gestureCapturer)
    {
        isButtonEnabled = false;
        //this.InstantClearState();
    }

    public void UnblockEvents(WooGestureCapturer gestureCapturer)
    {
        isButtonEnabled = true;
    }
}
