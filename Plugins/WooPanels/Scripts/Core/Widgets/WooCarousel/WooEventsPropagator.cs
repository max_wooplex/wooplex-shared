﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class WooEventsPropagator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IInitializePotentialDragHandler
{
    public bool PropagateIfGestureCaptured = false;

    private WooGestureCapturer gestureCapturer;

    private PointerEventData savedPointerDownEventData;
    private PointerEventData savedDragBeginEventData;
    private bool isBlocked = false;
    private bool CheckIfShouldDelayEvent()
    {
        gestureCapturer = GetComponent<WooGestureCapturer>();

        return gestureCapturer != null && gestureCapturer.CaptureType != GestureType.DontCapture;
    }

    private void PropagateOnFail()
    {
        gestureCapturer.OnGestureAttemptEnd.AddListener(OnGestureAttemptEnd);
    }

    private void OnGestureAttemptEnd(bool result)
    {
        isBlocked = false;
        if (!result)
        {
            PropagateMouseDown(savedPointerDownEventData);
            if (savedDragBeginEventData != null)
            {
                PropagateBeginDrag(savedDragBeginEventData);
                savedDragBeginEventData = null;
            }

            savedPointerDownEventData = null;
        }

        gestureCapturer.OnGestureAttemptEnd.RemoveListener(OnGestureAttemptEnd);
    }

	public void OnPointerDown(PointerEventData eventData)
	{
        if (CheckIfShouldDelayEvent())
        {
            savedPointerDownEventData = eventData;
            isBlocked = true;
            PropagateOnFail();
        }
        else
        {
            PropagateMouseDown(eventData);
        }
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
        if (isBlocked)
        {
            savedDragBeginEventData = eventData;
        }
        else
        {
            PropagateBeginDrag(eventData);
        }
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		PropagateEndDrag(eventData);
	}

	public void OnInitializePotentialDrag(PointerEventData eventData)
	{
		PropagateInitializePotentialDrag(eventData);
	}

	public void OnDrag(PointerEventData eventData)
	{
		PropagateDrag(eventData);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
        PropagateMouseUp(eventData);
	}
	
	private void PropagateBeginDrag(PointerEventData eventData = null)
	{
		if (eventData == null)
		{
			eventData = new PointerEventData(EventSystem.current);
		}
		ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.beginDragHandler);
	}

	private void PropagateEndDrag(PointerEventData eventData = null)
	{
		if (eventData == null)
		{
			eventData = new PointerEventData(EventSystem.current);
		}
		ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.endDragHandler);
	}

	private void PropagateInitializePotentialDrag(PointerEventData eventData = null)
	{
		if (eventData == null)
		{
		eventData = new PointerEventData(EventSystem.current);
		}
		ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.initializePotentialDrag);
	}

	private void PropagateDrag(PointerEventData eventData = null)
	{
		if (eventData == null)
		{
			eventData = new PointerEventData(EventSystem.current);
		}
		ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.dragHandler);
	}

	private void PropagateMouseDown(PointerEventData eventData = null)
	{
		if (eventData == null)
		{
			eventData = new PointerEventData(EventSystem.current);
		}
		ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.pointerDownHandler);
	}

	private void PropagateMouseUp(PointerEventData eventData = null)
	{
		if (eventData == null)
		{
			eventData = new PointerEventData(EventSystem.current);
		}

		ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.pointerUpHandler);
	}



	//public void OnCaptureAttemptBegin()
	//{
		
	//}

	//public void OnCaptureAttemptEnd()
	//{

	//}

    //public void OnCaptureStart()
    //{
        
    //}

    //public void OnCaptureEnd()
    //{
        
    //}
}
