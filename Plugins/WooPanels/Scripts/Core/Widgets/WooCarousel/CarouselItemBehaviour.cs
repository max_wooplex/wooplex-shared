﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CarouselItemBehaviour : MonoBehaviour 
{
	public UnityEvent OnItemSelected;
	public UnityEvent OnItemUnselected;

    [HideInInspector]
	public float ParametricPosition;
    [HideInInspector]
	public float SelectorParametricPosition;
    [HideInInspector]
	public float Velocity;

	public void OnSelected()
	{
		if (OnItemSelected != null)
		{
			OnItemSelected.Invoke();
		}
	}

	public void OnUnselected()
	{
		if (OnItemUnselected != null)
		{
			OnItemUnselected.Invoke();
		}
	}

	void Update()
	{

	}
}
