using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using Wooplex.Panels;

public enum GestureState
{
    Possible,
    Active,
    NotPossible
}

public enum GestureType
{
    Horizontal,
    Vertical,
    Any,
    DontCapture
}

public interface IGestureHandler : IEventSystemHandler
{
    void OnGestureAttemptBegin();
    void OnGestureAttemptEnd(bool result);
    void OnGestureStart();
    void OnGestureEnd();
}

public class WooGestureCapturer : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IGestureHandler
{
    public GestureType CaptureType;

    protected GestureState gestureState = GestureState.NotPossible;

    public GestureState GestureState
    {
        get
        {
            return gestureState;
        }
    }
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool>
    {

    }
    [HideInInspector]
    public UnityEvent OnGestureAttemptBegin = new UnityEvent();
    [HideInInspector]
    public BoolEvent OnGestureAttemptEnd = new BoolEvent();
    [HideInInspector]
    public UnityEvent OnGestureStart = new UnityEvent();
    [HideInInspector]
    public UnityEvent OnGestureEnd = new UnityEvent();

    private Vector3 startDragPosition;
    private Vector3 startLocalPosition;
    private Vector3 offset = Vector3.zero;

    private float dAngle = 90.0f;
    private float captureMagnitude = 1.0f;
    private bool isClickPossible = false;
    private const float MIN_CLICK_DISTANCE = 0.2f;
    private int pointerId = -1;

    private Canvas parentCanvas;

    protected void Awake()
    {
        startLocalPosition = this.transform.localPosition;
        var parentCanvases = this.GetComponentsInParent<Canvas>();

        parentCanvas = (parentCanvases != null && parentCanvases.Length > 0) ? parentCanvases[parentCanvases.Length - 1] : null;
    }

	// Use this for initialization
	void Start()
	{

	}

    public void LateUpdate()
    {
        if (CaptureType != GestureType.DontCapture)
        {
            ProcessDragging();
        }
    }

    private void ProcessDragging()
    {
        if (gestureState == GestureState.Possible || gestureState == GestureState.Active)
        {
            TryGesture();
        }
    }

    private void TryGesture()
    {
        if (Input.touchCount == 0 && !Input.GetMouseButton(0))
        {
            OnPointerUp(null);
            return;
        }
        var dir = GetWorldInputPosition() - startDragPosition;
        var minAgle = 90.0f - dAngle;
        var maxAngle = 90.0f + dAngle;
        var axis = CaptureType == GestureType.Horizontal ? Vector3.up : Vector3.right;
        var angle = Vector3.Angle(dir, axis);

        offset = dir;
        if (gestureState == GestureState.Active || (angle > minAgle && angle < maxAngle) || this.CaptureType == GestureType.Any)
        {
            if (gestureState != GestureState.Active && offset.magnitude > captureMagnitude * ((float)Screen.dpi / 100.0f))
            {
                gestureState = GestureState.Active;
                PropagateGestureAttemptEnd(true);
                PropagateGestureStart();
            }
        }
        else if (gestureState == GestureState.Possible)
        {
            gestureState = GestureState.NotPossible;

            PropagateGestureAttemptEnd(false);
        }
    }

    public Vector3 GetWorldInputPosition()
    {
        var inputPosition = pointerId < 0 ? (Vector2)Input.mousePosition : Input.GetTouch(pointerId).position;

        if (parentCanvas != null)
        {
            if (parentCanvas.renderMode != RenderMode.ScreenSpaceOverlay && parentCanvas.worldCamera != null)
            {
                inputPosition = parentCanvas.worldCamera.ScreenToWorldPoint(inputPosition);
            }
        }

        return inputPosition;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (pointerId != -1)
        {
            return;
        }

        startDragPosition = GetWorldInputPosition();
        isClickPossible = true;
        pointerId = eventData.pointerId;
        PropagateGestureAttemptBegin();

        offset = Vector3.zero;
        if (CaptureType != GestureType.DontCapture)
        {
            gestureState = GestureState.Possible;
            startDragPosition = GetWorldInputPosition();
            startLocalPosition = this.transform.localPosition;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData != null && eventData.pointerId != pointerId)
        {
            return;
        }

        if (gestureState != GestureState.NotPossible)
        {
            if (gestureState == GestureState.Active)
            {
                PropagateGestureEnd();
            }
            else
            {
                PropagateGestureAttemptEnd(false);
            }
            gestureState = GestureState.NotPossible;
        }
        pointerId = -1;
    }

    private void PropagateGestureAttemptBegin(PointerEventData eventData = null)
    {
        if (eventData == null)
        {
            eventData = new PointerEventData(EventSystem.current);
        }

        OnGestureAttemptBegin.Invoke();
        ExecuteEvents.ExecuteHierarchy<IGestureHandler>(transform.gameObject, eventData, (x, y) => x.OnGestureAttemptBegin());
    }

    private void PropagateGestureAttemptEnd(bool result, PointerEventData eventData = null)
    {
        if (eventData == null)
        {
            eventData = new PointerEventData(EventSystem.current);
        }

        OnGestureAttemptEnd.Invoke(result);
        ExecuteEvents.ExecuteHierarchy<IGestureHandler>(transform.gameObject, eventData, (x, y) => x.OnGestureAttemptEnd(result));
    }

    private void PropagateGestureStart(PointerEventData eventData = null)
    {
        if (eventData == null)
        {
            eventData = new PointerEventData(EventSystem.current);
        }

        var eventBlockers = GetComponentsInChildren<IEventBlocker>();

        for (int i = 0; i < eventBlockers.Length; i++)
        {
            eventBlockers[i].BlockEvents(this);
        }

        OnGestureStart.Invoke();
        ExecuteEvents.ExecuteHierarchy<IGestureHandler>(transform.gameObject, eventData, (x, y) => x.OnGestureStart());
    }

    private void PropagateGestureEnd(PointerEventData eventData = null)
    {
        if (eventData == null)
        {
            eventData = new PointerEventData(EventSystem.current);
        }
        StartCoroutine(UnblockCoroutine());
        OnGestureEnd.Invoke();
        ExecuteEvents.ExecuteHierarchy<IGestureHandler>(transform.gameObject, eventData, (x, y) => x.OnGestureEnd());
    }

    private IEnumerator UnblockCoroutine()
    {
        yield return new WaitForEndOfFrame();

        var eventBlockers = GetComponentsInChildren<IEventBlocker>();

        for (int i = 0; i < eventBlockers.Length; i++)
        {
            eventBlockers[i].UnblockEvents(this);
        }
    }
    void IGestureHandler.OnGestureAttemptBegin()
    {
        
    }

    void IGestureHandler.OnGestureAttemptEnd(bool result)
    {
        
    }

    void IGestureHandler.OnGestureStart()
    {
        
    }

    void IGestureHandler.OnGestureEnd()
    {
        
    }
}
