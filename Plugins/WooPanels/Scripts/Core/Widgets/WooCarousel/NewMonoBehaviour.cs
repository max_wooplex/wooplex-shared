using UnityEngine;
using System.Collections;

public interface IEventBlocker
{
    void BlockEvents(WooGestureCapturer gestureCapturer);
    void UnblockEvents(WooGestureCapturer gestureCapturer);
}
