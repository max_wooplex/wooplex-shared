﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEditor;
using Wooplex.Panels;

public enum CarouselState
{
    Idle,
    Grabbed,
    Released,
    Transitioning,
    Repelling
}

public enum ControlType
{
    Horizontal,
    Vertical,
    AlongPath
}

public enum CarouselType
{
    OneByOne,
    FreeScrolling
}

[RequireComponent(typeof(WooEventsPropagator), typeof(WooGestureCapturer))]
public class WooCarousel : WooPanel, IGestureHandler
{
    public ControlType ControlType = ControlType.Horizontal;
    [HideInInspector]
    public CarouselType CarouselType = CarouselType.OneByOne;

    public Transform Content;
    //[HideInInspector]
    public Transform Selector;

    [HideInInspector]
    public bool ElementsRetainPositions = true;

    //public bool AbsoluteSpeed;
    [HideInInspector]
    [Range(0.0f, 2.0f)]
    public float OverallSpeed = 1.0f;

    [HideInInspector]
    [Range(0.0f, 1.0f)]
    public float GrabDrag = 1.0f;

    //[HideInInspector]
    [Range(0.0f, 1.0f)]
    public float ItemGravity = 0.7f;

    //[HideInInspector]
    [Range(0.0f, 1.0f)]
    public float ItemDrag = 0.3f;

    [HideInInspector]
    [Range(0.0f, 1.0f)]
    public float BoundsForce = 0.2f;

    [HideInInspector]
    [Range(0.0f, 1.0f)]
    public float BoundsDrag = 0.0f;

    [HideInInspector]
    [Range(0.0f, 1.0f)]
    public float SnapReaction = 0.5f;

    //[HideInInspector]
    [Range(0.0f, 1.0f)]
    public float ScrollingDrag = 0.3f;

    [HideInInspector]
    [Range(0.0f, 1.0f)]
    public float DistanceBetweenFirstAndLast = 0.5f;

    [HideInInspector]
    public bool IsInfinite = false;

    [HideInInspector]
    public bool Avarage = true;

    [HideInInspector]
    public bool AllowBounceFromSides = false;

    //[HideInInspector]
    public Transform SelectedTransform;

    [HideInInspector]
    public UnityEvent OnCarouselItemChange;

    private CarouselState _state = CarouselState.Released;

    public CarouselState state
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
        }
    }

    private Vector3 startMousePosition;
    private Animator selectedAnimator;

    private Transform rememberedSelection;
    [HideInInspector]
    public Transform pendingTransformToTransitionTo;

    private const float DragMultiplier = 35.0f;
    private bool repelling = false;
    private bool shouldExecuteControlLostOnLateUpdate = false;
    private bool shouldSwipe = false;
    //[HideInInspector]
    private float Velocity = 0.0f;
    [HideInInspector]
    public float Position = 0.0f;

    private float physicalMouseParametricPosition;
    private CarouselPath path;

    private float RepellMultiplier = 1.0f;

    private WooGestureCapturer captureControlItem;


    private void OnInit()
    {
        this.captureControlItem = GetComponent<WooGestureCapturer>();
    }

    public void RebuildPath()
    {
        var horizontalLayoutGroup = Content.GetComponent<HorizontalLayoutGroup>();
        var verticalLayoutGroup = Content.GetComponent<VerticalLayoutGroup>();
        if (Selector == null)
        {
            Selector = Content;
        }
        if (horizontalLayoutGroup != null || verticalLayoutGroup != null)
        {
            var canvas = GetComponentInParent<Canvas>();
            Vector3[] corners = new Vector3[4];
            Content.parent.GetComponent<RectTransform>().GetLocalCorners(corners);
            var width = corners[2].x - corners[0].x;
            var height = corners[2].y - corners[3].y;

            var parentWidth = width * canvas.scaleFactor;
            var parentHeight = height * canvas.scaleFactor;

            Content.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, horizontalLayoutGroup ? Content.childCount * width : width);
            Content.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, verticalLayoutGroup ? Content.childCount * height : height);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(Content as RectTransform);

        List<Transform> children = new List<Transform>();
        for (int i = 0; i < Content.childCount; i++)
        {
            children.Add(Content.GetChild(i));
        }

        if (children.Count < 2)
        {
            return;
        }

        path = new CarouselPath(children, IsInfinite ? DistanceBetweenFirstAndLast : -1.0f, Avarage, ElementsRetainPositions);

        if (path.Magnitude == 0.0f)
        {
            path = null;
            return;
        }
        for (int i = 0; i < Content.childCount; i++)
        {
            var carouselItem = Content.GetChild(i).GetComponent<CarouselItemBehaviour>();
            if (carouselItem == null)
            {
                carouselItem = Content.GetChild(i).gameObject.AddComponent<CarouselItemBehaviour>();
            }
            var captureControl = Content.GetChild(i).GetComponent<WooEventsPropagator>();
            if (captureControl == null)
            {
                captureControl = Content.GetChild(i).gameObject.AddComponent<WooEventsPropagator>();
            }

            UnselectItemLogic(carouselItem.transform);
            carouselItem.ParametricPosition = path.GetParametricPosition(Content.GetChild(i).localPosition);
        }
        var newTransform = GetClosestToSelectorTransform();
        if (newTransform != SelectedTransform)
        {
            SelectedTransform = newTransform;
            OnCarouselItemChange.Invoke();
        }

        Position = path.GetParametricPosition(SelectedTransform.localPosition);
    }

    private bool CheckIfContentIsAnyOfParents(Transform transform, out Transform result)
    {
        var pointer = transform;
        result = null;
        while (pointer.parent != null)
        {
            if (pointer.parent == Content)
            {
                result = pointer;
                return true;
            }
            pointer = pointer.parent;
        }

        return false;
    }

    private void ProcessSwipe()
    {
        var distance = (GetUniversalGrabPosition() - physicalMouseParametricPosition);

        if (distance > 0.000001f)
        {
            SelectItem(GetPreviousClosestToSelector());
        }
        else if (distance < -0.000001f)
        {
            SelectItem(GetNextClosestToSelector());
        }
    }

    public void SelectNextItem()
    {
        SelectItem(GetNextItem());
    }

    private List<CarouselItemBehaviour> GetSortedCarouselItemsList()
    {
        List<CarouselItemBehaviour> carouselItems = new List<CarouselItemBehaviour>();

        for (int i = 0; i < Content.childCount; i++)
        {
            var childTransform = Content.GetChild(i);
            carouselItems.Add(childTransform.GetComponent<CarouselItemBehaviour>());
        }

        carouselItems.Sort((item1, item2) => item1.ParametricPosition.CompareTo(item2.ParametricPosition));

        return carouselItems;
    }

    private Transform GetPreviousItem()
    {
        CarouselItemBehaviour selectedCarouselItem = SelectedTransform.GetComponent<CarouselItemBehaviour>();

        Transform result = SelectedTransform;
        List<CarouselItemBehaviour> carouselItems = GetSortedCarouselItemsList();

        var thisIndex = carouselItems.FindIndex((item) => item == selectedCarouselItem);
        var resultIndex = thisIndex - 1;
        if (resultIndex < 0)
        {
            if (IsInfinite)
            {
                resultIndex += carouselItems.Count;
            }
            else
            {
                resultIndex += 1;
            }

        }
        result = carouselItems[resultIndex].transform;

        return result;
    }

    private Transform GetNextItem()
    {
        CarouselItemBehaviour selectedCarouselItem = SelectedTransform.GetComponent<CarouselItemBehaviour>();

        Transform result = SelectedTransform;
        List<CarouselItemBehaviour> carouselItems = GetSortedCarouselItemsList();

        var thisIndex = carouselItems.FindIndex((item) => item == selectedCarouselItem);
        var resultIndex = thisIndex + 1;
        if (resultIndex > carouselItems.Count - 1)
        {
            if (IsInfinite)
            {
                resultIndex -= carouselItems.Count;
            }
            else
            {
                resultIndex -= 1;
            }
        }
        result = carouselItems[resultIndex].transform;

        return result;
    }

    public void SelectPreviousItem()
    {
        SelectItem(GetPreviousItem());
    }

    public void SelectItem(Transform item)
    {
        pendingTransformToTransitionTo = item;
        state = CarouselState.Transitioning;
    }

    private void FixedOnControlLost()
    {
        if (shouldExecuteControlLostOnLateUpdate)
        {
            state = CarouselState.Released;
            //OnSelectionAttemptEnd();
            shouldExecuteControlLostOnLateUpdate = false;

            if (shouldSwipe && this.CarouselType == CarouselType.OneByOne)
                ProcessSwipe();
            shouldSwipe = false;
        }
    }


    private void PanelUpdate()
    {
        if (captureControlItem.CaptureType != GestureType.Any)
        {
            captureControlItem.CaptureType = ControlType == ControlType.Horizontal ? GestureType.Horizontal : captureControlItem.CaptureType;
            captureControlItem.CaptureType = ControlType == ControlType.Vertical ? GestureType.Vertical : captureControlItem.CaptureType;
            captureControlItem.CaptureType = ControlType == ControlType.AlongPath ? GestureType.Any : captureControlItem.CaptureType;
        }


        if (path == null)
        {
            RebuildPath();
        }
        if (path == null)
        {
            return;
        }

        if (this.state == CarouselState.Idle)
        {
            return;
        }

        FixedOnControlLost();
        NewProcessAnimations();
        NewModifyVelocityBasedOnSelected();
        NewApplyDragToVelocity();
        NewProcessGrab();
        NewProcessVelocityForChildren();
        ChangeStateToIdleWhenMovingSlowly();
    }

    private float NewGetRepellingForce()
    {
        var selectorPos = path.GetParametricPosition(Content.InverseTransformPoint(Selector.position));
        var minPos = float.MaxValue;
        var maxPos = float.MinValue;

        for (int i = 0; i < Content.childCount; i++)
        {
            var carouselItem = Content.GetChild(i).GetComponent<CarouselItemBehaviour>();

            if (carouselItem.ParametricPosition + Velocity < minPos)
            {
                minPos = carouselItem.ParametricPosition + Velocity;
            }
            if (carouselItem.ParametricPosition + Velocity > maxPos)
            {
                maxPos = carouselItem.ParametricPosition + Velocity;
            }
        }

        repelling = selectorPos < minPos - 0.01f || selectorPos > maxPos + 0.01f;

        if (selectorPos < minPos)
        {
            return -(minPos - selectorPos);
        }
        if (selectorPos > maxPos)
        {
            return -(maxPos - selectorPos);
        }

        return 0.0f;
    }

    private void NewProcessAnimations()
    {
        var prevSelected = SelectedTransform;

        var closestToSelectorTransform = GetClosestToSelectorTransform();

        if (selectedAnimator != null)
        {
            selectedAnimator.logWarnings = false;
            selectedAnimator.SetBool("IsSelected", true);
        }

        if (SelectedTransform != null && SelectedTransform != closestToSelectorTransform && state == CarouselState.Grabbed)
        {
            UnselectItemLogic(SelectedTransform);
            SelectedTransform = null;
            selectedAnimator = null;
        }

        if (TryToGetNewSelectedItem())
        {
            if (prevSelected != null)
            {
                UnselectItemLogic(prevSelected);
            }
            SelectItemLogic(SelectedTransform);
        }
    }

    private void NewProcessGrab()
    {
        if (state == CarouselState.Grabbed)
        {
            NewProcessGrabbingForChildren();
        }
    }

    private void ChangeStateToIdleWhenMovingSlowly()
    {
        if ((state == CarouselState.Released) && Mathf.Abs(Velocity) < float.Epsilon)
        {
            state = CarouselState.Idle;
        }
        if (state == CarouselState.Transitioning && Mathf.Abs(Velocity) < 0.0001f && GetClosestToSelectorTransform() == SelectedTransform)
        {
            state = CarouselState.Idle;
        }
    }

    private void NewProcessVelocityForChildren()
    {
        var repellingDistance = NewGetRepellingForce();
        var repellingForce = repellingDistance * (1.0f - BoundsForce) * (state == CarouselState.Grabbed ? 1.0f : (1.0f - BoundsDrag) / 50.0f);

        Velocity += repellingForce;

        physicalMouseParametricPosition += Velocity;
        Position -= Velocity;
        var selectorParamPos = path.GetParametricPosition(Content.InverseTransformPoint(Selector.position));

        if (Position < 0.0f)
        {
            Position += 1.0f;
        }
        else if (Position > 1.0f)
        {
            Position -= 1.0f;
        }
        for (int i = 0; i < Content.childCount; i++)
        {
            var carouselItem = Content.GetChild(i).GetComponent<CarouselItemBehaviour>();
            carouselItem.ParametricPosition += Velocity;

            if (IsInfinite)
            {
                if (carouselItem.ParametricPosition > selectorParamPos + 0.5f)
                {
                    carouselItem.ParametricPosition -= 1.0f;
                }
                else if (carouselItem.ParametricPosition < selectorParamPos - 0.5f)
                {
                    carouselItem.ParametricPosition += 1.0f;
                }
            }

            carouselItem.SelectorParametricPosition = selectorParamPos;
            carouselItem.transform.localPosition = path.GetPosition(carouselItem.ParametricPosition);
        }
    }

    private void NewProcessGrabbingForChildren()
    {
        Velocity = (GetUniversalGrabPosition() - physicalMouseParametricPosition);
    }

    private Vector3 GetGrabPosition()
    {
        return captureControlItem.GetWorldInputPosition();
    }

    private float GetUniversalGrabPosition()
    {
        var newWorldPos = this.GetGrabPosition();
        float result = 0.0f;
        newWorldPos = Content.InverseTransformPoint(newWorldPos);

        if (this.ControlType == ControlType.AlongPath)
        {
            result = path.GetParametricPosition(newWorldPos);
        }
        if (this.ControlType == ControlType.Horizontal)
        {
            result = newWorldPos.x / path.Magnitude;
        }
        if (this.ControlType == ControlType.Vertical)
        {
            result = newWorldPos.y / path.Magnitude;
        }

        return result;
    }

    private bool TryToGetNewSelectedItem()
    {
        var changed = false;
        var prevSelected = SelectedTransform;

        if (state != CarouselState.Transitioning /*&& state != CarouselState.Grabbed*/)
        {
            SelectedTransform = GetClosestToSelectorTransform();
            selectedAnimator = SelectedTransform.GetComponent<Animator>();
        }
        else if (state == CarouselState.Transitioning && pendingTransformToTransitionTo != null)
        {
            rememberedSelection = SelectedTransform;
            SelectedTransform = pendingTransformToTransitionTo;
            selectedAnimator = SelectedTransform.GetComponent<Animator>();
            pendingTransformToTransitionTo = null;
        }

        if (prevSelected != SelectedTransform)
        {
            changed = true;
            OnCarouselItemChange.Invoke();
        }

        if ((state == CarouselState.Released && CarouselType == CarouselType.OneByOne) || (state == CarouselState.Released) && (!repelling || !AllowBounceFromSides) && (Mathf.Abs(Velocity) < SnapReaction * 0.05f))
        {
            state = CarouselState.Transitioning;
        }
        return changed;
    }

    private void NewModifyVelocityBasedOnSelected()
    {
        if (SelectedTransform != null && state == CarouselState.Transitioning)
        {
            var selectedParametricPosition = SelectedTransform.GetComponent<CarouselItemBehaviour>().ParametricPosition;
            var selectorParametricPosition = path.GetParametricPosition(Content.InverseTransformPoint(Selector.position));

            Velocity = Mathf.Lerp(Velocity, selectorParametricPosition - selectedParametricPosition, ItemGravity * 0.25f);
        }
    }

    private void NewApplyDragToVelocity()
    {
        if (state == CarouselState.Grabbed)
        {
            Velocity *= Mathf.Clamp01(1.0f - GrabDrag * DragMultiplier * 0.017f);
        }
        if (state != CarouselState.Grabbed)
        {
            Velocity *= Mathf.Clamp01(1.0f - ScrollingDrag * DragMultiplier * 0.006f);
        }
        if (state == CarouselState.Transitioning)
        {
            Velocity *= Mathf.Clamp01(1.0f - ItemDrag * DragMultiplier * 0.017f);
        }
        if (state == CarouselState.Idle)
        {
            Velocity = 0.0f;
        }
        if (repelling)
        {
            Velocity *= Mathf.Clamp01(1.0f - BoundsDrag * DragMultiplier * 0.017f);
        }
    }

    private Transform GetNextClosestToSelector()
    {
        Transform result = null;

        float selectorPosition = path.GetParametricPosition(Selector.position);
        float minD = float.MaxValue;

        for (int i = 0; i < Content.childCount; i++)
        {
            var childPos = path.GetParametricPosition(Content.GetChild(i).position);
            var newD = childPos - selectorPosition;

            if (childPos > selectorPosition && minD > newD)
            {
                minD = newD;
                result = Content.GetChild(i);
            }
        }
        return result;
    }

    private Transform GetPreviousClosestToSelector()
    {
        Transform result = null;

        float selectorPosition = path.GetParametricPosition(Selector.position);
        float minD = float.MaxValue;

        for (int i = 0; i < Content.childCount; i++)
        {
            var childPos = path.GetParametricPosition(Content.GetChild(i).position);
            var newD = Mathf.Abs(childPos - selectorPosition);

            if (childPos < selectorPosition && minD > newD)
            {
                minD = newD;
                result = Content.GetChild(i);
            }
        }
        return result;
    }
    private Transform GetClosestToSelectorTransform()
    {
        Transform result = null;

        Vector3 selectorPosition = Selector.position;
        float minD = float.MaxValue;

        for (int i = 0; i < Content.childCount; i++)
        {
            var childPos = Content.GetChild(i).position;
            var d = Vector3.SqrMagnitude(childPos - selectorPosition);
            if (minD > d)
            {
                if (result == null)
                {
                    result = Content.GetChild(i);
                }
                else
                {
                    result = Content.GetChild(i);
                }
                minD = d;
            }
        }
        return result;
    }

    private void UnselectItemLogic(Transform item)
    {
        if (item != null)
        {
            var animator = item.GetComponent<Animator>();

            if (animator != null)
            {
                animator.logWarnings = false;
                var prevTime = 0.0f;
                if (animator.GetCurrentAnimatorStateInfo(animator.GetLayerIndex("Carousel")).IsName("ItemSelected"))
                {
                    prevTime = Mathf.Min(1.0f, animator.GetCurrentAnimatorStateInfo(animator.GetLayerIndex("Carousel")).normalizedTime);
                }
                animator.SetBool("IsSelected", false);

                animator.Play("ItemUnselected", animator.GetLayerIndex("Carousel"), 1.0f - prevTime);
            }

            item.GetComponent<CarouselItemBehaviour>().OnUnselected();
        }
    }

    private void SelectItemLogic(Transform item, bool sendMessage = true)
    {
        if (item != null)
        {
            var animator = item.GetComponent<Animator>();

            if (animator != null)
            {
                animator.logWarnings = false;
                var prevTime = 1.0f;
                if (animator.GetCurrentAnimatorStateInfo(animator.GetLayerIndex("Carousel")).IsName("ItemUnselected"))
                {
                    prevTime = Mathf.Min(1.0f, animator.GetCurrentAnimatorStateInfo(animator.GetLayerIndex("Carousel")).normalizedTime);
                }
                animator.SetBool("IsSelected", true);

                animator.Play("ItemSelected", animator.GetLayerIndex("Carousel"), 1.0f - prevTime);
            }

            //if (sendMessage)
            //{
                item.GetComponent<CarouselItemBehaviour>().OnSelected();
            //}
        }
    }

    public void OnGestureAttemptBegin()
    {
        startMousePosition = GetGrabPosition();
    }

    private void TryToClick()
    {
        if ((startMousePosition - GetGrabPosition()).magnitude < 0.2f)
        {
            var pointer = new PointerEventData(EventSystem.current);

            pointer.position = Input.mousePosition;
            var raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointer, raycastResults);
            foreach (var raycastResult in raycastResults)
            {
                var captureControlItem = raycastResult.gameObject.GetComponent<WooEventsPropagator>();
                Transform item;

                if (CheckIfContentIsAnyOfParents(raycastResult.gameObject.transform, out item))
                {
                    SelectItem(item);
                    break;
                }
            }
        }
    }

    public void OnGestureAttemptEnd(bool result)
    {
        if (!result)
        {
            TryToClick();
        }
    }

    public void OnGestureStart()
    {
        OnGestureAttemptBegin();
        state = CarouselState.Grabbed;

        physicalMouseParametricPosition = GetUniversalGrabPosition();
    }

    public void OnGestureEnd()
    {
        TryToClick();

        shouldSwipe = captureControlItem.GestureState == GestureState.Active;

        shouldExecuteControlLostOnLateUpdate = true;
    }
}
