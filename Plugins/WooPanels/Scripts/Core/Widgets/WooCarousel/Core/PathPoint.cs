using UnityEngine;
using System.Collections;

public struct PathPoint
{
    public float ParametricPosition;

    //public float ParametricPosition
    //{
    //    get
    //    {
    //        return parametricPosition;
    //    }
    //    set
    //    {
    //        parametricPosition = value;
    //    }
    //}

    public Vector3 Position;

    //public Vector3 Position
    //{
    //    get
    //    {
    //        return position;
    //    }
    //    set
    //    {
    //        position = value;
    //    }
    //}

    public CarouselPath path;
}
