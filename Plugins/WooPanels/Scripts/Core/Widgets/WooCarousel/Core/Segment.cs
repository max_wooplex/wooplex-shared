using UnityEngine;
using System.Collections;

public struct Segment
{
    public PathPoint Start;
    public PathPoint End;
    public bool IsFirst;
    public bool IsLast;

    public Segment(PathPoint start, PathPoint end)
    {
        Start = start;
        End = end;
        IsFirst = false;
        IsLast = false;
    }

    public Vector3 GetPositionOnSegment(float parametricPosition)
    {
        var direction = End.Position - Start.Position;
        return Start.Position + direction * parametricPosition;
    }

    public PathPoint GetPointOnSegment(Vector3 pnt, out float distance)
    {
        Vector3 lineDir = End.Position - Start.Position;
        lineDir.Normalize();

        PathPoint result = new PathPoint();
        Vector3 firstOffset = Vector3.zero;
        if (IsFirst)
        {
            firstOffset = (lineDir * -100000.0f);
        }
        Vector3 lastOffset = Vector3.zero;
        if (IsLast)
        {
            lastOffset = (lineDir * 100000.0f);
        }

        result.Position = ProjectPointLine(pnt, Start.Position + firstOffset, End.Position + lastOffset);

        var resultStartDistance = (result.Position - Start.Position).magnitude;
        var resultEndDistance = (result.Position - End.Position).magnitude;
        var resultPointDistance = (result.Position - pnt).magnitude;
        var startEndDistance = (Start.Position - End.Position).magnitude;

        var angle = Angle360(End.Position - Start.Position, result.Position - Start.Position);
        var sign = angle > 90.0f && angle < 270.0f ? -1.0f : 1.0f;

        result.ParametricPosition = Start.ParametricPosition + sign * resultStartDistance * (Mathf.Abs((End.ParametricPosition - Start.ParametricPosition)) / startEndDistance);

        distance = resultPointDistance;

        return result;
    }

    public Vector3 ProjectPointLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
    {
        Vector3 rhs = point - lineStart;
        Vector3 vector2 = lineEnd - lineStart;
        float magnitude = vector2.magnitude;
        Vector3 lhs = vector2;
        if (magnitude > 1E-06f)
        {
            lhs = (Vector3)(lhs / magnitude);
        }
        float num2 = Mathf.Clamp(Vector3.Dot(lhs, rhs), 0f, magnitude);
        return (lineStart + ((Vector3)(lhs * num2)));
    }

    float Angle360(Vector3 from, Vector3 to)
    {
        Vector3 right = Vector3.right;
        float angle = Vector3.Angle(from, to);
        return (Vector3.Angle(right, to) > 90f) ? 360f - angle : angle;
    }
}