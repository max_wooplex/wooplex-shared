using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarouselPath
{
    public List<PathPoint> Points;

    public List<Segment> Segments;

    public float Magnitude;

    public CarouselPath(List<Transform> transformPoints, float distanceBetweenFirstAndLast = -1.0f, bool avarage = true, bool elementsRetainPositions = true)
    {
        SetPath(transformPoints, distanceBetweenFirstAndLast, avarage, elementsRetainPositions);
    }

    public List<Vector3> GetVector3ArrayFromTransforms(List<Transform> transforms)
    {
        List<Vector3> result = new List<Vector3>();

        for (int i = 0; i < transforms.Count; i++)
        {
            result.Add(transforms[i].transform.localPosition);
        }

        return result;
    }

    public List<Vector3> AddFirstAndLastPoint(List<Vector3> Points, float distanceBetweenFirstAndLast)
    {
        List<Vector3> result = new List<Vector3>();


        var oldMagnitude = Magnitude;
        Magnitude += (distanceBetweenFirstAndLast * Magnitude);

        for (int i = 0; i < Points.Count + 2; i++)
        {
            if (i == 0)
            {
                result.Add(Points[0] + (Points[0] - Points[1]).normalized * distanceBetweenFirstAndLast * oldMagnitude / 2.0f);

            }
            else if (i == Points.Count + 1)
            {
                result.Add(Points[Points.Count - 1] + ((Points[Points.Count - 1] - Points[Points.Count - 2]).normalized * distanceBetweenFirstAndLast * oldMagnitude / 2.0f));

            }
            else
            {
                result.Add(Points[i - 1]);
            }

        }

        return result;
    }

    public void SetPath(List<Transform> transforms, float distanceBetweenFirstAndLast = -1.0f, bool avarage = true, bool elementsRetainPositions = true)
    {
        Points = new List<PathPoint>();
        Segments = new List<Segment>();

        Magnitude = 0.0f;

        List<Vector3> transformPoints = GetVector3ArrayFromTransforms(transforms);
        for (int i = 0; i < transformPoints.Count - 1; i++)
        {
            Magnitude += (transformPoints[i + 1] - transformPoints[i]).magnitude;
        }


        var pointsAdded = false;
        if (distanceBetweenFirstAndLast >= 0.0f)
        {
            if (avarage)
            {
                distanceBetweenFirstAndLast = 1.0f / ((float)transformPoints.Count - 1);
            }

            pointsAdded = true;
            transformPoints = AddFirstAndLastPoint(transformPoints, distanceBetweenFirstAndLast);

            if (avarage)
            {
                distanceBetweenFirstAndLast = 1.0f / ((float)transformPoints.Count - 2);
            }
        }

        var currentLength = 0.0f;
        var paramPositionAccum = 0.0f;
        for (int i = 0; i < transformPoints.Count; i++)
        {
            Points.Add(new PathPoint());
            var temp = Points[i];

            if (elementsRetainPositions)
            {
                temp.ParametricPosition = paramPositionAccum;

                if (pointsAdded)
                {
                    if (i == 0 || i == transformPoints.Count - 2)
                    {
                        paramPositionAccum += distanceBetweenFirstAndLast / 2.0f;
                    }
                    else
                    {
                        paramPositionAccum += (1.0f - distanceBetweenFirstAndLast) / (transformPoints.Count - 3);
                    }
                }
                else
                {

                    temp.ParametricPosition = (float)i / (transformPoints.Count - 1);

                }
            }
            else
            {
                temp.ParametricPosition = currentLength / Magnitude;

                if (i < transformPoints.Count - 1)
                {
                    currentLength += (transformPoints[i + 1] - transformPoints[i]).magnitude;
                }
            }

            temp.Position = transformPoints[i];
            Points[i] = temp;
        }

        for (int i = 0; i < Points.Count; i++)
        {
            if (i < Points.Count - 1)
            {
                Segments.Add(new Segment(Points[i], Points[i + 1]));
            }
            if (i == 0)
            {
                var temp = Segments[i];
                temp.IsFirst = true;
                Segments[i] = temp;
            }
            if (i == Points.Count - 2)
            {
                var temp = Segments[i];
                temp.IsLast = true;
                Segments[i] = temp;
            }
        }

    }

    public Vector3 GetPosition(float parametricPosition)
    {
        var segment = GetPathSegment(parametricPosition);

        var parametricPositionOnSegment = parametricPosition;
        if (segment.End.ParametricPosition - segment.Start.ParametricPosition > Mathf.Epsilon)
        {
            parametricPositionOnSegment = (parametricPosition - segment.Start.ParametricPosition) / (segment.End.ParametricPosition - segment.Start.ParametricPosition);
        }

        return segment.GetPositionOnSegment(parametricPositionOnSegment);
    }

    private Segment GetPathSegment(float parametricPosition)
    {
        var segment = new Segment();

        for (int i = 0; i < Points.Count; i++)
        {
            if (i < Points.Count - 1)
            {
                if (Points[i].ParametricPosition.Equals(parametricPosition) || parametricPosition > Points[i].ParametricPosition && parametricPosition < Points[i + 1].ParametricPosition)
                {
                    segment.Start = Points[i];
                    segment.End = Points[i + 1];
                    break;
                }
            }

            if (i == Points.Count - 1)
            {
                if (Points[i].ParametricPosition.Equals(parametricPosition) || parametricPosition > Points[i].ParametricPosition)
                {
                    segment.Start = Points[i];
                    segment.End = new PathPoint();
                    segment.End.ParametricPosition = parametricPosition;
                    segment.End.Position = Points[i].Position + (Points[i].Position - Points[i - 1].Position).normalized * Mathf.Abs(parametricPosition - Points[i].ParametricPosition) * Magnitude;
                    break;
                }
            }

            if (i == 0)
            {
                if (parametricPosition < Points[i].ParametricPosition)
                {
                    segment.Start = new PathPoint();
                    segment.Start.ParametricPosition = parametricPosition;
                    segment.Start.Position = Points[0].Position + (Points[0].Position - Points[1].Position).normalized * Mathf.Abs(parametricPosition) * Magnitude;

                    segment.End = Points[i];
                    break;
                }
            }
        }

        return segment;
    }
    public PathPoint GetPathPoint(Vector3 point)
    {
        var minimumDistance = Mathf.Infinity;

        PathPoint ClosestPoint = new PathPoint();
        Segment closestSegment = new Segment();

        for (int i = 0; i < Segments.Count; i++)
        {
            float distance;
            var pointOnSegment = Segments[i].GetPointOnSegment(point, out distance);
            if (distance < minimumDistance)
            {
                closestSegment = Segments[i];
                ClosestPoint = pointOnSegment;
                minimumDistance = distance;
            }
        }

        return ClosestPoint;
    }

    public float GetParametricPosition(Vector3 point)
    {
        return GetPathPoint(point).ParametricPosition;
    }
}
