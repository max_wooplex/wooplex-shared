#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

public class WooSceneViewWindow
{
    protected float width = 150.0f;
    protected float height = 300.0f;
    protected float x = 0.0f;
    protected float y = 0.0f;
    protected string closedButtonTitle = "...";
    protected string windowTitle = "WindowTitle";
    protected float titleHeight = 25.0f;

    protected bool showMenu = false;
    protected GameObject target;
    protected SceneView sceneView;


    public virtual void Show(SceneView sceneView, GameObject target)
    {
        this.target = target;
        this.sceneView = sceneView;
        Handles.BeginGUI();

        if (showMenu)
        {
            DrawBackground(new Rect(x, y, width + 5.0f, titleHeight));
            DrawBackground(new Rect(x, y + titleHeight, width + 5.0f, height));
        }

        GUILayout.BeginArea(new Rect(new Vector2(x, y), new Vector2(width, height + titleHeight)));
        GUILayout.BeginVertical();

        ProcessShowMenuButton();

        GUILayout.Space(7.0f);

        if (showMenu)
        {
            ProcessMenuWithBackground();
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
        Handles.EndGUI();
    }

    protected virtual void ProcessMenu()
    {

    }

    private void ProcessMenuWithBackground()
    {
        var rect = new Rect(0.0f, titleHeight, width, height);

        GUILayout.BeginVertical();
        ProcessMenu();
        GUILayout.EndVertical();
        BlockClickThrough(rect);
    }

    private void DrawBackground(Rect rect)
    {
        var prevColor = GUI.backgroundColor;
        GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.3f * (target.activeInHierarchy ? 1.0f : 2.0f));
        GUI.Box(rect, GUIContent.none);
        GUI.backgroundColor = prevColor;
    }

    private void BlockClickThrough(Rect rect)
    {
        GUI.Button(rect, "", GUIStyle.none);
    }

    private void ProcessShowMenuButton()
    {
        if (!showMenu)
        {
            showMenu = GUILayout.Button(closedButtonTitle, GUILayout.Width(25.0f));
        }
        else
        {
            var titleRect = new Rect(0.0f, 0.0f, width, titleHeight);

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            GUIStyle style = new GUIStyle();
            style.fontSize = 12;
            style.alignment = TextAnchor.MiddleCenter;
            style.normal.textColor = Color.white;
            GUILayout.Space(28.0f);
            GUILayout.Label(windowTitle, style, GUILayout.Height(25.0f));

            GUILayout.FlexibleSpace();
            showMenu = !GUILayout.Button("X", GUILayout.Width(25.0f));
            GUILayout.Space(3.0f);
            GUILayout.EndHorizontal();
            BlockClickThrough(titleRect);
        }
    }
}
#endif
