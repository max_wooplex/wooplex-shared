﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using Wooplex.Panels;
using UnityEditor;
using UnityEngine.UI;

public class WooPanelSceneViewWindow : WooSceneViewWindow
{
    public WooPanelSceneViewWindow()
    {
        this.x = 2.0f;
        this.y = 22.0f;
        this.width = 130.0f;
        this.height = 220.0f;
        this.windowTitle = "Snippets";

        FromTopLeft = (Texture)Resources.Load("WooPanels/Textures/FromTopLeft");
        FromTopMid = (Texture)Resources.Load("WooPanels/Textures/FromTopMid");
        FromTopRight = (Texture)Resources.Load("WooPanels/Textures/FromTopRight");

        FromMidLeft = (Texture)Resources.Load("WooPanels/Textures/FromMidLeft");
        FromMid = (Texture)Resources.Load("WooPanels/Textures/FromMid");
        FromMidRight = (Texture)Resources.Load("WooPanels/Textures/FromMidRight");

        FromBotLeft = (Texture)Resources.Load("WooPanels/Textures/FromBotLeft");
        FromBotMid = (Texture)Resources.Load("WooPanels/Textures/FromBotMid");
        FromBotRight = (Texture)Resources.Load("WooPanels/Textures/FromBotRight");

        ToTopLeft = (Texture)Resources.Load("WooPanels/Textures/ToTopLeft");
        ToTopMid = (Texture)Resources.Load("WooPanels/Textures/ToTopMid");
        ToTopRight = (Texture)Resources.Load("WooPanels/Textures/ToTopRight");

        ToMidLeft = (Texture)Resources.Load("WooPanels/Textures/ToMidLeft");
        ToMid = (Texture)Resources.Load("WooPanels/Textures/ToMid");
        ToMidRight = (Texture)Resources.Load("WooPanels/Textures/ToMidRight");

        ToBotLeft = (Texture)Resources.Load("WooPanels/Textures/ToBotLeft");
        ToBotMid = (Texture)Resources.Load("WooPanels/Textures/ToBotMid");
        ToBotRight = (Texture)Resources.Load("WooPanels/Textures/ToBotRight");
    }

    WooPanel panel;
    WooDraggableContent draggableContent;

    private Texture FromLeftToLeftTexture;
    private Texture FromTopLeft;
    private Texture FromTopMid;
    private Texture FromTopRight;
    private Texture FromMidLeft;
    private Texture FromMid;
    private Texture FromMidRight;
    private Texture FromBotLeft;
    private Texture FromBotMid;
    private Texture FromBotRight;

    private Texture ToTopLeft;
    private Texture ToTopMid;
    private Texture ToTopRight;

    private Texture ToMidLeft;
    private Texture ToMid;
    private Texture ToMidRight;

    private Texture ToBotLeft;
    private Texture ToBotMid;
    private Texture ToBotRight;

    private float expandTreshold = 1050.0f;
    private float expandedWidth = 280.0f;
    private float expandedHeight = 515.0f;
    private float normalWidth = 130.0f;
    private float normalHeight = 270.0f;

    protected override void ProcessMenu()
    {
        if (panel == null)
        {
            return;
        }

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Space(4.0f);
        ProcessPanelTitle(panel);
        GUILayout.Space(4.0f);
        ProcessNewPanelButtons();
        GUILayout.Space(7.0f);

        //ProcessHierarchy(panel);

        if (!(panel is WooTabBar || panel is WooNavigationBar))
        {
            ProcessTabBar(panel);
            ProcessNavigationBar(panel);
           // ProcessCarousel(panel);
        }

        if (panel is WooTabBar)
        {
           // ProcessTabButton(panel);
        }

        GUILayout.Space(7.0f);

        ProcessEffects(panel);

        GUILayout.Space(7.0f);

        if (!(panel is WooTabBar || panel is WooNavigationBar))
        {
            ProcessDraggableContent(panel);
        }


        if (sceneView.camera.pixelHeight < expandTreshold)
        {
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
        }

        if (draggableContent != null)
        {
            DrawPresetsButtons();
        }
        if (sceneView.camera.pixelHeight < expandTreshold)
        {
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();

    }

    //private void ProcessHierarchy(WooPanel panel)
    //{
    //    return;
    //    var childPanels = WooPanelsEngine.Instance.GetChildren(panel);
    //    var parentPanel = panel.GetParentPanel();
    //    System.Collections.Generic.List<WooPanelNode> siblings = null;

    //    if (parentPanel != null)
    //    {
    //        siblings = WooPanelsEngine.Instance.GetChildren(parentPanel);
    //    }
    //    else
    //    {
    //        siblings = WooPanelsEngine.Instance.allRootNodes[panel.transform.parent];
    //    }

    //    this.height = this.height + childPanels.Count * 20.0f + siblings.Count * 20.0f;

    //    foreach (var sibling in siblings)
    //    {
    //        GUILayout.Button(sibling.Panel.name);

    //        if (sibling.Panel == panel)
    //        {
    //            foreach (var child in childPanels)
    //            {
    //                GUILayout.BeginHorizontal();
    //                GUILayout.Space(15.0f);
    //                GUILayout.Button(child.Panel.name);
    //                GUILayout.EndHorizontal();
    //            }
    //        }
    //    }
    //}

    private void ProcessNewPanelButtons()
    {
        var prevColor = GUI.color;
        var fadedPrevColor = prevColor;
        fadedPrevColor.a = 1.0f;

        GUI.color = panel.IsOpened() ? Color.green : panel.IsOpenedOrOpening() ? new Color(1.0f, 0.7f, 0.2f) : panel.IsClosing() ? fadedPrevColor : prevColor;

        if (GUILayout.Button(panel.IsOpened() ? "Close" : panel.IsOpening() ? "Opening" : panel.IsClosing() ? "Closing" : "Open"))
        {
            //Selection.activeInstanceID = instanceID;
            panel.Toggle();
        }
        GUI.color = prevColor;
        GUILayout.Space(2.0f);
        if (GUILayout.Button("Create Child"))
        {
            CreatePanel(panel.transform);
        }
        if (GUILayout.Button("Create Sibling"))
        {
            CreatePanel(panel.transform.parent);
        }
    }

    public override void Show(SceneView sceneView, GameObject target)
    {
        panel = WooPanelsEngine.Instance.GetSelectedPanel(target.transform);
        draggableContent = panel.Movement.DraggableContent;

        if (draggableContent != null)
        {
            if (sceneView.camera.pixelHeight < expandTreshold)
            {
                width = expandedWidth;
                height = normalHeight;
            }
            else
            {
                height = expandedHeight;
                width = normalWidth;
            }
        }
        else
        {
            height = normalHeight;
            width = normalWidth;
        }

        if (draggableContent != null && target == draggableContent.gameObject)
        {
            this.y = 20.0f;
        }
        else
        {
            this.y = 2.0f;
        }

        base.Show(sceneView, target);
    }

    private void DrawPresetsButtons()
    {
        var rectTransform = panel.GetComponent<RectTransform>();
        var draggableContentRectTransform = draggableContent.GetComponent<RectTransform>();

        var panelHeight = rectTransform.rect.height * panel.Canvas.scaleFactor;
        var panelWidth = rectTransform.rect.width * panel.Canvas.scaleFactor;
        var contentHeight = draggableContentRectTransform.rect.height * panel.Canvas.scaleFactor;
        var contentWidth = draggableContentRectTransform.rect.width * panel.Canvas.scaleFactor;


        GUIStyle style = new GUIStyle(GUI.skin.label);
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;

        GUILayout.Space(4.0f);

        GUILayout.Label("From", style, GUILayout.ExpandWidth(true));

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        // From Top Left
        if (GUILayout.Button(FromTopLeft, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, 0.0f, panelHeight, -contentWidth / 2.0f, contentHeight / 2.0f);
        }

        // From Top Mid
        if (GUILayout.Button(FromTopMid, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, panelWidth / 2.0f, panelHeight, 0.0f, contentHeight / 2.0f);
        }

        // From Top Right
        if (GUILayout.Button(FromTopRight, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, panelWidth, panelHeight, contentWidth / 2.0f, contentHeight / 2.0f);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        // From Mid Left
        if (GUILayout.Button(FromMidLeft, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, 0.0f, panelHeight / 2.0f, -contentWidth / 2.0f, 0.0f);
        }

        // From Mid
        if (GUILayout.Button(FromMid, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, panelWidth / 2.0f, panelHeight / 2.0f, 0.0f, 0.0f);
        }

        // From Mid Right
        if (GUILayout.Button(FromMidRight, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, panelWidth, panelHeight / 2.0f, contentWidth / 2.0f, 0.0f);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        // From Bot Left
        if (GUILayout.Button(FromBotLeft, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, 0.0f, 0.0f, -contentWidth / 2.0f, -contentHeight / 2.0f);
        }
        // From Bot Mid
        if (GUILayout.Button(FromBotMid, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, panelWidth / 2.0f, 0.0f, 0.0f, -contentHeight / 2.0f);
        }
        // From Bot Right
        if (GUILayout.Button(FromBotRight, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragFrom, panelWidth, 0.0f, contentWidth / 2.0f, -contentHeight / 2.0f);
        }
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Label("To", style, GUILayout.ExpandWidth(true));

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        // To Top Left
        if (GUILayout.Button(ToTopLeft, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, 0.0f, panelHeight, contentWidth / 2.0f, -contentHeight / 2.0f);
        }

        // To Top Mid
        if (GUILayout.Button(ToTopMid, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, panelWidth / 2.0f, panelHeight, 0.0f, -contentHeight / 2.0f);
        }

        // To Top Right
        if (GUILayout.Button(ToTopRight, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, panelWidth, panelHeight, -contentWidth / 2.0f, -contentHeight / 2.0f);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        // To Mid Left
        if (GUILayout.Button(ToMidLeft, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, 0.0f, panelHeight / 2.0f, contentWidth / 2.0f, 0.0f);
        }

        // To Mid
        if (GUILayout.Button(ToMid, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, panelWidth / 2.0f, panelHeight / 2.0f, 0.0f, 0.0f);
        }

        // To Mid Right
        if (GUILayout.Button(ToMidRight, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, panelWidth, panelHeight / 2.0f, -contentWidth / 2.0f, 0.0f);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        // To Bot Left
        if (GUILayout.Button(ToBotLeft, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, 0.0f, 0.0f, contentWidth / 2.0f, contentHeight / 2.0f);
        }
        // To Bot Mid
        if (GUILayout.Button(ToBotMid, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, panelWidth / 2.0f, 0.0f, 0.0f, contentHeight / 2.0f);
        }
        // To Bot Right
        if (GUILayout.Button(ToBotRight, GUILayout.Width(30.0f), GUILayout.Height(30.0f)))
        {
            MoveGameObjectAndContent(panel.Movement.DragTo, panelWidth, 0.0f, -contentWidth / 2.0f, contentHeight / 2.0f);
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    private void MoveGameObjectAndContent(GameObject gameObjectToMove, float goDeltaX, float goDeltaY, float contentDeltaX, float contentDeltaY)
    {
        if (gameObjectToMove == panel.Movement.DragFrom)
        {
            panel.Close();
        }
        else
        {
            panel.Open();
        }
        WooPanelsEngine.Instance.FinishOpeningAndClosingForPanel(panel);

        var rect = panel.GetComponent<RectTransform>().rect;
        var panelRectWidth = rect.width * panel.Canvas.scaleFactor;
        var panelRectHeight = rect.height * panel.Canvas.scaleFactor;
        var contentTransform = panel.transform;
        var contentRectTransform = contentTransform.GetComponent<RectTransform>();

        var pos = contentTransform.position - new Vector3(panelRectWidth * contentRectTransform.pivot.x, panelRectHeight * contentRectTransform.pivot.y, 0.0f);

        Undo.RecordObjects(new[] { gameObjectToMove.transform, draggableContent.transform }, "asd");
        var newGameObjectToMovePosition = pos + new Vector3(goDeltaX, goDeltaY, gameObjectToMove.transform.position.z);

        draggableContent.transform.position = newGameObjectToMovePosition + new Vector3(contentDeltaX, contentDeltaY, draggableContent.transform.position.z);
        gameObjectToMove.transform.position = newGameObjectToMovePosition;
        UpdateAnchorValues(gameObjectToMove, gameObjectToMove.transform.parent.gameObject);
        gameObjectToMove.transform.position = newGameObjectToMovePosition;



        Selection.activeObject = draggableContent.gameObject;

        if (panel.IsClosed())
        {
            panel.Movement.DragFromAnchor.transform.position = newGameObjectToMovePosition;
            UpdateAnchorValues(panel.Movement.DragFromAnchor, draggableContent.gameObject);
            panel.Movement.DragFromAnchor.transform.position = newGameObjectToMovePosition;
        }
        else
        {
            panel.Movement.DragToAnchor.transform.position = newGameObjectToMovePosition;
            UpdateAnchorValues(panel.Movement.DragToAnchor, draggableContent.gameObject);
            panel.Movement.DragToAnchor.transform.position = newGameObjectToMovePosition;
        }

    }

    private void UpdateAnchorValues(GameObject anchor, GameObject content)
    {
        var prevPivot = content.GetComponent<RectTransform>().pivot;
        var newPivot = new Vector2(0.5f, 0.5f);

        content.GetComponent<RectTransform>().pivot = newPivot;
        anchor.GetComponent<RectTransform>().pivot = newPivot;

        var w = content.GetComponent<RectTransform>().rect.width;

        var halfW = w / 2.0f;
        var h = content.GetComponent<RectTransform>().rect.height;
        var halfH = h / 2.0f;

        var localPosition = anchor.transform.position - content.transform.position;

        var anchorMinX = (halfW + localPosition.x) / w;
        var anchorMinY = (halfH + localPosition.y) / h;

        anchor.GetComponent<RectTransform>().anchorMin = new Vector2(anchorMinX, anchorMinY);
        anchor.GetComponent<RectTransform>().anchorMax = new Vector2(anchorMinX, anchorMinY);
        content.GetComponent<RectTransform>().pivot = prevPivot;
    }


    private void ProcessDraggableContent(WooPanel panel)
    {
        var draggableContent = GetComponentInFirstChildren<WooDraggableContent>(panel) as WooDraggableContent;
        if (draggableContent == null)
        {
            if (GUILayout.Button("Draggable Content"))
            {
                AddDraggableContent(panel);
            }
        }
        else
        {
            var prevColor = GUI.color;
            GUI.color = Color.green;
            if (GUILayout.Button("Draggable Content"))
            {
                RemoveDraggableContent(draggableContent);
            }
            GUI.color = prevColor;
        }
    }

    private void RemoveDraggableContent(WooDraggableContent draggableContent)
    {
        var panel = draggableContent.Panel;

        if (draggableContent != null)
        {
            GameObject.DestroyImmediate(draggableContent);
        }

        if (panel.Movement.DragFrom != null)
        {
            GameObject.DestroyImmediate(panel.Movement.DragFrom);
        }
        if (panel.Movement.DragTo != null)
        {
            GameObject.DestroyImmediate(panel.Movement.DragTo);
        }
        if (panel.Movement.DragFromAnchor != null)
        {
            GameObject.DestroyImmediate(panel.Movement.DragFromAnchor);
        }
        if (panel.Movement.DragToAnchor != null)
        {
            GameObject.DestroyImmediate(panel.Movement.DragToAnchor);
        }

        panel.PanelProperties.ActiveWhenClosed = false;

        if (panel.GetComponent<Animator>().runtimeAnimatorController.name == "Always Visible Panel Controller")
        {
            panel.GetComponent<Animator>().runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("WooPanels/Animators/Default Panel Controller");
        }
    }

    private void AddDraggableContent(WooPanel panel)
    {
        var content = GetOrCreateContent(panel.transform);

        var contentGo = content.gameObject;

        if (contentGo != null)
        {
            var draggableContent = contentGo.AddComponent<WooDraggableContent>();
            draggableContent.Panel = panel;
            panel.Movement.DraggableContent = draggableContent;
            Selection.activeTransform = draggableContent.transform;
        }

        if (panel.GetComponent<Animator>().runtimeAnimatorController.name == "Default Panel Controller")
        {
            panel.GetComponent<Animator>().runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("WooPanels/Animators/Always Visible Panel Controller");
        }

        panel.PanelProperties.ActiveWhenClosed = true;
        panel.Movement.DragFrom = CreateAndHideGameObjectIfNecessary(panel.gameObject, "DragFrom", Vector3.zero, panel);
        panel.Movement.DragTo = CreateAndHideGameObjectIfNecessary(panel.gameObject, "DragTo", Vector3.zero, panel);
        panel.Movement.DragFromAnchor = CreateAndHideGameObjectIfNecessary(panel.Movement.DraggableContent.gameObject, "DragFromAnchor", Vector3.zero, panel);
        panel.Movement.DragToAnchor = CreateAndHideGameObjectIfNecessary(panel.Movement.DraggableContent.gameObject, "DragToAnchor", Vector3.zero, panel);
    }

    private GameObject CreateAndHideGameObjectIfNecessary(GameObject parent, string name, Vector3 localPosition, WooPanel panel)
    {
        var goTransform = parent.transform.Find(name);
        GameObject result;

        if (goTransform == null)
        {
            result = new GameObject();
            result.AddComponent<RectTransform>();
            result.transform.parent = parent.transform;
            result.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

            result.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0.0f);
            result.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0.0f);

            if (panel.Canvas.renderMode != RenderMode.WorldSpace)
            {
                result.transform.localPosition = localPosition * panel.Canvas.GetComponent<CanvasScaler>().referencePixelsPerUnit;
            }
            result.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;

            result.name = name;
        }
        else
        {
            result = goTransform.gameObject;
        }

        return result;
    }

    private void ProcessTabButton(WooPanel panel)
    {
        if (GUILayout.Button("Add Tab Button"))
        {
            CreateTabButton(panel);
        }
    }

    private void ProcessTabBar(WooPanel panel)
    {
        var oldTabBar = GetComponentInFirstChildren<WooTabBar>(panel);

        if (oldTabBar != null)
        {
            var prevColor = GUI.color;
            GUI.color = Color.green;
            if (GUILayout.Button("Tab Bar"))
            {
                RemoveTabBar(panel);
            }
            GUI.color = prevColor;
        }
        else
        {
            if (GUILayout.Button("Tab Bar"))
            {
                CreateTabBar(panel);
            }
        }
    }

    private void CreateTabButton(WooPanel panel)
    {
        var tabPrefab = Resources.Load("WooPanels/Prefabs/Tab Button");

        var tabButtonsContainer = GetOrCreateContent(panel.transform);

        var parentPanel = panel.transform.parent.GetComponent<WooPanel>();
        if (parentPanel != null)
        {
            var newPanelContent = GetOrCreateContent(parentPanel.transform);

            if (newPanelContent != null)
            {
                var newPanel = CreatePanel(newPanelContent);

                if (tabButtonsContainer != null)
                {
                    var tabButton = (GameObject.Instantiate(tabPrefab, tabButtonsContainer.transform) as GameObject).transform;
                    tabButton.name = "Tab Button";
                    tabButton.GetComponent<WooTabButton>().Panel = newPanel;
                    Selection.activeTransform = panel.transform;
                }
                else
                {
                    //Debug.LogWarning("There should be Content gameObject in order to create tab button");
                }
            }
        }

    }

    private void ProcessCarousel(WooPanel panel)
    {
        var oldCarousel = GetComponentInFirstChildren<WooTabBar>(panel);

        if (oldCarousel != null)
        {
            var prevColor = GUI.color;
            GUI.color = Color.green;
            if (GUILayout.Button("Carousel"))
            {
                RemoveCarousel(panel);
            }
            GUI.color = prevColor;
        }
        else
        {
            if (GUILayout.Button("Carousel"))
            {
                CreateCarousel(panel);
            }
        }
    }

    private void CreateCarousel(WooPanel panel)
    {

    }

    private void RemoveCarousel(WooPanel panel)
    {

    }

    WooPanel CreatePanel(Transform parentTransform = null)
    {
        var go = new GameObject();
        var selectedGo = Selection.activeGameObject as GameObject;

        if (selectedGo != null)
        {
            go.transform.SetParent(selectedGo.transform);
        }

        go.AddComponent<WooPanel>();
        go.GetComponent<Animator>().runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("WooPanels/Animators/Default Panel Controller");

        var panelRectTransform = go.AddComponent<RectTransform>();


        go.name = "New Panel";
        go.transform.SetParent(parentTransform);
        Selection.activeGameObject = go;

        var contentGo = new GameObject();
        contentGo.name = "Content";
        var contentRectTransform = contentGo.AddComponent<RectTransform>();

        contentGo.transform.SetParent(go.transform);

        if (go.GetComponentInParent<Canvas>() == null)
        {
            GameObject canvasGo = null;

            if (canvasGo == null)
            {
                canvasGo = new GameObject();
                canvasGo.AddComponent<Canvas>();
                canvasGo.transform.SetParent(go.transform.parent);
                canvasGo.name = "Canvas";
                canvasGo.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
                canvasGo.AddComponent<CanvasScaler>();
            }

            go.transform.SetParent(canvasGo.transform);
        }

        SetDefaultRect(panelRectTransform);
        SetDefaultRect(contentRectTransform);

        if (GameObject.FindObjectOfType<PanelManager>() == null)
        {
            var panelManager = new GameObject();
            panelManager.AddComponent<PanelManager>();
            panelManager.name = "Panel Manager";
        }

        return go.GetComponent<WooPanel>();
    }

    private Transform CreateContent(Transform parentTransform)
    {
        var contentGo = new GameObject();
        contentGo.name = "Content";
        var contentRectTransform = contentGo.AddComponent<RectTransform>();

        contentGo.transform.SetParent(parentTransform);
        SetDefaultRect(contentRectTransform);

        return contentGo.transform;
    }

    private Transform GetOrCreateContent(Transform parentTransform)
    {
        var result = parentTransform.Find("Content");

        if (result == null)
        {
            result = CreateContent(parentTransform);
        }

        return result;
    }

    private void SetDefaultRect(RectTransform rectTransform)
    {
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(1, 1);
        rectTransform.sizeDelta = new Vector2(0.0f, 0.0f);

        rectTransform.pivot = new Vector2(0.5f, 0.5f);
        rectTransform.localScale = Vector3.one;
        rectTransform.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    }

    private void ProcessNavigationBar(WooPanel panel)
    {
        var oldTabBar = GetComponentInFirstChildren<WooNavigationBar>(panel);

        if (oldTabBar != null)
        {
            var prevColor = GUI.color;
            GUI.color = Color.green;
            if (GUILayout.Button("Navigation Bar"))
            {
                RemoveNavBar(panel);
            }
            GUI.color = prevColor;
        }
        else
        {
            if (GUILayout.Button("Navigation Bar"))
            {
                CreateNavBar(panel);
            }
        }
    }

    private void RemoveNavBar(WooPanel panel)
    {
        var oldTabBar = GetComponentInFirstChildren<WooNavigationBar>(panel);

        if (oldTabBar != null)
        {
            GameObject.DestroyImmediate(oldTabBar.gameObject);
        }
    }

    private void CreateNavBar(WooPanel panel)
    {
        var tabContainerPrefab = Resources.Load("WooPanels/Prefabs/Navigation Bar");

        var oldTabBar = GetComponentInFirstChildren<WooNavigationBar>(panel);

        if (oldTabBar == null)
        {
            var tabBar = (GameObject.Instantiate(tabContainerPrefab, panel.transform) as GameObject);
            WooPanelsEngine.Instance.Rebuild();
            tabBar.GetComponent<WooPanel>().Open();
            tabBar.name = "Navigation Bar";
        }
    }

    private Component GetComponentInFirstChildren<T>(MonoBehaviour mb) where T : MonoBehaviour
    {
        for (int i = 0; i < mb.transform.childCount; i++)
        {
            var searchedComponent = mb.transform.GetChild(i).GetComponent<T>();

            if (searchedComponent != null)
            {
                return searchedComponent;
            }
        }

        return null;
    }

    private void RemoveTabBar(WooPanel panel)
    {
        var oldTabBar = GetComponentInFirstChildren<WooTabBar>(panel);

        if (oldTabBar != null)
        {
            GameObject.DestroyImmediate(oldTabBar.gameObject);
        }
    }

    private void CreateTabBar(WooPanel panel)
    {
        var tabContainerPrefab = Resources.Load("WooPanels/Prefabs/Tab Bar");

        var oldTabBar = GetComponentInFirstChildren<WooTabBar>(panel);

        if (oldTabBar == null)
        {
            var tabBar = (GameObject.Instantiate(tabContainerPrefab, panel.transform) as GameObject);
            WooPanelsEngine.Instance.Rebuild();
            tabBar.GetComponent<WooPanel>().Open();
            tabBar.name = "Tab Bar";
            Selection.activeTransform = tabBar.transform;
        }
    }

    private void ProcessEffects(WooPanel panel)
    {
        var effectsGo = panel.transform.Find("Effects");
        Transform blur = effectsGo != null ? effectsGo.Find("Blur") : null;
        Transform whiten = effectsGo != null ? effectsGo.Find("Whiten") : null;
        Transform darken = effectsGo != null ? effectsGo.Find("Darken") : null;
        var prevColor = GUI.color;
        var blurSelected = false;
        var whiteSelected = false;
        var darkSelected = false;

        GUI.color = blur ? Color.green : prevColor;
        blurSelected = GUILayout.Button("Blur Background");
        GUI.color = whiten ? Color.green : prevColor;
        whiteSelected = GUILayout.Button("Whiten Background");
        GUI.color = darken ? Color.green : prevColor;
        darkSelected = GUILayout.Button("Darken Background");
        GUI.color = Color.white;

        if (blurSelected || whiteSelected || darkSelected)
        {
            panel.Open();
            if (panel.transform.Find("Effects") == null)
            {
                effectsGo = CreateGameObject("Effects", panel.transform).transform;
                effectsGo.transform.SetAsFirstSibling();
            }

            if (blurSelected)
            {
                if (blur == null)
                {
                    var go = CreateGameObjectWithRawImage("Blur", effectsGo);
                    var blurMaterial = Resources.Load<Material>("WooPanels/Materials/Blurred"); ;

                    go.GetComponent<RawImage>().material = blurMaterial;
                }
                else
                {
                    GameObject.DestroyImmediate(blur.gameObject);
                }
            }

            if (whiteSelected)
            {
                if (whiten == null)
                {
                    var go = CreateGameObjectWithRawImage("Whiten", effectsGo);
                    go.GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0.3f);
                }
                else
                {
                    GameObject.DestroyImmediate(whiten.gameObject);
                }
            }
            if (darkSelected)
            {
                if (darken == null)
                {
                    var go = CreateGameObjectWithRawImage("Darken", effectsGo);
                    go.GetComponent<RawImage>().color = new Color(0.0f, 0.0f, 0.0f, 0.3f);
                }
                else
                {
                    GameObject.DestroyImmediate(darken.gameObject);
                }
            }
        }
    }

    private GameObject CreateGameObjectWithRawImage(string name, Transform transform)
    {
        var go = CreateGameObject(name, transform);

        go.AddComponent<RawImage>();

        return go;
    }

    private GameObject CreateGameObject(string name, Transform transform)
    {
        var go = new GameObject();

        go.transform.parent = transform;
        var panelRectTransform = go.AddComponent<RectTransform>();

        panelRectTransform.anchorMin = new Vector2(0, 0);
        panelRectTransform.anchorMax = new Vector2(1, 1);
        panelRectTransform.sizeDelta = new Vector2(0.0f, 0.0f);

        panelRectTransform.pivot = new Vector2(0.5f, 0.5f);
        panelRectTransform.localScale = Vector3.one;
        panelRectTransform.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

        go.name = name;

        return go;
    }

    private void ProcessPanelTitle(WooPanel panel)
    {
        GUILayout.BeginHorizontal();
        var prevColor = GUI.backgroundColor;
        GUI.backgroundColor = Color.white;
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        //  style.fontSize = 12;
        style.alignment = TextAnchor.MiddleCenter;
        //  GUILayout.FlexibleSpace();
        GUILayout.Label("Title:", style, GUILayout.Height(17.0f));
        GUI.backgroundColor = prevColor;
        panel.Title = GUILayout.TextField(panel.Title, GUILayout.Width(90.0f));
        // GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }
}
#endif