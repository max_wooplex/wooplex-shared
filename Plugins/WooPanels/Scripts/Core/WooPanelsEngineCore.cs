﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Wooplex.Panels;

public class WooPanelsEngineCore 
{
    public Dictionary<WooPanel, WooPanelNode> panelNodes = new Dictionary<WooPanel, WooPanelNode>();
    public Dictionary<Transform, int> rootsHierarchyIndeces;
    public Dictionary<Transform, List<WooPanelNode>> allRootNodes;
    public Dictionary<int, WooPanel> panelIds = new Dictionary<int, WooPanel>();

    public bool isBuilt = false;

    protected System.DateTime prevTime;

    private const string DEFAULT_ANIMATOR_NAME = "WooPanels/Animators/Default Panel Controller";
    private RuntimeAnimatorController defaultAnimator = null;

    private List<WooPanel> panelsToRegister = new List<WooPanel>();
    private List<WooPanel> panelsToDelete = new List<WooPanel>();

    private bool canRegister = true;
    private bool canDelete = true;

    public bool RegisterPanel(WooPanel panel)
    {
        //if (!canRegister)
        //{
        //    panelsToRegister.Add(panel);
        //}
        //else 
       if (!panelNodes.ContainsKey(panel))
        {
            AddPanelToEngine(panel);
            panel.Init();
            panel.CloseImmediately();
        }

        return false;
    }

    public void DisableRegisteringAndUnregistering()
    {
        canRegister = false;
        canDelete = false;
    }

    public void EnableRegisteringAndUnregistering()
    {
        canRegister = true;
        canDelete = true;

        foreach (var panel in panelsToRegister)
        {
            RegisterPanel(panel);
        }

        foreach (var panel in panelsToDelete)
        {
            RemovePanelNode(panel);
        }

        panelsToRegister.Clear();
        panelsToDelete.Clear();
    }

    public void EnableAll()
    {
        //DisableRegisteringAndUnregistering();
        for (int i = 0; i < panelNodes.Count; i++)
        {
            var panel = panelNodes.ElementAt(i);

            if (panel.Value.Panel != null)
            {
                panel.Value.Panel.gameObject.SetActive(true);
            }
        }
        //EnableRegisteringAndUnregistering();
    }

    public void DisableAll()
    {
        //DisableRegisteringAndUnregistering();
        for (int i = 0; i < panelNodes.Count; i++)
        {
            var panel = panelNodes.ElementAt(i);

            if (panel.Value.Panel != null)
            {
                panel.Value.Panel.gameObject.SetActive(false);
            }
        }
        //EnableRegisteringAndUnregistering();
    }

    public void ProcessAlwaysOnTop(WooPanel panel)
    {
        if (panel.PanelProperties.AlwaysOnTop)
        {
            panel.transform.SetAsLastSibling();
        }
    }

    public bool IsWaitingToClose(WooPanel panel)
    {
        return panelNodes[panel].IsWaitingToClose;
    }

    public bool IsOpen(WooPanel panel)
    {
        return panelNodes[panel].IsOpen;
    }

    public bool IsClosed(WooPanel panel)
    {
        return panelNodes[panel].IsClosed;
    }

    public WooPanel GetParentPanel(WooPanel panel)
    {
        var cursor = panel.transform.parent;

        while (cursor != null)
        {
            var result = cursor.GetComponent<WooPanel>();
            if (result != null)
            {
                return result;
            }
            cursor = cursor.parent;
        }

        return null;
    }

    public WooPanel GetSelectedPanel(Transform transform)
    {
        var cur = transform;
        WooPanel result = null;

        while (cur != null)
        {
            result = cur.GetComponent<WooPanel>();
            if (result != null)
            {
                return result;
            }
            cur = cur.parent;
        }

        return result;
    }

    public void MakeTreeActive(WooPanel panel)
    {
        var parent = GetParent(panel);

        panel.gameObject.SetActive(true);

        if (parent != null)
        {
            MakeTreeActive(parent.Panel);
        }
    }

    public void RebuildIfPanelGameObjectIsNull(WooPanel panel)
    {
        if (panel.gameObject == null)
        {
            Rebuild();
        }

        if (panel.gameObject == null)
        {
            return;
        }
    }

    public Transform GetRoot(WooPanel panel)
    {
        var underRootPanel = GetParent(panel);
        var underRootTransform = panel.transform;

        if (underRootPanel != null && underRootPanel.Panel != null)
        {
            underRootTransform = underRootPanel.Panel.transform;
        }

        return underRootTransform.parent;
    }

    public bool HasInTree(WooPanel panel, WooPanel child)
    {
        var children = GetChildren(panel);

        for (int i = 0; i < children.Count; i++)
        {
            if (HasInTree(children[i].Panel, child))
            {
                return true;
            }
        }

        if (panel == child)
        {
            return true;
        }

        return false;
    }

    public List<WooPanelNode> GetRootNodes(WooPanel panel)
    {
        return allRootNodes[GetRoot(panel)];
    }

    public WooPanelNode GetParent(WooPanel panel)
    {
        return panelNodes.ContainsKey(panel) ? panelNodes[panel].Parent as WooPanelNode : null;
    }

    public List<WooPanelNode> GetChildren(WooPanel panel)
    {
        return panelNodes.ContainsKey(panel) ? panelNodes[panel].Children : null;
    }

    List<T> GetAllOnScene<T>() where T : Component
    {
        List<T> objectsInScene = new List<T>();

        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            var wooPanel = go.GetComponent<T>();
            if (go.scene != SceneManager.GetActiveScene())
            {
                continue;
            }
            if (wooPanel != null)
            {
                objectsInScene.Add(wooPanel);
            }
        }

        return objectsInScene;
    }

    public void RemovePanelNode(WooPanel panel)
    {
        if (!canDelete)
        {
            panelsToDelete.Add(panel);
            return;
        }

        var panelNode = panelNodes.ContainsKey(panel) ? panelNodes[panel] : null;
        var root = GetRoot(panel);

        if (panelNode != null)
        {
            var rootNode = allRootNodes.ContainsKey(root) ? allRootNodes[root] : null;
            if (rootNode != null && rootNode.Contains(panelNode))
            {
                rootNode.Remove(panelNode);
            }

            panelNodes.Remove(panel);
        }

        var id = panel.gameObject.GetInstanceID();

        if (panelIds.ContainsKey(id))
        {
            panelIds.Remove(id);
        }
    }

    public WooPanelNode GetOrCreatePanelNode(WooPanel panel)
    {
        WooPanelNode panelNode;

        if (!panelNodes.ContainsKey(panel))
        {
            panelNode = new WooPanelNode();
            panelNode.Panel = panel;
            panelNodes.Add(panel, panelNode);
        }
        else
        {
            panelNode = panelNodes[panel];
            panelNode.Panel = panel;
        }
        var id = panel.gameObject.GetInstanceID();

        if (!panelIds.ContainsKey(id))
        {
            panelIds.Add(id, panel);
        }
        else
        {
            panelIds[id] = panel;
        }
            
        return panelNode;
    }

    private void InitializePanelAnimator(WooPanel panel)
    {
        if (panel.GetComponent<Animator>() != null && panel.GetComponent<Animator>().runtimeAnimatorController == null)
        {
            panel.GetComponent<Animator>().runtimeAnimatorController = defaultAnimator == null ? 
                Resources.Load(DEFAULT_ANIMATOR_NAME) as RuntimeAnimatorController : defaultAnimator;
        }
    }

    public void OnDisable()
    {
        isBuilt = false;
    }

    public void TryToRebuildIfNecessary()
    {
        if (!isBuilt)
        {
            Rebuild();
        }
        else
        {
            foreach (var panel in panelNodes)
            {
                if (panel.Value.Panel == null || panel.Value.Panel.gameObject == null)
                {
                    isBuilt = false;
                }
            }
            if (!isBuilt)
            {
                Rebuild();
            }
        }
    }

    public void Rebuild()
    {
        prevTime = System.DateTime.Now;
        isBuilt = true;

        allRootNodes = new Dictionary<Transform, List<WooPanelNode>>();
        rootsHierarchyIndeces = new Dictionary<Transform, int>();

        var panels = GetAllOnScene<WooPanel>();

        foreach (var key in panelNodes.Keys)
        {
            panelNodes[key].Panel = null;
            panelNodes[key].Children = new List<WooPanelNode>();
        }

        for (int i = 0; i < panels.Count; i++)
        {
            AddPanelToEngine(panels[i]);
        }

        RemoveNullPanels();
        SortNodes();
    }

    public void RemovePanelFromEngine(WooPanel panel)
    {
        WooPanel parentPanel = GetParentPanel(panel);
        WooPanelNode panelNode = panelNodes.ContainsKey(panel) ? panelNodes[panel] : null;

        if (parentPanel != null)
        {
            WooPanelNode parentNode = panelNodes.ContainsKey(parentPanel) ? panelNodes[parentPanel] : null;
            if (parentNode != null)
            {
                parentNode.Children.Remove(panelNode);
            }
        }

        if (panelNode != null)
        {
            for (int i = 0; i < panelNode.Children.Count; i++)
            {
                RemovePanelFromEngine(panelNode.Children[i].Panel);
            }
        }

        RemovePanelNode(panel);
    }

    private void AddPanelToEngine(WooPanel panel)
    {
        WooPanelNode panelNode = GetOrCreatePanelNode(panel);

        InitializePanelAnimator(panel);

        WooPanel parentPanel = GetParentPanel(panel);
        panel.PreviousDependentPanel = null;
        panel.SetCanvas();
       // panel.PanelState = PanelState.NotDefined;

        if (parentPanel != null)
        {
            WooPanelNode parentNode = GetOrCreatePanelNode(parentPanel);
            panelNode.Parent = parentNode;
            parentNode.Children.Add(panelNode);
            panelNode.ParentTransform = parentNode.Panel.transform;
        }
        else
        {
            var parentTransform = panel.transform.parent;

            if (parentTransform != null)
            {
                if (allRootNodes.ContainsKey(parentTransform))
                {
                    allRootNodes[parentTransform].Add(panelNode);
                }
                else
                {
                    List<WooPanelNode> rootNodes = new List<WooPanelNode>();
                    rootNodes.Add(panelNode);

                    allRootNodes.Add(parentTransform, rootNodes);
                }
            }

            panelNode.ParentTransform = parentTransform;
        }
    }

    public void InitPanels()
    {
        foreach (var node in panelNodes)
        {
            node.Value.Panel.Init();
        }
    }

    public void RemoveNullPanels()
    {
        List<WooPanel> toRemove = new List<WooPanel>();
        foreach (var key in panelNodes.Keys)
        {
            if (panelNodes[key].Panel == null)
            {
                toRemove.Add(key);
            }
        }
        foreach (var key in toRemove)
        {
            panelNodes.Remove(key);
        }

    }

    public void SortNodes()
    {
        var currentIndex = 0;
        List<Transform> rootTransforms = new List<Transform>();

        foreach (Transform obj in GetAllOnScene<Transform>())
        {
            if (obj.parent == null)
            {
                rootTransforms.Add(obj);
            }
        }

        rootTransforms.Sort((x, y) => x.GetSiblingIndex().CompareTo(y.GetSiblingIndex()));

        foreach (Transform rootTransform in rootTransforms)
        {
            currentIndex = Traverse(rootTransform, currentIndex);
        }

        foreach (var rootNode in allRootNodes)
        {
            rootNode.Value.Sort((x, y) => x.HierarchyIndex.CompareTo(y.HierarchyIndex));
            for (int i = 0; i < rootNode.Value.Count; i++)
            {
                SortChildren(rootNode.Value[i]);
            }
        }

        var list = allRootNodes.Keys.ToList();
        list.Sort((x, y) => rootsHierarchyIndeces[x].CompareTo(rootsHierarchyIndeces[y]));

        var sortedDictionary = new Dictionary<Transform, List<WooPanelNode>>();

        foreach (var root in list)
        {
            sortedDictionary.Add(root, allRootNodes[root]);
        }

        allRootNodes = sortedDictionary;
    }

    int Traverse(Transform transformToTraverse, int index)
    {
        index++;

        var panel = transformToTraverse.GetComponent<WooPanel>();
        if (panel != null)
        {
            panelNodes[panel].HierarchyIndex = index;
        }

        if (allRootNodes.ContainsKey(transformToTraverse))
        {
            rootsHierarchyIndeces[transformToTraverse] = index;
        }

        foreach (Transform child in transformToTraverse)
        {
            index = Traverse(child, index);
        }

        return index;
    }

    public void SortChildren(WooPanelNode panel)
    {
        panel.Children.Sort((x, y) => x.HierarchyIndex.CompareTo(y.HierarchyIndex));

        for (int i = 0; i < panel.Children.Count; i++)
        {
            SortChildren(panel.Children[i]);
        }
    }

}
