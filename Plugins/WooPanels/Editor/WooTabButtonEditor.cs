﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;

using UnityEngine;
using System.IO;
using System;
using System.Text;
using System.Reflection;
using UnityEngine.UI;
using System.Linq;

namespace Wooplex.Panels
{
	[CanEditMultipleObjects]
	[CustomEditor(typeof(WooTabButton), true)]
	public class WooTabButtonEditor : Editor
	{
        private bool foldOut = false;

		public void OnEnable()
		{
            
		}

        public override void OnInspectorGUI()
        {
            var it = serializedObject.GetIterator();
            it.NextVisible(true);

            EditorGUILayout.PropertyField(it, true);
            var button = target as WooTabButton;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("Panel"));

            foldOut = EditorGUILayout.Foldout(foldOut, "Properties");

            if (foldOut)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("TargetGraphic"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("TargetText"));

                if (button.TargetGraphic != null)
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("SwapSprites"));
                }

                if (button.SwapSprites && button.TargetGraphic != null)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("OnSprite"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("OffSprite"));
                    EditorGUI.indentLevel--;
                }

                EditorGUILayout.PropertyField(serializedObject.FindProperty("SwapColors"));
                if (button.SwapColors && (button.TargetGraphic != null || button.TargetText != null))
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("OnColor"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("OffColor"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("PressedColor"));
                    EditorGUI.indentLevel--;
                }

                EditorGUILayout.PropertyField(serializedObject.FindProperty("SwapText"));
                if (button.SwapText && button.TargetText != null)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("OnText"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("OffText"));
                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(serializedObject.FindProperty("SelfClickToClose"));
                EditorGUI.indentLevel--;
            }

            serializedObject.ApplyModifiedProperties();
        }
	}
}
