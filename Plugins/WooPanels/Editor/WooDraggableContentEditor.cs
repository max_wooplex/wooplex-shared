﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Wooplex.Panels
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(WooDraggableContent), true)]
    public class WooDraggableContentEditor : Editor
    {
        private bool editAnchors = false;
        private bool setAnchorPoint = false;
        private float prevScreenHeight = -1.0f;
        private float prevScreenWidth = -1.0f;
        private int counter = 0;
        private Camera mainCamera;
        private bool shiftSnapEnabled = false;
        private bool altSnapEnabled = false;
        private Vector2 snapVector = Vector2.zero;

        private GameObject AnchorToSetGo = null;
        private Vector2 directionVector;

        private Texture FromLeftToLeftTexture;
        private Texture FromTopLeft;
        private Texture FromTopMid;
        private Texture FromTopRight;
        private Texture FromMidLeft;
        private Texture FromMid;
        private Texture FromMidRight;
        private Texture FromBotLeft;
        private Texture FromBotMid;
        private Texture FromBotRight;

        private Texture ToTopLeft;
        private Texture ToTopMid;
        private Texture ToTopRight;

        private Texture ToMidLeft;
        private Texture ToMid;
        private Texture ToMidRight;

        private Texture ToBotLeft;
        private Texture ToBotMid;
        private Texture ToBotRight;

        enum DraggingEditMode
        {
            None,
            StartPosition,
            StartAnchor,
            EndPosition,
            EndAnchor
        }

        private DraggingEditMode draggingEditMode = DraggingEditMode.None;

        public void OnEnable()
        {
            mainCamera = Camera.main;

            FromLeftToLeftTexture = (Texture)Resources.Load("WooPanels/Textures/IconWithParent");

            FromTopLeft = (Texture)Resources.Load("WooPanels/Textures/FromTopLeft");
            FromTopMid = (Texture)Resources.Load("WooPanels/Textures/FromTopMid");
            FromTopRight = (Texture)Resources.Load("WooPanels/Textures/FromTopRight");

            FromMidLeft = (Texture)Resources.Load("WooPanels/Textures/FromMidLeft");
            FromMid = (Texture)Resources.Load("WooPanels/Textures/FromMid");
            FromMidRight = (Texture)Resources.Load("WooPanels/Textures/FromMidRight");

            FromBotLeft = (Texture)Resources.Load("WooPanels/Textures/FromBotLeft");
            FromBotMid = (Texture)Resources.Load("WooPanels/Textures/FromBotMid");
            FromBotRight = (Texture)Resources.Load("WooPanels/Textures/FromBotRight");

            ToTopLeft = (Texture)Resources.Load("WooPanels/Textures/ToTopLeft");
            ToTopMid = (Texture)Resources.Load("WooPanels/Textures/ToTopMid");
            ToTopRight = (Texture)Resources.Load("WooPanels/Textures/ToTopRight");

            ToMidLeft = (Texture)Resources.Load("WooPanels/Textures/ToMidLeft");
            ToMid = (Texture)Resources.Load("WooPanels/Textures/ToMid");
            ToMidRight = (Texture)Resources.Load("WooPanels/Textures/ToMidRight");

            ToBotLeft = (Texture)Resources.Load("WooPanels/Textures/ToBotLeft");
            ToBotMid = (Texture)Resources.Load("WooPanels/Textures/ToBotMid");
            ToBotRight = (Texture)Resources.Load("WooPanels/Textures/ToBotRight");
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }

        private bool IsMouseCloseTo(GameObject go)
        {
            var result = false;

            Vector3 mousePosition = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;

            var direction = mousePosition - go.transform.position;
            direction.z = 0.0f;

            if (direction.magnitude < HandleUtility.GetHandleSize(mousePosition) / 8.0f)
            {
                result = true;
            }

            return result;
        }

        private int prevControlId = -1;
        private bool controlTakenOver = false;

        private void OnSceneGUI()
        {
            if (Application.isPlaying)
            {
                return;
            }

            var draggableContent = ((WooDraggableContent)target);
            var panel = draggableContent.Panel;
            bool changed = false;

            bool cameraChanged = mainCamera == null || prevScreenHeight != Camera.main.pixelHeight || prevScreenWidth != Camera.main.pixelWidth;
            prevScreenHeight = mainCamera.pixelHeight;
            prevScreenWidth = mainCamera.pixelWidth;

            Handles.BeginGUI();
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.green;

            GUI.Label(new Rect(new Vector2(2.0f, 2.0f), new Vector2(200.0f, 100.0f)), "Shift - snap to line | Alt - snap to angle", style);

            ////DrawPresetsButtons();

            Handles.EndGUI();

            if (Event.current.modifiers == EventModifiers.Shift)
            {
                if (!shiftSnapEnabled)
                {
                    directionVector = panel.Movement.DragTo.transform.position - panel.Movement.DragFrom.transform.position;
                }
                shiftSnapEnabled = true;
            }
            else
            {
                shiftSnapEnabled = false;

            }
            if (Event.current.modifiers == EventModifiers.Alt)
            {
                altSnapEnabled = true;
            }
            else
            {
                altSnapEnabled = false;
            }

            if (cameraChanged)
            {
                counter = 3;
            }

            if (counter > 0)
            {
                counter--;
                if (WooPanelsEngine.Instance.panelNodes.ContainsKey(panel))
                {
                    WooPanelsEngine.Instance.panelNodes[panel].SamplePanelAnimator(true);
                }

                return;
            }

            bool skipPositioning = false;

            if (!panel.Movement.IsDraggable || panel.Movement.DraggableContent == null || counter > 0)
            {
                skipPositioning = true;
            }


            Vector3 newTargetPosition = Vector3.zero;
            int controlId = GUIUtility.GetControlID(FocusType.Passive);
            var isMouseCloseToStart = IsMouseCloseTo(panel.Movement.DragFrom);
            var isMouseCloseToEnd = IsMouseCloseTo(panel.Movement.DragTo);

            if (isMouseCloseToStart || isMouseCloseToEnd)
            {
                GUIUtility.hotControl = controlId;
                controlTakenOver = true;
            }
            else if (controlTakenOver)
            {
                controlTakenOver = false;
                GUIUtility.hotControl = 0;
            }

            if (Event.current.type == EventType.MouseDown)
            {
                if (isMouseCloseToStart)
                {
                    draggingEditMode = DraggingEditMode.StartPosition;
                    GUIUtility.hotControl = controlId;
                    AnchorToSetGo = panel.Movement.DragFrom;

                    Undo.RecordObject(panel.Movement.DragFrom.transform, "DragFromPosition");

                    panel.Close();

                    Event.current.Use();
                }

                if (isMouseCloseToEnd)
                {
                    draggingEditMode = DraggingEditMode.EndPosition;
                    GUIUtility.hotControl = controlId;
                    AnchorToSetGo = panel.Movement.DragTo;
                    Undo.RecordObject(panel.Movement.DragTo, "DragToPosition");

                    panel.Open();

                    Event.current.Use();
                }
            }

            if (Event.current.type == EventType.MouseUp && (isMouseCloseToStart || isMouseCloseToEnd))
            {
                draggingEditMode = DraggingEditMode.None;
                GUIUtility.hotControl = 0;
                shiftSnapEnabled = false;
                Event.current.Use();
            }

            if (Event.current.type == EventType.MouseDrag && !skipPositioning)
            {
                Vector3 mousePosition = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;

                MoveActiveAnchor(mousePosition, panel);
               
                if (draggingEditMode != DraggingEditMode.None)
                {
                    changed = true;
                    Event.current.Use();
                }
            }

            if (!changed)
            {
                if (panel.IsClosed())
                {
                    panel.Movement.DragFromAnchor.transform.position = panel.Movement.DragFrom.transform.position;
                    UpdateAnchorValues(panel.Movement.DragFromAnchor, draggableContent.gameObject);
                    panel.Movement.DragFromAnchor.transform.position = panel.Movement.DragFrom.transform.position;
                }

                if (panel.IsOpened())
                {
                    panel.Movement.DragToAnchor.transform.position = panel.Movement.DragTo.transform.position;
                    UpdateAnchorValues(panel.Movement.DragToAnchor, draggableContent.gameObject);
                    panel.Movement.DragToAnchor.transform.position = panel.Movement.DragTo.transform.position;
                }
            }

            if (changed)
            {
                WooPanelsEngine.Instance.panelNodes[panel].SamplePanelAnimator(true);
            }

            if (draggingEditMode == DraggingEditMode.StartPosition)
            {
                isMouseCloseToStart = true;
                isMouseCloseToEnd = false;
            }
            else if (draggingEditMode == DraggingEditMode.EndPosition)
            {
                isMouseCloseToStart = false;
                isMouseCloseToEnd = true;
            }

            if (isMouseCloseToEnd && isMouseCloseToStart)
            {
                isMouseCloseToStart = false;
            }

            DrawText(panel.Movement.DragFrom.transform.position, "From", isMouseCloseToStart, new Vector2(-2.0f, -8.0f));
            DrawText(panel.Movement.DragTo.transform.position, "To", isMouseCloseToEnd, new Vector2(-1.0f, 1.5f));

            WooEditorHelper.DrawArrow(panel.Movement.DragFrom.transform.position, panel.Movement.DragTo.transform.position, isMouseCloseToStart, isMouseCloseToEnd, WooPanelsEngine.Instance.panelNodes[panel].SimulatedTime);
        }

        private void UpdateAnchorValues(GameObject anchor, GameObject content)
        {
            var w = content.GetComponent<RectTransform>().rect.width;
            var halfW = w / 2.0f;
            var h = content.GetComponent<RectTransform>().rect.height;
            var halfH = h / 2.0f;

            var anchorMinX = (halfW + anchor.transform.localPosition.x) / w;
            var anchorMinY = (halfH + anchor.transform.localPosition.y) / h;

            anchor.GetComponent<RectTransform>().anchorMin = new Vector2(anchorMinX, anchorMinY);
            anchor.GetComponent<RectTransform>().anchorMax = new Vector2(anchorMinX, anchorMinY);
        }

        private void DrawText(Vector3 position, string operationName, bool isActive, Vector2 anchor)
        {
            GUIStyle style = new GUIStyle();

            Handles.BeginGUI();

            style.normal.textColor = Color.black;
            var dx = HandleUtility.GetHandleSize(position) / 16.0f * anchor.x;
            var dy = HandleUtility.GetHandleSize(position) / 32.0f * anchor.y;

            var dx1 = dx - HandleUtility.GetHandleSize(position) * 0.01f;
            var dy1 = dy - HandleUtility.GetHandleSize(position) * 0.01f;

            Handles.Label(position + new Vector3(dx1, -dy1, 0.0f), operationName, style);

            if (isActive)
            {
                style.normal.textColor = Color.green;
            }
            else
            {
                style.normal.textColor = Color.white;
            }

            Handles.Label(position + new Vector3(dx, -dy, 0.0f), operationName, style);

            Handles.EndGUI();
        }


        private void MoveActiveAnchor(Vector3 newTargetPosition, WooPanel panel)
        {
            Transform transformToSet = null;
            Transform otherTransform = null;

            if (draggingEditMode == DraggingEditMode.StartPosition)
            {
                transformToSet = panel.Movement.DragFrom.transform;
                otherTransform = panel.Movement.DragTo.transform;
            }
            else if (draggingEditMode == DraggingEditMode.EndPosition)
            {
                transformToSet = panel.Movement.DragTo.transform;
                otherTransform = panel.Movement.DragFrom.transform;
            }
            else
            {
                return;
            }

            newTargetPosition.z = transformToSet.position.z;

            var newVector = newTargetPosition - transformToSet.position;
            if (shiftSnapEnabled)
            {
                newTargetPosition = Vector3.Project(newVector, directionVector);
                newTargetPosition += transformToSet.position;
            }

            if (altSnapEnabled)
            {
                Vector2[] snapVectors = { Vector2.up, Vector2.left, Vector2.right, Vector2.down, new Vector2(-1.0f, -1.0f), new Vector2(-1.0f, 1.0f), new Vector2(1.0f, -1.0f), new Vector2(1.0f, 1.0f) };
                Vector2 closest = Vector2.up;
                var dir = newTargetPosition - otherTransform.position;
                float closestAngle = 999.0f;

                for (int i = 0; i < snapVectors.Length; i++)
                {
                    var newAngle = Vector2.Angle(dir, snapVectors[i]);

                    if (newAngle < closestAngle)
                    {
                        closestAngle = newAngle;
                        closest = snapVectors[i];
                    }
                }
                snapVector = closest;

                transformToSet.position = otherTransform.position + Vector3.Project(dir, snapVector);
                newVector = newTargetPosition - transformToSet.position;
                newTargetPosition = Vector3.Project(newVector, snapVector);
                newTargetPosition += transformToSet.position;
            }

            UpdateAnchorValues(transformToSet.gameObject, transformToSet.parent.gameObject);
            transformToSet.position = newTargetPosition;

        }

        private Vector3 GetPositionOfAnchor(Vector2 anchorPoint, WooPanel panel, GameObject anchorGo)
        {
            var delta = anchorPoint - new Vector2(0.5f, 0.5f);
            var rectTransform = (anchorGo.transform as RectTransform);
            var parentRectTransform = (rectTransform.parent.transform as RectTransform);
            var contentPos = parentRectTransform.position;
            var result = contentPos;
            delta *= 2.0f;
            result.x += delta.x * parentRectTransform.rect.width * panel.Canvas.scaleFactor / 2.0f;
            result.y += delta.y * parentRectTransform.rect.height * panel.Canvas.scaleFactor / 2.0f;

            return result;
        }

        private void DrawAnchorLine(DraggingEditMode editModeToBeActive, GameObject anchorGo, WooPanel panel)
        {
            var rectTransform = anchorGo.transform as RectTransform;
            var isActive = draggingEditMode == editModeToBeActive;

            var anchorMin = rectTransform.anchorMin;
            var anchorMax = rectTransform.anchorMax;
            var anchor = (anchorMin + anchorMax) / 2.0f;
            var positionOfAnchor = GetPositionOfAnchor(anchor, panel, anchorGo);

            var prevColor = Handles.color;

            if (isActive)
            {
                Handles.color = Color.blue;
            }

            Handles.DrawDottedLine(positionOfAnchor, anchorGo.transform.position, 3.0f);

            if (isActive)
            {
                Handles.color = prevColor;
            }
        }

        private void SetAnchorPoint(GameObject anchorGo, WooPanel panel, Vector2 anchorPoint)
        {
            if (anchorGo == null)
            {
                return;
            }

            var delta = anchorPoint - new Vector2(0.5f, 0.5f);
            var rectTransform = (anchorGo.transform as RectTransform);
            var positionOfAnchor = GetPositionOfAnchor(anchorPoint, panel, anchorGo);
            var isActive = rectTransform.anchorMin == anchorPoint && rectTransform.anchorMax == anchorPoint;
            var prevColor = Handles.color;

            if (isActive)
            {
                Handles.color = Color.blue;
            }

            if (Handles.Button(positionOfAnchor, Quaternion.identity, 15.0f, 15.0f, Handles.SphereHandleCap))
            {
                var prevPos = rectTransform.position;
                rectTransform.anchorMin = anchorPoint;
                rectTransform.anchorMax = anchorPoint;
                rectTransform.position = prevPos;
            }
            if (isActive)
            {
                Handles.color = prevColor;
            }
        }
    }
}
