﻿using System;
using UnityEngine;
using UnityEditor;

namespace Wooplex.Panels
{
    [CustomPropertyDrawer(typeof(Movement))]
    public class MovementPropertyDrawer: PropertyDrawer
    {
        bool propertyOpened = false;

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.

            EditorGUI.BeginProperty(position, label, property);// Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

           // propertyOpened = EditorGUILayout.Foldout(propertyOpened, property.name);

           // if (propertyOpened)
           // {
                GUILayout.Button("Add Draggable Content");
               
           // }
            EditorGUI.EndProperty();
        }
    }
}
