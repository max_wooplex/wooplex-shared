﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Wooplex.Panels
{
    public class WooEditorHelper
    {

        public static void DrawArrow(Vector3 start, Vector3 end, bool isMouseCloseToStart, bool isMouseCloseToEnd, float simulatedTime = 0.0f)
        {
            var arrowHead = new Vector3[3];

            var arrowStart = new Vector3[4];
            //var positionHandle = new Vector3[4];

            var arrowLine = new Vector3[2];

            var forward = (end - start).normalized;
            var right = Vector3.Cross(new Vector3(0.0f, 0.0f, -1.0f), forward).normalized;
            var size = (end - start).magnitude / 5.0f;
            var width = HandleUtility.GetHandleSize(end) / 16.0f;
            var height = HandleUtility.GetHandleSize(end) / 8.0f;
            var discPosition = Vector3.Lerp(start, end, simulatedTime);

            start.z = 0.0f;
            end.z = 0.0f;
            arrowHead[0] = end;
            arrowHead[1] = end - forward * height + right * width;
            arrowHead[2] = end - forward * height - right * width;

            arrowStart[0] = start + right * width - forward * height / 2.0f;
            arrowStart[1] = start + right * width + forward * height / 2.0f;
            arrowStart[2] = start - right * width + forward * height / 2.0f;
            arrowStart[3] = start - right * width - forward * height / 2.0f; ;

            width *= 1.5f;
            height *= 1.5f;

            //positionHandle[0] = discPosition + right * width  - forward * height / 2.0f;
            //positionHandle[1] = discPosition + right * width + forward * height / 2.0f;
            //positionHandle[2] = discPosition - right * width + forward * height / 2.0f;
            //positionHandle[3] = discPosition - right * width - forward * height / 2.0f; ;


            arrowLine[0] = start;
            arrowLine[1] = end ;


            //if (dotted)
            //{
            //    Handles.DrawDottedLines(arrowLine, 4.0f);
			//else
            //}
            //{
            var prevColor2 = Handles.color;
            Handles.color = Color.blue;
            var prevColor = Handles.color;

            Handles.DrawAAPolyLine(3.0f, arrowLine);


            //Handles.DrawSolidDisc(start, new Vector3(0.0f, 0.0f, 1.0f), 3.0f);
            //Handles.DrawSolidDisc(end, new Vector3(0.0f, 0.0f, 1.0f), 3.0f);

            if (isMouseCloseToStart)
            {
                Handles.color = Color.green;
            }
            Handles.DrawAAConvexPolygon(arrowStart);

            Handles.color = prevColor;

            if (isMouseCloseToEnd)
            {
                Handles.color = Color.green;
            }
            Handles.DrawAAConvexPolygon(arrowHead);

            Handles.color = Color.cyan;
            Handles.DrawSolidDisc(discPosition, new Vector3(0.0f, 0.0f, 1.0f), HandleUtility.GetHandleSize(discPosition) / 32.0f);
            Handles.color = prevColor;
            Handles.color = prevColor2;
        }
    }

}
