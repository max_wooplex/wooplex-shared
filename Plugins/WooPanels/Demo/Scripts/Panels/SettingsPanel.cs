using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;

public class SettingsPanel : WooPanel
{
	public Slider VolumeSlider;
	public Slider SFXSlider;

	public AudioSource BackgroundSource;
	public AudioSource SFXSource;

	// At the beginning even if the panel is closed 
	void OnInit ()
	{
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;

        VolumeSlider.value = BackgroundSource.volume;
		SFXSlider.value = SFXSource.volume;
	}

	public void OnVolumeChange()
	{
		BackgroundSource.volume = VolumeSlider.value;
	}

	public void OnSFXChange()
	{
		SFXSource.volume = SFXSlider.value;
	}

	// Before animation starts
	void OnOpen ()
	{
		
	}

	// After animation finishes
	void OnOpenEnd ()
	{
		
	}

	// Before animation starts
	void OnClose ()
	{
		
	}

	// After animation finishes
	void OnCloseEnd ()
	{
		
	}

	// Every frame when the panel is opened or being opened
	void PanelUpdate ()
	{
	}
}
